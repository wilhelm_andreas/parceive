Data Collector for C++
======================
This folder contains the C++ backend of Parceive and the node.js module for software architecture
reconstruction. Build is handled through the Cmake script available locally within the same folder.

Directory Structure
===================
The directory is divided into the following sub-directories:
* `dbghelp`: reader for Windows DbgHelp symbol information
* `dwarf`: reader for Linux dwarf symbol information
* `include`: header files of the backend
* `nodejs`: node.js module for software architecture reconstruction
* `src`: source files of the backend
* `symbols`: symbol information writer (standalone tool)