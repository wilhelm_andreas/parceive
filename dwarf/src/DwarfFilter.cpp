//
// Created by wilhelma on 1/23/17.
//

#include "DwarfFilter.h"

namespace pcv {
namespace dwarf {

DwarfFilter::DwarfFilter(const std::string &include, const std::string &exclude)
  : include_{include}, exclude_{exclude} {};

bool DwarfFilter::isIncluded(const std::string &fileName) const
{
  auto included = std::regex_match(fileName, include_);
  return included;
}

bool DwarfFilter::isExcluded(const std::string &fileName) const
{
  auto excluded = std::regex_match(fileName, exclude_);
  return excluded;
}

bool DwarfFilter::isIncluded(const std::string &fileName, uint lineNo) const
{
  return !isExcluded(fileName) && isIncluded(fileName) && (lineNo > 0);
}

void DwarfFilter::assign(const std::string &include, const std::string &exclude)
{
  include_.assign(include);
  exclude_.assign(exclude);
}

}  // namespace dwarf
}  // namespace pcv
