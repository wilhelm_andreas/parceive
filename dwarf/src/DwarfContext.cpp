//
// Created by wilhelma on 1/7/17.
//

#include "DwarfContext.h"
#include <Type.h>
#include <algorithm>
#include <cassert>

namespace pcv {

using entity::Class;

namespace dwarf {

void DwarfContext::addClass(Dwarf_Off off, Class *cls)
{
  add(off, cls);

  if (getClass(*cls->nmsp, cls->name) == nullptr) {
    auto classSpecifier           = getClassSpecifier(cls->nmsp->name, cls->name);
    nameClassMap_[classSpecifier] = cls;

    // add nested classes
    if (cls->cls != nullptr) {
      if (std::find(std::begin(cls->cls->nestedClasses), std::end(cls->cls->nestedClasses), cls) ==
          std::end(cls->cls->nestedClasses)) {
        cls->cls->nestedClasses.push_back(cls);
      }
    }
  }

  currentClass.push_back(cls);
}

Class *DwarfContext::getClass(const Namespace &nmsp, const Class::name_t &name) const
{
  auto classSpecifier = getClassSpecifier(nmsp.name, name);
  auto tmp            = nameClassMap_.find(classSpecifier);
  if (tmp != std::end(nameClassMap_)) return tmp->second;

  return nullptr;
}

void DwarfContext::addRoutine(Dwarf_Off off, std::unique_ptr<Routine> rtn)
{
  nameRtnMap_[rtn->name] = rtn.get();
  add(off, rtn.get());
  routines.emplace_back(std::move(rtn));
}

void DwarfContext::linkNameToRoutine(const std::string &rtnName, Routine *rtn)
{
  nameRtnMap_[rtnName] = rtn;
}

Routine *DwarfContext::getRoutine(const pcv::entity::Id_t imageId,
                                  const Routine::name_t &name) const
{
  auto fixedName = getFixedConstructorName(name);
  auto rtn       = nameRtnMap_.find(fixedName);
  if (rtn != std::end(nameRtnMap_))
    return rtn->second;
  else
    return nullptr;
}

void DwarfContext::addTypedef(Dwarf_Off off, const std::string &name)
{
  offTypedefName_[off] = name;
}

void DwarfContext::addInheritance(Dwarf_Off baseOff, Dwarf_Off inhOff)
{
  inheritances_.emplace_back(ClassRelation(baseOff, inhOff));
}

void DwarfContext::addComposition(Dwarf_Off first, Dwarf_Off second)
{
  compositions_.emplace_back(ClassRelation(first, second));
}

void DwarfContext::demangleRoutines()
{
  for (auto &rtn : routines) {
    if (rtn->name.c_str()[0] == '_')
      rtn->name = demangle(rtn->name.c_str()); //demangleWithoutReturnType(rtn->name.c_str());
  }
}

void DwarfContext::establishInheritance()
{
  for (auto &inh : inheritances_) {
    Class *inhClass  = get<Class>(inh.second);
    Class *baseClass = get<Class>(inh.first);
    if (inhClass && baseClass) {
      inhClass->baseClasses.push_back(baseClass);
      baseClass->inheritClasses.push_back(inhClass);
    }
  }
}

void DwarfContext::establishComposition()
{
  for (auto &com : compositions_) {
    Class *inhClass  = get<Class>(com.second);
    Class *baseClass = get<Class>(com.first);
    if (inhClass && baseClass) {
      inhClass->composites.push_back(baseClass);
    }
  }
}

void DwarfContext::establishTypedefs()
{
  for (auto &td : offTypedefName_) {
    auto ptr = get<Class>(td.first);
    if (ptr) {
      ptr->name = td.second;
    }
  }
}

void DwarfContext::reset() noexcept
{
  toClean.clear();
  currentClass.clear();
  auto empty       = std::stack<Routine *>{};
  currentNamespace = emptyNamespace;
  currentRoutine.swap(empty);
  offTypedefName_.clear();
  inheritances_.clear();
  compositions_.clear();
  nameClassMap_.clear();
}

void DwarfContext::clearCache() noexcept { offTypedefName_.clear(); }

std::string DwarfContext::getClassSpecifier(const std::string &namespaceName,
                                            const Class::name_t &className) const
{
  std::string specifier{namespaceName};

  if (!currentClass.empty()) {
    for (auto &cls : currentClass) {
      specifier += cls->name + "::";
    }
  }

  return specifier + className;
}

const Routine::name_t DwarfContext::getFixedConstructorName(const Routine::name_t &cstr) const
{
  // a workaround for the following problem (C1 and C2 constructor emitted):
  // http://stackoverflow.com/questions/6921295/dual-emission-of-constructor-symbols
  Routine::name_t altConstructor{cstr};
  std::size_t pos = cstr.find("C2E");
  if (pos != std::string::npos) {
    altConstructor.replace(pos, 3, "C1E");
  } else {
    pos = cstr.find("C1E");
    if (pos != std::string::npos) altConstructor.replace(pos, 3, "C2E");
  }

  return altConstructor;
}

void DwarfContext::reserveEntityVector(uint32_t imgId, size_t e)
{
  vectorSize_[imgId] = e;
  entities_[imgId] =
    std::unique_ptr<std::vector<SoftwareEntity *>>{new std::vector<SoftwareEntity *>(e, nullptr)};
  currentVectorSize_ = vectorSize_[imgId];
  currentEntity_     = entities_[imgId].get();
  externalSymbols_ =
    std::unique_ptr<std::vector<ExternalSymbol>>{new std::vector<ExternalSymbol>(e, 0)};
}

void DwarfContext::addSourceLocation(const Dwarf_Addr &ip,
                                     std::unique_ptr<pcv::dwarf::SourceLocation> loc) noexcept
{
  sourceLocations_[ip + imgOffset_] = std::move(loc);
}

SourceLocation *DwarfContext::getSourceLocation(Dwarf_Addr ip) const
{
  auto location = sourceLocations_.upper_bound(ip);
  if (location != std::begin(sourceLocations_)) {
    location--;
    return location->second.get();
  }
  return nullptr;
}

void DwarfContext::finalizeSourceLocations()
{
  if (!sourceLocations_.empty()) {
    sourceLocations_[sourceLocations_.rbegin()->first + 1] =
      std::unique_ptr<SourceLocation>{new SourceLocation(0, nullptr)};
  }
}

void DwarfContext::add(Dwarf_Off off, SoftwareEntity *entity) { (*currentEntity_)[off] = entity; }

void DwarfContext::addMember(Dwarf_Off off, Variable *member)
{
  currentClass.back()->members.emplace_back(member);
  add(off, member);
}
void DwarfContext::transformSourceLocations(Dwarf_Addr low_pc,
                                            Dwarf_Addr high_pc,
                                            uint32_t lineNo,
                                            uint32_t file)
{
  auto lower = sourceLocations_.lower_bound(low_pc);
  if (lower != std::end(sourceLocations_)) {
    auto upper = sourceLocations_.upper_bound(high_pc);
    std::for_each(lower, upper, [&lineNo, &file, this](sourceLocations_t::value_type &location) {
      location.second->lineNo = lineNo;
      location.second->file   = getCuSrcFile(file - 1);
    });
  }
}

void DwarfContext::setCurrentImage(std::unique_ptr<Image> image,
                                   offset_t imgOffset,
                                   size_t infoSize)
{
  reserveEntityVector(image->id, infoSize);
  images.emplace_back(std::move(image));
  currentImage = images.back().get();
  imgOffset_   = imgOffset;
}

void DwarfContext::markSymbolAsExternal(Dwarf_Off off)
{
  (*externalSymbols_)[off].isExternal = true;
  if (!currentClassOffset_.empty()) {
    (*externalSymbols_)[off].parentOffset = currentClassOffset_.back();
    for (auto &parentOffset : currentClassOffset_) {
      (*externalSymbols_)[parentOffset].isExternal = true;
    }
  }
}

void DwarfContext::unmarkSymbolAsExternal(Dwarf_Off off)
{
  (*externalSymbols_)[off].isExternal = false;
  if ((*externalSymbols_)[off].parentOffset)
    unmarkSymbolAsExternal((*externalSymbols_)[off].parentOffset);
}

void DwarfContext::pushCurrentClassOffset(Dwarf_Off off)
{
  (*externalSymbols_)[off].isExternal = false;
  if (!currentClassOffset_.empty()) {
    (*externalSymbols_)[off].parentOffset = currentClassOffset_.back();
  }

  currentClassOffset_.emplace_back(off);
}

void DwarfContext::popCurrentClassOffset() { currentClassOffset_.pop_back(); }

bool DwarfContext::isSymbolExternal(Dwarf_Off off) const
{
  return (*externalSymbols_)[off].isExternal;
}

void DwarfContext::addCuSrcFile(std::string fileName)
{
  cuSrcFiles_.emplace_back(std::move(fileName));
}

File *DwarfContext::getCuSrcFile(size_t pos)
{
  const auto &it = imageSrcFiles_.find(cuSrcFiles_[pos]);

  if (it == std::end(imageSrcFiles_)) {
    files.emplace_back(
      std::unique_ptr<entity::File>{new entity::File(cuSrcFiles_[pos], currentImage)});
    imageSrcFiles_[cuSrcFiles_[pos]] = files.back().get();
    return files.back().get();
  } else {
    return it->second;
  }
}

std::string DwarfContext::getCuSrcFileName(size_t pos) const { return cuSrcFiles_[pos]; }

void DwarfContext::clearCuSrcFiles() { cuSrcFiles_.clear(); }

void DwarfContext::clearImgSrcFiles() { imageSrcFiles_.clear(); }

}  // namespace dwarf
}  // namespace pcv
