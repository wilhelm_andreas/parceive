//
// Created by wilhelma on 12/12/17.
//

#pragma once

#include "DwarfHelper.h"
#include "tag/TagGeneric.h"

namespace {

using pcv::dwarf::DwarfContext;
using pcv::dwarf::getAttrAddr;
using pcv::dwarf::getAttrUint;
using pcv::dwarf::hasAttr;

bool handle(DwarfContext &ctxt)
{
  Dwarf_Addr low_pc, high_pc;
  Dwarf_Unsigned call_line, call_file;

  Dwarf_Off off;
  dwarf_dieoffset(ctxt.die, &off, nullptr);

  getAttrUint(ctxt.dbg, ctxt.die, DW_AT_call_line, &call_line);
  getAttrUint(ctxt.dbg, ctxt.die, DW_AT_call_file, &call_file);

  if (hasAttr(ctxt.die, DW_AT_low_pc) && hasAttr(ctxt.die, DW_AT_high_pc)) {
    getAttrAddr(ctxt.dbg, ctxt.die, DW_AT_low_pc, &low_pc);
    getAttrUint(ctxt.dbg, ctxt.die, DW_AT_high_pc, &high_pc);
    ctxt.transformSourceLocations(low_pc, low_pc + high_pc, call_line, call_file);
  } else if (hasAttr(ctxt.die, DW_AT_entry_pc) && hasAttr(ctxt.die, DW_AT_ranges)) {
    // todo: support inlined ranges with dwarf_get_ranges
    //    getAttrAddr(ctxt.dbg, ctxt.die, DW_AT_entry_pc, &low_pc);
    //    getAttrAddr(ctxt.dbg, ctxt.die, DW_AT_ranges, &high_pc);
  }

  return true;  // do not continue
}

}  // namespace

namespace pcv {
namespace dwarf {

class DwarfContext;

template <>
struct TagHandler<DW_TAG_inlined_subroutine> {
  static bool handle(DwarfContext &ctxt) { return ::handle(ctxt); }
  static bool handleDuplicate(DwarfContext &ctxt) { return ::handle(ctxt); }
};

}  // namespace dwarf
}  // namespace pcv
