Linux Dwarf Reader
==================
This component is responsible for parsing ELF based debug information of Linux executables and
shared libraries.

The main class is `DwarfReader` (reader), which inherits from the abstract class `SymInfo` used by
Parceive for symbol resolving. The reader iterates via depth-first search through all Dwarf nodes in
given debug information. Whenever a node has an interesting type for the analysis (e.g., compile
units, classes, functions, or variables), the reader collects the respective symbol information.
The 
