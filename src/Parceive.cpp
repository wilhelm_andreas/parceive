/* Copyright 2014 Siemens Technology and Services*/

#include <boost/lambda/lambda.hpp>

#include "Parceive.h"

#include "DummyWriter.h"
#include "LoopDetector.h"
#include "Resolver.h"
#include "SQLWriter.h"
#include "Thread.h"
#ifdef WIN32
#else
#include "ThreadEmulate.h"
#include "ThreadPosix.h"
#endif
#include "Routine.h"
#include "Variable.h"
#ifdef WIN32
#include "ParceiveDbgHelpReader.h"
#else
#include "DwarfReader.h"
#endif

#ifdef WIN32
namespace WINDOWS {
#include <Psapi.h>
#include <Windows.h>
}  // namespace WINDOWS
#else
#include <sys/resource.h>
#endif

#define MEMORY_INFO fprintf(stderr, " - Virt.Mem: %d\n", current_virtual_memory_usage())

/*#define PROGRESS_PRINT(...)       \
{                               \
  fprintf(stderr, __VA_ARGS__); \
}*/
#define PROGRESS_PRINT(...)

uint32_t current_virtual_memory_usage()
{
#ifdef WIN32
  WINDOWS::PROCESS_MEMORY_COUNTERS cnt;
  WINDOWS::Sleep(10);
  WINDOWS::GetProcessMemoryInfo(WINDOWS::GetCurrentProcess(), &cnt, sizeof(cnt));
  return static_cast<uint32_t>(cnt.PeakWorkingSetSize);
  // return 0;
#else
  struct rusage r_usage;
  usleep(10000);
  getrusage(RUSAGE_SELF, &r_usage);
  return r_usage.ru_maxrss;
#endif

#if 0
  typedef struct {
  unsigned long size, resident, share, text, lib, data, dt;
} statm_t;
  statm_t result;
  const char *statm_path = "/proc/self/statm";

  FILE *f = fopen(statm_path, "r");
  if (f == nullptr) {
    perror(statm_path);
    abort();
  }
  if (7 != fscanf(f, "%ld %ld %ld %ld %ld %ld %ld",
                  &result.size, &result.resident, &result.share, &result.text, &result.lib, &result.data, &result.dt)) {
    perror(statm_path);
    abort();
  }
  fclose(f);
  return result.size;
#endif
}

namespace pcv {

Parceive::Parceive(int _argc, char **_argv) : argc(_argc), argv(_argv)
{
  initializeKnobs();

  long size = 294967296l;
  for (short i = 0; i < size; ++i) {}

  PIN_InitSymbols();
  if (PIN_Init(argc, argv)) {
    usage();
    exit(0);
  }

  // get the paths of the parceive tool and the user application
  std::string parceivePath{};
  for (int i = 0; i < argc; i++) {
    if (string(argv[i]) == "-t") {
      parceivePath = argv[i + 1];
    } else if (string(argv[i]) == "--") {
      executable = argv[i + 1];
    }
  }

  lookupSourceLocations = getKnob("sourcelocation") == "on";
  traceUnknowns         = getKnob("unknowns") == "on";

  // create Parceive's mandatory modules
  pFilter   = std::unique_ptr<Filter>(new Filter(getKnob("f")));
  pWriter   = std::unique_ptr<Writer>(new SQLWriter(getKnob("db"), true));
  pModel    = std::unique_ptr<DataModel>(new DataModel(this));
  pResolver = std::unique_ptr<Resolver>(new Resolver(this, pSymInfo.get()));

  // create Parceive's optional modules
  if (getKnob("variable") == "on") { pVariable = std::unique_ptr<Variable>(new Variable(this)); }
  if (getKnob("routine") == "on") { pRoutine = std::unique_ptr<Routine>(new Routine(this)); }
  if (getKnob("loop") == "on") { pLoop = std::unique_ptr<LoopDetector>(new LoopDetector(this)); }

#ifdef WIN32
  const bool skipLocalBlocks = getKnob("skiplocalblocks") == "on";
  const auto dbgHelpReader   = new dbghelp::ParceiveDbgHelpReader(pFilter.get());
  dbgHelpReader->setParceivePath(parceivePath);
  dbgHelpReader->setSkipLocalBlocks(skipLocalBlocks);
  pSymInfo = std::unique_ptr<SymInfo>(dbgHelpReader);
#else
  pSymInfo = std::unique_ptr<SymInfo>(new dwarf::DwarfReader(pFilter.get()));
#endif

  if (getKnob("thread") == "default") {
    pThread = std::unique_ptr<Thread>(new Thread(this));
#ifndef WIN32
  } else if (getKnob("thread") == "posix") {
    pThread = std::unique_ptr<Thread>(new ThreadPosix(this));
  } else if (getKnob("thread") == "emulate") {
    pThread = std::unique_ptr<Thread>(new ThreadEmulate(this));
#endif
  } else {
    assert(0);
  }

  setAnalysisEnabled(true);
}

Parceive::~Parceive() {}

void Parceive::initializeKnobs()
{
  // 'mode' is no longer used, but the tests still pass it
  // for now, we need the knob so Parceive can start
  // TODO: remove later
  knobMap["thread"] = new KNOB<std::string>(
    KNOB_MODE_WRITEONCE, "pintool", "thread", "posix", "thread module (default, posix, emulate)");

  knobMap["variable"] = new KNOB<std::string>(
    KNOB_MODE_WRITEONCE, "pintool", "variable", "on", "variable module (on, off)");

  knobMap["routine"] = new KNOB<std::string>(
    KNOB_MODE_WRITEONCE, "pintool", "routine", "on", "routine module (on, off)");

  knobMap["loop"] =
    new KNOB<std::string>(KNOB_MODE_WRITEONCE, "pintool", "loop", "off", "loop module (on, off)");

  knobMap["db"] = new KNOB<std::string>(
    KNOB_MODE_WRITEONCE, "pintool", "db", "trace.db", "specify the name of the DB");

  knobMap["single"] = new KNOB<std::string>(
    KNOB_MODE_WRITEONCE, "pintool", "single", "off", "enable single stack reference resolving");

  knobMap["f"] = new KNOB<std::string>(
    KNOB_MODE_WRITEONCE, "pintool", "f", "default", "specify filter file name");

  knobMap["sourcelocation"] = new KNOB<std::string>(
    KNOB_MODE_WRITEONCE, "pintool", "sourcelocation", "off", "lookup source locations");

  knobMap["offsets"] =
    new KNOB<std::string>(KNOB_MODE_WRITEONCE, "pintool", "offsets", "off", "trace access offsets");

  knobMap["optimized"] =
    new KNOB<std::string>(KNOB_MODE_WRITEONCE,
                          "pintool",
                          "optimized",
                          "off",
                          "returns 1 if any compile unit is optimized, otherwise 0");

  knobMap["unknowns"] =
    new KNOB<std::string>(KNOB_MODE_WRITEONCE,
                          "pintool",
                          "unknowns",
                          "off",
                          "handle unresolvable variables as a single unknown reference");

#ifdef WIN32
  knobMap["skiplocalblocks"] = new KNOB<std::string>(
    KNOB_MODE_WRITEONCE,
    "pintool",
    "skiplocalblocks",
    "off",
    "skip deep scanning of basic local blocks which is more unprecise but much faster.");
#endif
}

const std::string &Parceive::getKnob(const std::string &knob)
{
  auto it = knobMap.find(knob);
  assert(it != end(knobMap));
  return (it->second)->Value();
}

void Parceive::registerModule(Module *module) { moduleList.push_back(module); }

bool Parceive::isOptimized() { return pSymInfo->isBinaryOptimized(executable); }

void Parceive::initialize()
{
  // Module initialization
  for (auto module : moduleList) { module->initialize(); }

  // Setup instrumentation
  IMG_AddInstrumentFunction(cb_instrumentImage, this);
  TRACE_AddInstrumentFunction(cb_instrumentTrace, this);
  PIN_AddApplicationStartFunction(cb_programStart, this);
  PIN_AddFiniFunction(cb_programEnd, this);

  for (auto module : moduleList) { module->beforeInstrumentation(); }
}

void Parceive::startProgram() { PIN_StartProgram(); }

void Parceive::usage()
{
  std::cout << "[Info] pin -t <tool> "
               "-thread <thread_module> "
               "-variable <on/off> "
               "-routine <on/off> "
               "-loop <on/off> "
               "-db <database_name> "
               "-single <on/off>"
               "-f <filter_files> "
               "-sourcelocation <on/off> "
               "-offsets <on/off> "
               "-optimized <on/off> "
               "-unknowns <on/off> "
               "-symbols <on/off> "
               "<executable>";
  std::cout << "[Info] Arguments:";
  std::cout << "[Info]\t-t [mandatory] Specifies the Pin tool library.";
  std::cout << "[Info]\t-thread [optional] Specifies the threading module "
               "(options: default, posix, emulate; default: posix).";
  std::cout << "[Info]\t-variable [optional] Specifies if variables accesses shall "
               "be instrumented (default: on).";
  std::cout << "[Info]\t-routine [optional] Specifies if call instructions shall be instrumented "
               "(default: on).";
  std::cout
    << "[Info]\t-loop [optional] Specifies if loops and loop iterations shall be instrumented "
       "(default: off).";
  std::cout << "[Info]\t-db [optional] Specifies the name of the database to be generated "
               "(default: Parceive.db).";
  std::cout
    << "[Info]\t-single [optional] Specifies if stack variables shall be considered per call "
       "(default: off).";
  std::cout << "[Info]\t-f  [optional] Specifies the filter XML file.";
  std::cout << "[Info]\t-sourcelocation [optional] Specifies if line numbers shall be evaluated "
               "(default: on).";
  std::cout
    << "[Info]\t-offsets [optional] Specifies if the offsets of memory accesses shall be traced "
       "(default: off).";
  std::cout
    << "[Info]\t-optimized [optional] The pintool checks if the binary is optimized (return code "
       "1)."
       " If optimized is on, no instrumentation or analysis is done at all. (default: off).";
  std::cout << "[Info]\t-unknowns [optional] Specifies if unresolved symbols regarding memory "
               "accesses shall"
               " be bound to a single reference default: off).";
  std::cout
    << "[Info]\t-symbols [optional] Specifies if diagnostic symbol information shall be written."
       "default: off.";
  std::cout << "[Info]\t<executable>   [mandatory]  Specifies the "
               "executable to be analyzed ";
  // TODO: use the PIN mechanisms to print help on the KNOBS
}

void Parceive::finalize()
{
  for (auto module : moduleList) { module->finalize(); }
}

void Parceive::cb_instrumentImage(IMG img, VOID *v)
{
  auto *pThis = static_cast<Parceive *>(v);
  pThis->instrumentImage(img);
}

void Parceive::cb_instrumentTrace(TRACE trace, VOID *v)
{
  auto *pThis = static_cast<Parceive *>(v);
  pThis->instrumentTrace(trace);
}

void Parceive::cb_programStart(void *v)
{
  auto *pThis = static_cast<Parceive *>(v);
  pThis->pSymInfo->clearSymInfo();
}

void Parceive::cb_programEnd(INT32 /*Code*/, VOID *v)
{
  auto *pThis = static_cast<Parceive *>(v);
  pThis->finalize();
  delete pThis;
}

void Parceive::instrumentImage(IMG img)
{
  for (auto &module : moduleList) { module->beforeInstrumentImage(img); }

  if (!pFilter->isIncluded(img)) { return; }

  readSymbolsOf(img);

  RTN rtn_main = RTN_FindByName(img, "main");
  if (RTN_Valid(rtn_main)) { pFilter->forceInclusion(rtn_main); }

  pModel->processImage(IMG_Id(img), IMG_Name(img));

  for (SEC sec = IMG_SecHead(img); SEC_Valid(sec); sec = SEC_Next(sec))
    for (RTN rtn = SEC_RtnHead(sec); RTN_Valid(rtn); rtn = RTN_Next(rtn)) {
      std::string filename = "";
      if (lookupSourceLocations) {
        pSymInfo->getSourceLocation(RTN_Address(rtn), nullptr, &filename);
      }

      pFilter->precalculateFilterStateFor(rtn, filename);
      if (pFilter->isIncluded(rtn)) { instrumentRoutine(img, rtn); }
    }

    for (auto &module : moduleList) { module->instrumentImage(img); }
}

void Parceive::instrumentRoutine(IMG img, RTN rtn)
{
  for (auto module : moduleList) { module->instrumentRoutine(img, rtn); }

  RTN_Open(rtn);
  for (INS ins = RTN_InsHead(rtn); INS_Valid(ins); ins = INS_Next(ins)) {
    instrumentInstruction(img, rtn, ins);
  }
  RTN_Close(rtn);
}

void Parceive::instrumentInstruction(IMG img, RTN rtn, INS ins)
{
  for (auto module : moduleList) { module->instrumentInstruction(img, rtn, ins); }
}

void Parceive::instrumentTrace(TRACE trace)
{
  IMG img = IMG_FindByAddress(TRACE_Address(trace));
  if (!IMG_Valid(img)) return;

  if (!pFilter->isIncluded(img)) return;

  RTN rtn = TRACE_Rtn(trace);

  if (!RTN_Valid(rtn)) return;

  std::string filename = "";
  if (lookupSourceLocations) { pSymInfo->getSourceLocation(RTN_Address(rtn), nullptr, &filename); }

  const bool instrumentRoutine = pFilter->isIncluded(rtn);

  for (auto module : moduleList) { module->instrumentTrace(img, trace, instrumentRoutine); }
}

bool Parceive::isOptimizationCheckMode() { return (getKnob("optimized") == "on"); }

void Parceive::readSymbolsOf(IMG img) const
{
  const auto imgId = IMG_Id(img);

#ifdef _MSC_VER
  pSymInfo->readSymInfo(IMG_Name(img), imgId, IMG_StartAddress(img));
#elif defined __linux__
  pSymInfo->readSymInfo(IMG_Name(img), imgId, IMG_LoadOffset(img));
#endif

  pResolver->fillGlobalSymbolInfo(imgId);
  pResolver->fillStaticSymbolInfo(imgId);
}

}  // namespace pcv
