/* Copyright 2014 Siemens Technology and Services*/

#include "Routine.h"

#include <cassert>
#include <iostream>

#include "Filter.h"
#include "Parceive.h"
#include "Resolver.h"
#include "Thread.h"

#ifndef WIN32
#include "Type.h"
#endif

static bool isPLT(TRACE trace)
{
  RTN rtn = TRACE_Rtn(trace);

  // All .plt thunks have a valid RTN
  if (!RTN_Valid(rtn)) return false;

  auto name = SEC_Name(RTN_Sec(rtn));
  return (".plt.got" == name || ".plt" == name);
}

namespace pcv {

using pcv::entity::Class;

Routine::Routine(Parceive *parceive)
  : Module(parceive), pModel(nullptr), pResolver(nullptr), pThread(nullptr), pFilter(nullptr)
{}

void Routine::initialize()
{
  pModel    = pParceive->getModel();
  pThread   = pParceive->getThread();
  pResolver = pParceive->getResolver();
  pFilter   = pParceive->getFilter();

  // Disable analysis. We'll enable it again once we see main()
  pParceive->setAnalysisEnabled(false);
}

void Routine::finalize() {}

void Routine::registerInstance(S_ADDRUINT address,
                               const entity::ImageScopedId &classId,
                               pcv::model::Function::Id_t callerRtnId,
                               pcv::model::Call::Id_t callerId,
                               S_ADDRUINT caller_bp,
                               S_ADDRUINT sp) const
{
  Reference_t *ref = nullptr;

  if (!pResolver->getReference(address, &ref)) {
    S_ADDRUINT startAddress;
    SymbolInfo *info = pResolver->getGlobalSymbolInfo(address, &startAddress);
    if (info != nullptr) {
      pResolver->addGlobalReference(address, info, &ref);
    } else {
      if (callerRtnId == model::Function::EMPTY) {
        // We don't know who called us. Can't check for stack symbols.
        return;
      }

#ifdef WIN32
      auto offset = (S_ADDRINT)(address - sp - sizeof(void *));
#else
      auto offset = (S_ADDRINT)(address - caller_bp);
#endif

      info = pResolver->getStackSymbolInfo((UINT32)callerRtnId, offset);

      if (info != nullptr) {
        // we found something on the caller's stack
        pResolver->addStackReference(address, info, callerId, &ref);
      } else {
        return;
      }
    }
  }

  assert(ref);
  pResolver->assignReferenceToClass(address, ref->id, classId);
}

#ifdef WIN32
static ADDRINT getPinAddressFromOriginalCallTarget(const ADDRINT &target)
{
  const auto mnemonic = *(unsigned char *)target;
  if (mnemonic == 0xe9) {  // JMP
    const auto delta      = (unsigned *)(1 + (char *)target);
    const auto pinAddress = (ADDRINT)(((char *)target + 5) + *delta);
    return pinAddress;
  }
  return 0;
}
#endif

static bool isInterestingFilterTarget(pcv::Filter *filter, ADDRINT target)
{
#ifndef WIN32
  return filter->isRoutineAddressExpected(target);
#else
  if (filter->isRoutineAddressExpected(target)) { return true; }

  const auto pinAddress = getPinAddressFromOriginalCallTarget(target);
  return pinAddress != 0 && filter->isRoutineAddressExpected(pinAddress);
#endif
}

static IMG getImageFromRoutineAddress(ADDRINT routineAddr)
{
  PIN_LockClient();
  const RTN targetRtn = RTN_FindByAddress(routineAddr);
  PIN_UnlockClient();
  if (RTN_Valid(targetRtn)) {
    const SEC targetSec = RTN_Sec(targetRtn);
    if (SEC_Valid(targetSec)) { return SEC_Img(targetSec); }
  }
  return IMG_Invalid();
}

void Routine::cbRoutineEnter(Routine *pRoutine,
                             THREADID threadId,
                             ADDRINT targetIp,
                             ADDRINT returnIp,
                             ADDRINT sp,
                             const ADDRINT *arg0,
                             ADDRINT bp)
{
  assert(targetIp);

#ifdef WIN32
  if (!pRoutine->pFilter->isRoutineAddressExpected(targetIp)) {
    // this can happen on Windows if we entered via an indirect branch since the code
    // preparing this callback had no chance to fix the target address directly
    targetIp = getPinAddressFromOriginalCallTarget(targetIp);
  }
#endif

  R_TIMERTYPE startTime = getTimer();
  const auto rtnInfo    = pRoutine->pFilter->getRtnInfo(targetIp);

  if (rtnInfo == nullptr) { return; }

  Filter::Action lineFilterAction = Filter::NONE;

  if (pRoutine->pParceive->getLookupSourceLocations()) {
    INT32 targetLine = 0;
    string targetFilePath;
    if (pRoutine->pParceive->getSymInfo()->getSourceLocation(
          targetIp, &targetLine, &targetFilePath)) {
      lineFilterAction = pRoutine->pFilter->getLineFilterAction(targetFilePath, targetLine);
    }
  }
  const auto routineID         = rtnInfo->rtnId;
  const auto targetImage       = getImageFromRoutineAddress(targetIp);
  const uint32_t targetImageId = IMG_Valid(targetImage) ? IMG_Id(targetImage) : 0;
  const auto uniqueClassId = pRoutine->pResolver->getUniqueClassId(targetImageId, rtnInfo->rtnName);
  const auto threadData    = pRoutine->pThread->getThreadData(threadId);
  auto callStack           = threadData->getCallStack();
  const auto dynFilterAction = pRoutine->pFilter->getDynamicFilterAction(routineID);
  const bool isPartial       = pRoutine->pFilter->isPartial(targetIp);

  // stop if the execution is already on a partial path
  if (!callStack->empty() && isPartial && callStack->top()->isPartial) return;

  if (!pRoutine->pParceive->getAnalysisEnabled()) {
    // If the analysis is disabled, there are some things we need to do anyway:
    if (uniqueClassId.isValid()) {
      // Constructor calls need to be processed even before main() is executed.
      // They may initialize global objects.
      pRoutine->registerInstance(*arg0, uniqueClassId, model::Function::EMPTY, 0, 0, sp);
    }
    if (dynFilterAction == Filter::ENABLE || lineFilterAction == Filter::START) {
      pRoutine->pParceive->setAnalysisEnabled(true);
    }
  } else {
    if (dynFilterAction == Filter::DISABLE || lineFilterAction == Filter::STOP) {
      pRoutine->pParceive->setAnalysisEnabled(false);
    }
  }
  if (!pRoutine->pParceive->getAnalysisEnabled()) {
    if (!callStack->empty() && lineFilterAction == Filter::NONE &&
        dynFilterAction != Filter::NONE) {
      callStack->top()->skip_depth++;
    }
    return;
  }

  // adjust the stack in case of longjumps
  callStack->adjustStackEnter(sp, pRoutine->pResolver);

  auto callerId = model::Call::EMPTY;
  auto insId    = model::Instruction::EMPTY;

  // create new entry in the instruction table
  if (!callStack->empty()) {
    const auto topFrame = callStack->top();
    callerId            = topFrame->callId;
    (pRoutine->pModel)
      ->processInstruction(Thread::getThreadId(threadId),
                           callerId,
                           model::Instruction::Type::CALL,
                           topFrame->lastCallLineNo,
                           &insId);
  }

  // in case of constructors: assign class information to acquired memory
  if (uniqueClassId.isValid()) {
    const auto callerRtnId =
      callStack->empty() ? model::Function::EMPTY : callStack->top()->routineId;
    pRoutine->registerInstance(*arg0, uniqueClassId, callerRtnId, callerId, bp, sp);
  }

  // create new stack frame
  const auto calleeId = model::Call::getInstance()->nextId();
  callStack->addToStack(unique_ptr<StackFrame>{
    new StackFrame(rtnInfo->rtnId, calleeId, startTime, insId, sp, returnIp, isPartial)});

  // create new call / segment in the trace database
  pRoutine->pModel->processCallEntry(callerId, calleeId, threadId);

  //  threadData->clearAccessCache();
}

void Routine::cbRoutineLeave(
  Routine *pRoutine, unsigned int threadId, S_ADDRUINT routineId, ADDRINT sp, ADDRINT returnIp)
{
  auto endTime    = getTimer();
  auto threadData = pRoutine->pThread->getThreadData(threadId);
  auto callStack  = threadData->getCallStack();

  if (callStack->empty()) return;

  if (!pRoutine->pParceive->getAnalysisEnabled()) {
    if (!callStack->empty()) {
      const auto callFrame = callStack->top();
      if (callFrame->skip_depth > 0) {
        callFrame->skip_depth--;
        if (callFrame->skip_depth == 0) { pRoutine->pParceive->setAnalysisEnabled(true); }
      }
    }
    return;
  }

  callStack->adjustStackLeave(sp, returnIp, pRoutine->pResolver);

  if ((callStack->top()->sp == sp && callStack->top()->returnIP == returnIp) ||
      (pRoutine->isExceptionThrown && callStack->top()->sp == sp)) {
    auto callFrame = callStack->top();

    pRoutine->pModel->processCall(callFrame->callId,
                                  callFrame->instructionId,
                                  Thread::getThreadId(threadId),
                                  routineId,
                                  callFrame->startTime,
                                  endTime);
    callStack->pop(pRoutine->pResolver);

    if (!callStack->empty()) {
      StackFrame *parentFrame = callStack->top();
      if (parentFrame->skip_depth == 0) {
        pRoutine->pModel->processCallReEntry(parentFrame->callId, threadId);
      } else {
        pRoutine->pParceive->setAnalysisEnabled(false);
      }
    }

    pRoutine->isExceptionThrown = false;
  }
}

void Routine::instrumentImage(IMG img)
{
  const char *UNWIND_RAISEEXCEPTION{"_Unwind_RaiseException"};
  const char *MAIN{"main"};
  const char *MAIN_LIBC{"__libc_start_main"};

  static bool mainRoutineInstrumented{false};
  static bool unwindRaiseExceptionInstrumented{false};

  // Instrument main
  if (!mainRoutineInstrumented) {
    RTN rtn = RTN_FindByName(img, MAIN);
    if (rtn == RTN_Invalid()) { rtn = RTN_FindByName(img, MAIN_LIBC); }

    if (rtn != RTN_Invalid()) {
      mainRoutineInstrumented = true;
      RTN_Open(rtn);
      // clang-format off
      RTN_InsertCall(rtn, IPOINT_BEFORE, (AFUNPTR)cbMainEnter,
                     IARG_PTR, this,
                     IARG_UINT32, RTN_Id(rtn),
                     IARG_REG_VALUE, REG_STACK_PTR,
                     IARG_THREAD_ID,
                     IARG_END);
      RTN_InsertCall(rtn, IPOINT_AFTER, (AFUNPTR)cbMainLeave,
                     IARG_CALL_ORDER, CALL_ORDER_LAST,
                     IARG_PTR, this,
                     IARG_THREAD_ID,
                     IARG_END);
      // clang-format on
      RTN_Close(rtn);
    }
  }

  // Instrument unwind_raiseexception
  if (!unwindRaiseExceptionInstrumented) {
    RTN rtn = RTN_FindByName(img, UNWIND_RAISEEXCEPTION);

    if (RTN_Valid(rtn)) {
      unwindRaiseExceptionInstrumented = true;
      RTN_Open(rtn);
      // clang-format off
      RTN_InsertCall(rtn, IPOINT_BEFORE, (AFUNPTR)cbRaiseException,
                     IARG_PTR, this,
                     IARG_END);
      // clang-format on
      RTN_Close(rtn);
    }
  }

  // consider start ip's for all routines in the image
  for (SEC sec = IMG_SecHead(img); SEC_Valid(sec); sec = SEC_Next(sec)) {
    for (RTN rtn = SEC_RtnHead(sec); RTN_Valid(rtn); rtn = RTN_Next(rtn)) {
      if (pFilter->isIncluded(rtn)) {
        auto ip = RTN_Address(rtn);

        pFilter->addExpectedRoutineAddress(ip);
        pFilter->setRtnInfo(ip, std::unique_ptr<RtnInfo>{new RtnInfo(RTN_Id(rtn), RTN_Name(rtn))});
      }
    }
  }
}

void Routine::instrumentTrace(IMG, TRACE trace, bool rtn_instrumented)
{
  if (!rtn_instrumented) return;

  for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl)) {
    INS ins = BBL_InsTail(bbl);

    if (INS_IsCall(ins)) {
      if (pParceive->getLookupSourceLocations()) {
        INT32 lineNo{0};
        const auto insAddress = INS_Address(ins);
        pParceive->getSymInfo()->getSourceLocation(insAddress, &lineNo, nullptr);
        if (lineNo != 0) {
          // clang-format off
          INS_InsertPredicatedCall(ins, IPOINT_BEFORE, (AFUNPTR) cbCaptureCallingLineNo,
                                   IARG_PTR, this,
                                   IARG_THREAD_ID,
                                   IARG_ADDRINT, RTN_Id(RTN_FindByAddress(insAddress)),
                                   IARG_UINT32, (unsigned int) lineNo,
                                   IARG_END);
          // clang-format on
        }
      }

      if (INS_IsDirectBranchOrCall(ins)) {
        auto target = INS_DirectBranchOrCallTargetAddress(ins);

#ifdef WIN32
        if (!pFilter->isRoutineAddressExpected(target)) {
          target = getPinAddressFromOriginalCallTarget(target);
        }
#endif
        auto targetRtn = RTN_FindByAddress(target);
        if (!RTN_Valid(targetRtn)) { continue; }

        if (pFilter->isRoutineAddressExpected(target)) {
          // clang-format off
          INS_InsertIfCall(ins, IPOINT_TAKEN_BRANCH, (AFUNPTR) cbTargetInteresting,
                           IARG_BRANCH_TARGET_ADDR,
                           IARG_PTR, this,
                           IARG_END);
          INS_InsertThenCall(ins, IPOINT_TAKEN_BRANCH, (AFUNPTR)cbRoutineEnter,
                                   IARG_PTR, this,
                                   IARG_THREAD_ID,
                                   IARG_ADDRINT, target,
                                   IARG_RETURN_IP,
                                   IARG_REG_VALUE, REG_STACK_PTR,
                                   IARG_FUNCARG_ENTRYPOINT_REFERENCE, 0,
                                   IARG_REG_VALUE, REG_GBP,
                                   IARG_END);
          // clang-format on
        }
      } else if (INS_IsIndirectBranchOrCall(ins) && !isPLT(trace)) {
        // clang-format off
        INS_InsertIfCall(ins, IPOINT_TAKEN_BRANCH, AFUNPTR(cbTargetInteresting),
                         IARG_BRANCH_TARGET_ADDR,
                         IARG_PTR, this,
                         IARG_END);
        INS_InsertThenCall(ins, IPOINT_TAKEN_BRANCH, AFUNPTR(cbRoutineEnter),
                           IARG_PTR, this,
                           IARG_THREAD_ID,
                           IARG_BRANCH_TARGET_ADDR,
                           IARG_RETURN_IP,
                           IARG_REG_VALUE, REG_STACK_PTR,
                           IARG_FUNCARG_ENTRYPOINT_REFERENCE, 0,
                           IARG_REG_VALUE, REG_GBP,
                           IARG_END);
        // clang-format on
      }
    } else if (isPLT(trace)) {
      // clang-format off
      INS_InsertIfCall(ins, IPOINT_BEFORE, AFUNPTR(cbTargetInteresting),
                       IARG_BRANCH_TARGET_ADDR,
                       IARG_PTR, this,
                       IARG_END);
      INS_InsertThenCall(ins, IPOINT_TAKEN_BRANCH, AFUNPTR(cbRoutineEnter),
                         IARG_PTR, this,
                         IARG_THREAD_ID,
                         IARG_BRANCH_TARGET_ADDR,
                         IARG_RETURN_IP,
                         IARG_REG_VALUE, REG_STACK_PTR,
                         IARG_FUNCARG_ENTRYPOINT_REFERENCE, 0,
                         IARG_REG_VALUE, REG_GBP,
                         IARG_END);
      // clang-format on
    }

    if (INS_IsRet(ins)) {
      const auto rtn = RTN_FindByAddress(TRACE_Address(trace));
      // clang-format off
      INS_InsertCall(ins, IPOINT_BEFORE,
                     (AFUNPTR) cbRoutineLeave,
                     IARG_PTR, this,
                     IARG_THREAD_ID,
                     IARG_UINT32, RTN_Id(rtn),
                     IARG_REG_VALUE, REG_STACK_PTR,
                     IARG_BRANCH_TARGET_ADDR,
                     IARG_END);
      // clang-format on
    }
  }
}

void Routine::cbRaiseException(Routine *pRoutine) { pRoutine->isExceptionThrown = true; }
void Routine::cbMainLeave(Routine *pRoutine, THREADID threadId)
{
  R_TIMERTYPE endTime = getTimer();
  pRoutine->pParceive->setAnalysisEnabled(false);

  auto threadData = pRoutine->pThread->getThreadData(threadId);
  auto callStack  = threadData->getCallStack();

  auto callFrame = callStack->top();

  pRoutine->pModel->processCall(callFrame->callId,
                                callFrame->instructionId,
                                Thread::getThreadId(threadId),
                                callFrame->routineId,
                                callFrame->startTime,
                                endTime);

  callStack->pop(pRoutine->pResolver);
}

void Routine::cbMainEnter(Routine *pRoutine, uint32_t rtnId, ADDRINT sp, THREADID threadId)
{
  R_TIMERTYPE startTime = getTimer();

  auto threadData = pRoutine->pThread->getThreadData(threadId);
  auto callStack  = threadData->getCallStack();

  // create new stack frame
  auto calleeId = model::Call::getInstance()->nextId();
  callStack->addToStack(std::unique_ptr<StackFrame>{new StackFrame(
    rtnId, calleeId, startTime, model::Instruction::EMPTY, sp, model::Instruction::EMPTY)});

  // create new call / segment in the trace database
  pRoutine->pModel->processCallEntry(model::Call::EMPTY, calleeId, threadId);

  pRoutine->pParceive->setAnalysisEnabled(true);
}

ADDRINT Routine::cbTargetInteresting(ADDRINT target, Routine *pRoutine)
{
  return static_cast<ADDRINT>(isInterestingFilterTarget(pRoutine->pFilter, target));
}

void pcv::Routine::cbCaptureCallingLineNo(pcv::Routine *pRoutine,
                                          THREADID threadId,
                                          uint32_t rtnId,
                                          uint32_t lineNo)
{
  const auto threadData = pRoutine->pThread->getThreadData(threadId);
  const auto callStack  = threadData->getCallStack();
  if (!callStack->empty()) {
    const auto topFrame = callStack->top();

    if (topFrame->routineId == rtnId) { topFrame->lastCallLineNo = lineNo; }
  }
  threadData->lineNo = lineNo;
}

}  // namespace pcv
