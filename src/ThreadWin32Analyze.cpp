/* Copyright 2014 Siemens Technology and Services*/

#ifdef _WIN32

#include "PinToolAnalyzeWin32ThreadLibrary.h"

#include <string>
#include <vector>

#include "UtilityFunctions.h"

unsigned int WINDOWS::checkReturn(WINDOWS::DWORD returnValue)
{
  switch (returnValue) {
    case WAIT_OBJECT_0:
      return 0;
    case WAIT_ABANDONED:
      return 1;
    case WAIT_TIMEOUT:
      return 2;
    case WAIT_FAILED:
      return 3;
    default:
      return UINT_MAX;
  }
}

unsigned int WINDOWS::checkReturnWaitForMultipleObjects(WINDOWS::DWORD returnValue,
                                                        S_UINT handleCount)
{
  if (returnValue >= WAIT_OBJECT_0 && returnValue <= WAIT_OBJECT_0 + handleCount - 1)
    return 0;
  else if (returnValue >= WAIT_ABANDONED_0 && returnValue <= WAIT_ABANDONED_0 + handleCount - 1)
    return 1;
  else if (returnValue == WAIT_TIMEOUT)
    return 2;
  else if (returnValue == WAIT_FAILED)
    return 3;
  else
    return UINT_MAX;
}

bool WINDOWS::getHandleID(WINDOWS::DWORD returnValue, S_UINT handleCount, S_UINT *handleId)
{
  bool status = false;
  if (returnValue >= WAIT_OBJECT_0 && returnValue <= WAIT_OBJECT_0 + handleCount - 1) {
    status    = true;
    *handleId = returnValue - WAIT_OBJECT_0;
  } else if (returnValue >= WAIT_ABANDONED_0 && returnValue <= WAIT_ABANDONED_0 + handleCount - 1) {
    *handleId = returnValue - WAIT_ABANDONED_0;
  }
  return status;
}

namespace pcv {

PinToolAnalyzeWin32ThreadLibrary::PinToolAnalyzeWin32ThreadLibrary(void)
{
  PIN_RWMutexInit(&rwMutex);
  PIN_RWMutexInit(&thMutex);
  mainSeen = false;
}

PinToolAnalyzeWin32ThreadLibrary::~PinToolAnalyzeWin32ThreadLibrary(void)
{
  PIN_RWMutexFini(&rwMutex);
  PIN_RWMutexFini(&thMutex);
}

int PinToolAnalyzeWin32ThreadLibrary::searchSyncPrimitivePosVectorMap(char syncPrimitiveType,
                                                                      void **syncPrimitive)
{
  vector<SYNC_PRIMITIVE> syncPrimitiveVector = syncPrimitiveVectorMap[syncPrimitiveType];

  int start = -1, end = static_cast<int>(syncPrimitiveVector.size()), mid;

  if (syncPrimitiveVector.size() == 0) return -1;

  if (*syncPrimitive < syncPrimitiveVector.front().syncPrimitiveName) return -1;

  if (*syncPrimitive > syncPrimitiveVector.back().syncPrimitiveName) return -1;

  while (start <= end) {
    mid = (start + end) / 2;
    if (*syncPrimitive < syncPrimitiveVector[mid].syncPrimitiveName)
      end = mid;
    else if (*syncPrimitive > syncPrimitiveVector[mid].syncPrimitiveName)
      start = mid;
    else
      return mid;
  }

  return -1;
}

void PinToolAnalyzeWin32ThreadLibrary::insertSyncPrimitiveVectorMap(char syncType,
                                                                    void **syncPrimitive,
                                                                    R_TIMERTYPE createTime)
{
  SYNC_PRIMITIVE syncPrimitiveNode;
  syncPrimitiveNode.syncPrimitiveType = syncType;
  syncPrimitiveNode.syncPrimitiveName = *syncPrimitive;
  syncPrimitiveNode.createTime        = createTime;
  if (syncPrimitiveVectorMap.count(syncType) == 0) {
    vector<SYNC_PRIMITIVE> syncPrimitiveVector;
    syncPrimitiveVectorMap[syncType] = syncPrimitiveVector;
    syncPrimitiveVectorMap[syncType].push_back(syncPrimitiveNode);
  } else {
    syncPrimitiveVectorMap[syncType].push_back(syncPrimitiveNode);
    int curSize = static_cast<int>(syncPrimitiveVectorMap[syncType].size());
    int i       = 0;

    while (curSize - i - 2 >= 0 &&
           (syncPrimitiveVectorMap[syncType][curSize - i - 1].syncPrimitiveName <
            syncPrimitiveVectorMap[syncType][curSize - i - 2].syncPrimitiveName)) {
      SYNC_PRIMITIVE temp = syncPrimitiveVectorMap[syncType][curSize - i - 1];
      syncPrimitiveVectorMap[syncType][curSize - i - 1] =
        syncPrimitiveVectorMap[syncType][curSize - i - 2];
      syncPrimitiveVectorMap[syncType][curSize - i - 2] = temp;
      i++;
    }
  }
}

bool PinToolAnalyzeWin32ThreadLibrary::searchSyncPrimitiveVectorMap(char syncPrimitiveType,
                                                                    void **syncPrimitive,
                                                                    R_TIMERTYPE *createTime)
{
  int pos = searchSyncPrimitivePosVectorMap(syncPrimitiveType, syncPrimitive);
  if (pos != -1) {
    *createTime = syncPrimitiveVectorMap[syncPrimitiveType][pos].createTime;
    return true;
  }
  return false;
}

void PinToolAnalyzeWin32ThreadLibrary::insertThreadHandleMap(WINDOWS::HANDLE threadHandle,
                                                             WINDOWS::DWORD osThreadID,
                                                             WINDOWS::DWORD windowsParentTHREADID)
{
  WINTHREADIDHANDLE THREADIDHandle;
  THREADIDHandle.threadHandle               = threadHandle;
  THREADIDHandle.threadID                   = osThreadID;
  THREADIDHandle.handleOwnerWindowsThreadID = windowsParentTHREADID;
  bool handlePresent                        = false;
  PIN_RWMutexWriteLock(&thMutex);
  for (unsigned int i = 0; i < threadIDVector.size(); i++) {
    if (threadIDVector.at(i).threadHandle == threadHandle &&
        threadIDVector.at(i).handleOwnerWindowsThreadID == windowsParentTHREADID) {
      handlePresent                 = true;
      threadIDVector.at(i).threadID = osThreadID;
    }
  }
  if (!handlePresent) threadIDVector.push_back(THREADIDHandle);
  PIN_RWMutexUnlock(&thMutex);
}

bool PinToolAnalyzeWin32ThreadLibrary::searchThreadHandleMap(WINDOWS::DWORD osThreadID,
                                                             WINDOWS::HANDLE *handle)
{
  bool status = false;
  PIN_RWMutexReadLock(&thMutex);
  for (unsigned int i = 0; i < threadIDVector.size(); i++) {
    if (threadIDVector.at(i).threadID == osThreadID) {
      status  = true;
      *handle = threadIDVector.at(i).threadHandle;
      break;
    }
  }
  PIN_RWMutexUnlock(&thMutex);
  return status;
}

bool PinToolAnalyzeWin32ThreadLibrary::searchThreadHandleMap(WINDOWS::HANDLE handle,
                                                             WINDOWS::DWORD handleOwnerosThreadID,
                                                             WINDOWS::DWORD *osThreadID)
{
  bool status = false;
  *osThreadID = UINT_MAX;
  PIN_RWMutexReadLock(&thMutex);
  for (unsigned int i = 0; i < threadIDVector.size(); i++) {
    if (threadIDVector.at(i).threadHandle == handle &&
        threadIDVector.at(i).handleOwnerWindowsThreadID == handleOwnerosThreadID) {
      status      = true;
      *osThreadID = threadIDVector.at(i).threadID;
      break;
    }
  }
  if (!status) {
    for (S_UINT i = threadIDVector.size(); i > 0; i--) {
      if (threadIDVector.at(i - 1).threadHandle == handle) {
        status      = true;
        *osThreadID = threadIDVector.at(i - 1).threadID;
        break;
      }
    }
  }
  PIN_RWMutexUnlock(&thMutex);
  return status;
}

bool PinToolAnalyzeWin32ThreadLibrary::removeSyncPrimitiveVectorMap(void **syncPrimitive)
{
  char syncPrimitiveType = 'M';

  int pos = searchSyncPrimitivePosVectorMap(syncPrimitiveType, syncPrimitive);
  if (pos == -1) return false;
  vector<SYNC_PRIMITIVE>::iterator iter = syncPrimitiveVectorMap[syncPrimitiveType].begin();
  int i                                 = 0;
  while (iter != syncPrimitiveVectorMap[syncPrimitiveType].end()) {
    if ((*iter).syncPrimitiveName == *syncPrimitive) {
      iter = syncPrimitiveVectorMap[syncPrimitiveType].erase(iter);
      break;
    } else {
      ++i;
      ++iter;
    }
  }
  return true;
}

bool PinToolAnalyzeWin32ThreadLibrary::removeSyncPrimitiveVectorMap(char syncPrimitiveType,
                                                                    void **syncPrimitive)
{
  int pos = searchSyncPrimitivePosVectorMap(syncPrimitiveType, syncPrimitive);
  if (pos == -1) return false;
  vector<SYNC_PRIMITIVE>::iterator iter = syncPrimitiveVectorMap[syncPrimitiveType].begin();
  int i                                 = 0;
  while (iter != syncPrimitiveVectorMap[syncPrimitiveType].end()) {
    if ((*iter).syncPrimitiveName == *syncPrimitive) {
      iter = syncPrimitiveVectorMap[syncPrimitiveType].erase(iter);
      break;
    } else {
      ++i;
      ++iter;
    }
  }
  return true;
}

bool PinToolAnalyzeWin32ThreadLibrary::checkReturnIP(ADDRINT returnIP)
{
  PIN_LockClient();
  IMG img = IMG_FindByAddress(returnIP);
  std::string fName;
  PIN_GetSourceLocation(returnIP, NULL, NULL, &fName);
  PIN_UnlockClient();
  if (!IMG_Valid(img)) return false;
  std::string imgName = IMG_Name(img);
  std::string fileName;
  std::string::iterator it1;
  for (it1 = imgName.begin(); it1 != imgName.end(); ++it1) {
    switch (*it1) {
      case '\\':
        fileName += "\\\\";
        break;
      default:
        fileName += *it1;
    }
  }
  int position = static_cast<int>(fileName.find_last_of("\\"));

  if (!filterObject->checkValidity(FILEPATH, fName)) {
    /*Add vctools check here, winnt.h, vc/bin etc*/
    return false;
  }
  std::string file = fileName.substr(position + 1, fileName.length() - (position + 1));
  if (!filterObject->checkValidity(IMAGE, file)) {
    return false;
  }
  if (!(mainSeen || file == "PocoFoundationd.dll" || file == "PocoFoundation.dll" ||
        file == "PocoFoundation64.dll" || file == "PocoFoundation64d.dll"))
    return false;

#if defined TRACING_LEVEL1
  BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] File: " << file << " FName: " << fName;
#endif
  return true;
}

void afterCreateMutexA(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                       THREADID THREADID,
                       ADDRINT returnIP,
                       WINDOWS::BOOL initialOwner,
                       WINDOWS::LPCTSTR mutexName,
                       WINDOWS::HANDLE *mutexReturned)
{
  if (!threadLibraryAnalysisObject->mainSeen) return;
  if (*mutexReturned == NULL) {
    // The mutex was not created......
    // Need not check the error message.....
    return;
  } else {
    if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;

    R_TIMERTYPE createTime = getTimer();
    PIN_RWMutexWriteLock(&((*threadLibraryAnalysisObject).rwMutex));
    threadLibraryAnalysisObject->insertSyncPrimitiveVectorMap('M', mutexReturned, createTime);
    PIN_RWMutexUnlock(&(threadLibraryAnalysisObject->rwMutex));
    std::stringstream referenceId, variableName;
    referenceId << "M_";
    referenceId << *mutexReturned << "_" << createTime;
    if (mutexName != NULL)
      variableName << mutexName;
    else
      variableName << *mutexReturned;
    // Adding an entry into the database.....
    std::string refIdString                = referenceId.str();
    char *refIdCString                     = const_cast<char *>(refIdString.c_str());
    refIdCString[refIdString.length()]     = '\0';
    std::string varNameString              = variableName.str();
    char *varNameCString                   = const_cast<char *>(varNameString.c_str());
    varNameCString[varNameString.length()] = '\0';
    threadLibraryAnalysisObject->query->processReference(Reference::getInstance()->nextId(),
                                                         sizeof(*mutexReturned),
                                                         Reference::Type::HEAP,
                                                         &varNameCString,
                                                         returnIP);
#if defined TRACING_LEVEL2
    BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] Inserting MutexA: " << varNameCString;
#endif
  }
}

void afterCreateMutexW(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                       THREADID THREADID,
                       ADDRINT returnIP,
                       WINDOWS::BOOL initialOwner,
                       WINDOWS::LPCTSTR mutexName,
                       WINDOWS::HANDLE *mutexReturned)
{
  if (!threadLibraryAnalysisObject->mainSeen) return;
  if (*mutexReturned == NULL) {
    // The mutex was not created......
    // Need not check the error message.....
    return;
  } else {
    if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;

    R_TIMERTYPE createTime = getTimer();
    PIN_RWMutexWriteLock(&((*threadLibraryAnalysisObject).rwMutex));
    threadLibraryAnalysisObject->insertSyncPrimitiveVectorMap('M', mutexReturned, createTime);
    PIN_RWMutexUnlock(&(threadLibraryAnalysisObject->rwMutex));
    std::stringstream referenceId, variableName;
    referenceId << "M_";
    referenceId << *mutexReturned << "_" << createTime;
    if (mutexName != NULL)
      variableName << mutexName;
    else
      variableName << *mutexReturned;
    // Adding an entry into the database.....
    std::string refIdString                = referenceId.str();
    char *refIdCString                     = const_cast<char *>(refIdString.c_str());
    refIdCString[refIdString.length()]     = '\0';
    std::string varNameString              = variableName.str();
    char *varNameCString                   = const_cast<char *>(varNameString.c_str());
    varNameCString[varNameString.length()] = '\0';
    threadLibraryAnalysisObject->query->processReference(Reference::getInstance()->nextId(),
                                                         sizeof(*mutexReturned),
                                                         Reference::Type::HEAP,
                                                         &varNameCString,
                                                         returnIP);
#if defined TRACING_LEVEL2
    BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] Inserting MutexW: " << varNameCString;
#endif
  }
}

void beforeReleaseMutex(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                        THREADID THREADID,
                        WINDOWS::HANDLE *mutexName,
                        ADDRINT returnIP)
{
  if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;
  ThreadAnalyze *threadAnalysisObject = threadLibraryAnalysisObject->threadAnalyzeObject;
  ThreadTLS *threadTLSObject          = threadAnalysisObject->getTLSData(THREADID);
  threadTLSObject->mutexName          = *mutexName;
}

void afterReleaseMutex(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                       THREADID THREADID,
                       WINDOWS::BOOL retValue,
                       ADDRINT returnIP)
{
  if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;
  ThreadAnalyze *threadAnalysisObject = threadLibraryAnalysisObject->threadAnalyzeObject;
  ThreadTLS *threadTLSObject          = threadAnalysisObject->getTLSData(THREADID);
  void *mutexName                     = threadTLSObject->mutexName;
  R_TIMERTYPE createTime;
  PIN_RWMutexReadLock(&((*threadLibraryAnalysisObject).rwMutex));
  bool returnedValue =
    threadLibraryAnalysisObject->searchSyncPrimitiveVectorMap('M', &mutexName, &createTime);
  PIN_RWMutexUnlock(&(threadLibraryAnalysisObject->rwMutex));
  // Insert into the database....
  // That the mutex was captured.....
  // As of now do not distinguish between abandoned mutex and otherwise...
  // cout<<"[Parceive-DEBUG] The mutex "<<*mutexName
  //     <<" was released by thread id "<<THREADID<<endl;
  BOOST_LOG_TRIVIAL(trace) << "[Trace]The mutex " << mutexName << " was released by thread id "
                           << THREADID;
  if (retValue && returnedValue) {
    unsigned long long instructionID;
    std::stringstream referenceId;

    referenceId << "M_" << mutexName << "_" << createTime;
    ThreadData *threadData =
      (*(threadLibraryAnalysisObject->threadDataNodeList))->getThreadData(THREADID);
    ROUTINE_TRACE currentTop         = threadData->getThreadStackTop();
    unsigned long long currentCallID = currentTop.traceID;
    std::string callIdString         = returnCallID(THREADID, currentCallID);
    /*stringstream callIdUnLock;
    callIdUnLock<<"U_"<<callIdString<<"_"<<mutexName;
    std::string st = callIdUnLock.str();*/
    char *callIdCString                  = const_cast<char *>(callIdString.c_str());
    callIdCString[callIdString.length()] = '\0';
    std::string instType                 = "RELEASE";
    char *instTypeCString                = const_cast<char *>(instType.c_str());
    instTypeCString[instType.length()]   = '\0';
    // threadLibraryAnalysisObject->query->processCall(&callIdCString,
    //     ULLONG_MAX,THREADID,INT_MAX,
    //     ULLONG_MAX,ULLONG_MAX,&callId);
    threadLibraryAnalysisObject->query->processInstruction(
      THREADID, currentCallID, Instruction::Type::RELEASE, -1, &instructionID);
    std::string refIdString                  = referenceId.str();
    char *refIdCString                       = const_cast<char *>(refIdString.c_str());
    refIdCString[refIdString.length()]       = '\0';
    std::string memStateString               = "rw-shared";
    char *memStateCString                    = const_cast<char *>(memStateString.c_str());
    memStateCString[memStateString.length()] = '\0';
    threadLibraryAnalysisObject->query->processVarAccess(THREADID,
                                                         instructionID,
                                                         0,
                                                         Reference::getInstance()->nextId(),
                                                         Access::Type::WRITE,
                                                         Access::State::READ_WRITE_SHARED);

#if defined TRACING_LEVEL2
    BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] Release Mutex called for: " << refIdCString;
#endif
  }
}

void beforeCloseHandle(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                       THREADID THREADID,
                       WINDOWS::HANDLE *mutexName,
                       ADDRINT returnIP)
{
  if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;
  ThreadAnalyze *threadAnalysisObject = threadLibraryAnalysisObject->threadAnalyzeObject;
  ThreadTLS *threadTLSObject          = threadAnalysisObject->getTLSData(THREADID);
  threadTLSObject->mutexName          = *mutexName;
}

void afterCloseHandle(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                      THREADID THREADID,
                      WINDOWS::BOOL retValue,
                      ADDRINT returnIP)
{
  if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;
  ThreadAnalyze *threadAnalysisObject = threadLibraryAnalysisObject->threadAnalyzeObject;
  ThreadTLS *threadTLSObject          = threadAnalysisObject->getTLSData(THREADID);
  void *mutexName                     = threadTLSObject->mutexName;
  if (!retValue) {
    // The mutex was not closed properly......
    // Checking the error message.....
  } else {
    PIN_RWMutexWriteLock(&((*threadLibraryAnalysisObject).rwMutex));
    threadLibraryAnalysisObject->removeSyncPrimitiveVectorMap(&mutexName);
    PIN_RWMutexUnlock(&(threadLibraryAnalysisObject->rwMutex));
  }
}

void afterCreateMutexExA(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                         THREADID THREADID,
                         ADDRINT returnIP,
                         WINDOWS::LPCTSTR mutexName,
                         WINDOWS::HANDLE *mutexReturned)
{
  if (!threadLibraryAnalysisObject->mainSeen) return;
  if (*mutexReturned == NULL) {
    // The mutex was not created......
    // Need not check the error message.....
    return;
  } else {
    if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;
    R_TIMERTYPE createTime = getTimer();
    PIN_RWMutexWriteLock(&((*threadLibraryAnalysisObject).rwMutex));
    threadLibraryAnalysisObject->insertSyncPrimitiveVectorMap('M', mutexReturned, createTime);
    PIN_RWMutexUnlock(&(threadLibraryAnalysisObject->rwMutex));
    std::stringstream referenceId, variableName;
    referenceId << "M_" << *mutexReturned << "_" << createTime;
    if (mutexName != NULL)
      variableName << mutexName;
    else
      variableName << *mutexReturned;
    // Adding an entry into the database.....
    std::string refIdString   = referenceId.str();
    char *refIdCString        = const_cast<char *>(refIdString.c_str());
    std::string varNameString = variableName.str();
    char *varNameCString      = const_cast<char *>(varNameString.c_str());
    threadLibraryAnalysisObject->query->processReference(Reference::getInstance()->nextId(),
                                                         sizeof(*mutexReturned),
                                                         Reference::Type::HEAP,
                                                         &refIdCString,
                                                         returnIP);

#if defined TRACING_LEVEL2
    BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] After Mutex: " << refIdCString << " "
                             << varNameCString;
#endif
  }
}

void afterCreateMutexExW(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                         THREADID THREADID,
                         ADDRINT returnIP,
                         WINDOWS::LPCTSTR mutexName,
                         WINDOWS::HANDLE *mutexReturned)
{
  if (!threadLibraryAnalysisObject->mainSeen) return;
  if (*mutexReturned == NULL) {
    // The mutex was not created......
    // Need not check the error message.....
    return;
  } else {
    if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;
    R_TIMERTYPE createTime = getTimer();
    PIN_RWMutexWriteLock(&((*threadLibraryAnalysisObject).rwMutex));
    threadLibraryAnalysisObject->insertSyncPrimitiveVectorMap('M', mutexReturned, createTime);
    PIN_RWMutexUnlock(&(threadLibraryAnalysisObject->rwMutex));
    std::stringstream referenceId, variableName;
    referenceId << "M_";
    referenceId << *mutexReturned << "_" << createTime;
    // referenceId<<*mutexReturned;
    if (mutexName != NULL)
      variableName << mutexName;
    else
      variableName << *mutexReturned;
    // Adding an entry into the database.....
    std::string refIdString   = referenceId.str();
    char *refIdCString        = const_cast<char *>(refIdString.c_str());
    std::string varNameString = variableName.str();
    char *varNameCString      = const_cast<char *>(varNameString.c_str());
    threadLibraryAnalysisObject->query->processReference(Reference::getInstance()->nextId(),
                                                         sizeof(*mutexReturned),
                                                         Reference::Type::HEAP,
                                                         &varNameCString,
                                                         returnIP);

#if defined TRACING_LEVEL2
    BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] After Mutex: " << refIdCString << " "
                             << varNameCString;
#endif
  }
}

void beforeInitializeCriticalSection(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                                     THREADID THREADID,
                                     void **csName,
                                     ADDRINT returnIP)
{
  if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;
  ThreadAnalyze *threadAnalysisObject = threadLibraryAnalysisObject->threadAnalyzeObject;
  ThreadTLS *threadTLSObject          = threadAnalysisObject->getTLSData(THREADID);
  threadTLSObject->csName             = *csName;
}

void afterInitializeCriticalSection(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                                    THREADID THREADID,
                                    ADDRINT returnIP)
{
  if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;

  ThreadAnalyze *threadAnalysisObject = threadLibraryAnalysisObject->threadAnalyzeObject;
  ThreadTLS *threadTLSObject          = threadAnalysisObject->getTLSData(THREADID);
  void *csName                        = threadTLSObject->csName;

  R_TIMERTYPE createTime = getTimer();
  PIN_RWMutexWriteLock(&((*threadLibraryAnalysisObject).rwMutex));
  threadLibraryAnalysisObject->insertSyncPrimitiveVectorMap('C', &csName, createTime);
  PIN_RWMutexUnlock(&(threadLibraryAnalysisObject->rwMutex));

  std::stringstream referenceId, variableName;
  referenceId.str("");
  referenceId.clear();
  variableName.str("");
  variableName.clear();
  referenceId << "CS_";
  referenceId << csName << "_" << createTime;
  variableName << csName;

  // Adding an entry into the database.....
  std::string refIdString                = referenceId.str();
  char *refIdCString                     = const_cast<char *>(refIdString.c_str());
  refIdCString[refIdString.length()]     = '\0';
  std::string varNameString              = variableName.str();
  char *varNameCString                   = const_cast<char *>(varNameString.c_str());
  varNameCString[varNameString.length()] = '\0';
  threadLibraryAnalysisObject->query->processReference(Reference::getInstance()->nextId(),
                                                       sizeof(csName),
                                                       Reference::Type::HEAP,
                                                       &varNameCString,
                                                       returnIP);
#if defined TRACING_LEVEL2
  BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] After Initialize Critical Section: " << refIdCString
                           << " " << varNameCString;
#endif
}

void beforeInitializeCriticalSectionAndSpinCount(
  PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
  THREADID THREADID,
  void **csName,
  ADDRINT returnIP)
{
  if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;

  ThreadAnalyze *threadAnalysisObject = threadLibraryAnalysisObject->threadAnalyzeObject;
  ThreadTLS *threadTLSObject          = threadAnalysisObject->getTLSData(THREADID);
  threadTLSObject->csName             = *csName;
}

void afterInitializeCriticalSectionAndSpinCount(
  PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
  THREADID THREADID,
  ADDRINT returnIP)
{
  if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;

  ThreadAnalyze *threadAnalysisObject = threadLibraryAnalysisObject->threadAnalyzeObject;
  ThreadTLS *threadTLSObject          = threadAnalysisObject->getTLSData(THREADID);
  void *csName                        = threadTLSObject->csName;

  R_TIMERTYPE createTime = getTimer();
  PIN_RWMutexWriteLock(&((*threadLibraryAnalysisObject).rwMutex));
  threadLibraryAnalysisObject->insertSyncPrimitiveVectorMap('C', &csName, createTime);
  PIN_RWMutexUnlock(&(threadLibraryAnalysisObject->rwMutex));

  std::stringstream referenceId, variableName;
  referenceId.str("");
  referenceId.clear();
  variableName.str("");
  variableName.clear();

  referenceId << "CS_" << csName << "_" << createTime;

  variableName << csName;
  // Adding an entry into the database.....
  std::string refIdString                = referenceId.str();
  char *refIdCString                     = const_cast<char *>(refIdString.c_str());
  refIdCString[refIdString.length()]     = '\0';
  std::string varNameString              = variableName.str();
  char *varNameCString                   = const_cast<char *>(varNameString.c_str());
  varNameCString[varNameString.length()] = '\0';
  threadLibraryAnalysisObject->query->processReference(Reference::getInstance()->nextId(),
                                                       sizeof(csName),
                                                       Reference::Type::HEAP,
                                                       &varNameCString,
                                                       returnIP);

#if defined TRACING_LEVEL2
  BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] After Initialize Critical Section and Spin Count: "
                           << refIdCString << " " << varNameCString;
#endif
}

void beforeEnterCriticalSection(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                                THREADID THREADID,
                                void **csName,
                                ADDRINT returnIP)
{
  if (!threadLibraryAnalysisObject->mainSeen) return;
  if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;
  ThreadAnalyze *threadAnalysisObject = threadLibraryAnalysisObject->threadAnalyzeObject;
  ThreadTLS *threadTLSObject          = threadAnalysisObject->getTLSData(THREADID);
  threadTLSObject->csName             = *csName;
}

void afterEnterCriticalSection(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                               THREADID THREADID,
                               ADDRINT returnIP)
{
  if (!threadLibraryAnalysisObject->mainSeen) return;
  if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;

  // Adding an entry into the database.....
  ThreadAnalyze *threadAnalysisObject = threadLibraryAnalysisObject->threadAnalyzeObject;
  ThreadTLS *threadTLSObject          = threadAnalysisObject->getTLSData(THREADID);
  void *csName                        = threadTLSObject->csName;

  R_TIMERTYPE createTime;
  PIN_RWMutexReadLock(&((*threadLibraryAnalysisObject).rwMutex));
  bool returnedValue =
    threadLibraryAnalysisObject->searchSyncPrimitiveVectorMap('C', &csName, &createTime);
  PIN_RWMutexUnlock(&(threadLibraryAnalysisObject->rwMutex));

  if (!returnedValue) return;

  unsigned long long instructionID;
  std::stringstream referenceId;
  referenceId.str("");
  referenceId.clear();
  referenceId << "CS_" << csName << "_" << createTime;
  // std::cout<<referenceId.str()<<" "<<referenceId.str().length()<<endl;

  ThreadData *tData = (*(threadLibraryAnalysisObject->threadDataNodeList))->getThreadData(THREADID);
  ROUTINE_TRACE currentTop         = tData->getThreadStackTop();
  unsigned long long currentCallID = currentTop.traceID;
  std::string callIdString         = returnCallID(THREADID, currentCallID);
  /*stringstream callIdLock;
  callIdLock.str("");
  callIdLock.clear();
  callIdLock<<"L_"<<callIdString<<"_"<<referenceId.str();*/
  // std::cout<<callIdLock.str()<<" "<<callIdLock.str().length()<<endl;

  // threadLibraryAnalysisObject->query->processInstruction(callIdLock.str(),
  //     "LOCK",-1,&instructionID);
  // std::string st = callIdLock.str();
  char *callIdCString                  = const_cast<char *>(callIdString.c_str());
  callIdCString[callIdString.length()] = '\0';
  std::string instType                 = "CSENTER";
  char *instTypeCString                = const_cast<char *>(instType.c_str());
  instTypeCString[instType.length()]   = '\0';
  threadLibraryAnalysisObject->query->processInstruction(
    THREADID, currentCallID, Instruction::Type::ACQUIRE, -1, &instructionID);
  // threadLibraryAnalysisObject->query->processCall(&callIdCString,
  //    instructionID,THREADID,INT_MAX,
  //    ULLONG_MAX,ULLONG_MAX,&callId);
  std::string refIdString    = referenceId.str();
  char *refIdCString         = const_cast<char *>(refIdString.c_str());
  std::string memStateString = "rw-shared";
  char *memStateCString      = const_cast<char *>(memStateString.c_str());
  threadLibraryAnalysisObject->query->processVarAccess(THREADID,
                                                       instructionID,
                                                       0,
                                                       Reference::getInstance()->nextId(),
                                                       Access::Type::WRITE,
                                                       Access::State::READ_WRITE_SHARED);

#if defined TRACING_LEVEL2
  BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] After Enter Critical Section: " << refIdCString << " "
                           << memStateCString;
#endif
}

void beforeTryEnterCriticalSection(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                                   THREADID THREADID,
                                   void **csName,
                                   ADDRINT returnIP)
{
  if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;

  ThreadAnalyze *threadAnalysisObject = threadLibraryAnalysisObject->threadAnalyzeObject;
  ThreadTLS *threadTLSObject          = threadAnalysisObject->getTLSData(THREADID);
  threadTLSObject->csName             = *csName;
}

void afterTryEnterCriticalSection(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                                  THREADID THREADID,
                                  WINDOWS::BOOL retValue,
                                  ADDRINT returnIP)
{
  if (!threadLibraryAnalysisObject->mainSeen) return;
  if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;

  ThreadAnalyze *threadAnalysisObject = threadLibraryAnalysisObject->threadAnalyzeObject;
  ThreadTLS *threadTLSObject          = threadAnalysisObject->getTLSData(THREADID);
  void *csName                        = threadTLSObject->csName;

  R_TIMERTYPE createTime;
  PIN_RWMutexReadLock(&((*threadLibraryAnalysisObject).rwMutex));
  bool returnedValue =
    threadLibraryAnalysisObject->searchSyncPrimitiveVectorMap('C', &csName, &createTime);
  PIN_RWMutexUnlock(&(threadLibraryAnalysisObject->rwMutex));

  if (retValue && returnedValue) {
    // Adding an entry into the database.....
    unsigned long long instructionID;
    std::stringstream referenceId;
    referenceId.str("");
    referenceId.clear();
    referenceId << "CS_" << csName << "_" << createTime;
    // std::cout<<referenceId.str()<<" "<<referenceId.str().length()<<endl;

    ThreadData *threadData =
      (*(threadLibraryAnalysisObject->threadDataNodeList))->getThreadData(THREADID);
    ROUTINE_TRACE currentTop         = threadData->getThreadStackTop();
    unsigned long long currentCallID = currentTop.traceID;
    std::string callIdString         = returnCallID(THREADID, currentCallID);
    /*stringstream callIdLock;
    callIdLock.str("");
    callIdLock.clear();
    callIdLock<<"L_"<<callIdString<<"_"<<referenceId.str();*/
    // std::cout<<callIdLock.str()<<" "<<callIdLock.str().length()<<endl;

    // std::string st = callIdLock.str();
    char *callIdCString                  = const_cast<char *>(callIdString.c_str());
    callIdCString[callIdString.length()] = '\0';
    std::string instType                 = "CSTRYENTER";
    char *instTypeCString                = const_cast<char *>(instType.c_str());
    instTypeCString[instType.length()]   = '\0';
    threadLibraryAnalysisObject->query->processInstruction(
      THREADID, currentCallID, Instruction::Type::ACQUIRE, -1, &instructionID);
    // threadLibraryAnalysisObject->query->processCall(&callIdCString,
    //    instructionID,THREADID,INT_MAX,
    //    ULLONG_MAX,ULLONG_MAX,&callId);
    std::string refIdString    = referenceId.str();
    char *refIdCString         = const_cast<char *>(refIdString.c_str());
    std::string memStateString = "rw-shared";
    char *memStateCString      = const_cast<char *>(memStateString.c_str());
    threadLibraryAnalysisObject->query->processVarAccess(THREADID,
                                                         instructionID,
                                                         0,
                                                         Reference::getInstance()->nextId(),
                                                         Access::Type::WRITE,
                                                         Access::State::READ_WRITE_SHARED);

#if defined TRACING_LEVEL2
    BOOST_LOG_TRIVIAL(trace) << " [Trace][Lock] After try Enter Critical Section: " << refIdCString
                             << " " << memStateCString;
#endif
  }
}

void beforeLeaveCriticalSection(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                                THREADID THREADID,
                                void **csName,
                                ADDRINT returnIP)
{
  if (!threadLibraryAnalysisObject->mainSeen) return;
  if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;

  ThreadAnalyze *threadAnalysisObject = threadLibraryAnalysisObject->threadAnalyzeObject;
  ThreadTLS *threadTLSObject          = threadAnalysisObject->getTLSData(THREADID);
  threadTLSObject->csName             = *csName;
}

void afterLeaveCriticalSection(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                               THREADID THREADID,
                               ADDRINT returnIP)
{
  if (!threadLibraryAnalysisObject->mainSeen) return;

  if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;

  // Insert an entry into the database....
  // That the mutex was released......
  ThreadAnalyze *threadAnalysisObject = threadLibraryAnalysisObject->threadAnalyzeObject;
  ThreadTLS *threadTLSObject          = threadAnalysisObject->getTLSData(THREADID);
  void *csName                        = threadTLSObject->csName;

  R_TIMERTYPE createTime;
  PIN_RWMutexReadLock(&((*threadLibraryAnalysisObject).rwMutex));
  bool returnedValue =
    threadLibraryAnalysisObject->searchSyncPrimitiveVectorMap('C', &csName, &createTime);
  PIN_RWMutexUnlock(&(threadLibraryAnalysisObject->rwMutex));

  if (!returnedValue) return;

  unsigned long long instructionID;
  std::stringstream referenceId;
  referenceId.str("");
  referenceId.clear();
  referenceId << "CS_" << csName << "_" << createTime;
  // std::cout<<referenceId.str()<<" "<<referenceId.str().length()<<endl;

  ThreadData *tData = (*(threadLibraryAnalysisObject->threadDataNodeList))->getThreadData(THREADID);
  ROUTINE_TRACE currentTop         = tData->getThreadStackTop();
  unsigned long long currentCallID = currentTop.traceID;
  std::string callIdString         = returnCallID(THREADID, currentCallID);
  /*stringstream callIdUnLock;
  callIdUnLock.str("");
  callIdUnLock.clear();
  callIdUnLock<<"U_"<<callIdString<<"_"<<csName;*/
  // std::cout<<callIdUnLock.str()<<" "<<callIdUnLock.str().length()<<endl;

  // std::string st = callIdUnLock.str();

  char *callIdCString                  = const_cast<char *>(callIdString.c_str());
  callIdCString[callIdString.length()] = '\0';
  std::string instType                 = "CSLEAVE";
  char *instTypeCString                = const_cast<char *>(instType.c_str());
  threadLibraryAnalysisObject->query->processInstruction(
    THREADID, currentCallID, Instruction::Type::RELEASE, -1, &instructionID);
  // threadLibraryAnalysisObject->query->processCall(&callIdCString,
  //    instructionID,THREADID,INT_MAX,
  //    ULLONG_MAX,ULLONG_MAX,&callId);
  std::string refIdString    = referenceId.str();
  char *refIdCString         = const_cast<char *>(refIdString.c_str());
  std::string memStateString = "rw-shared";
  char *memStateCString      = const_cast<char *>(memStateString.c_str());
  threadLibraryAnalysisObject->query->processVarAccess(THREADID,
                                                       instructionID,
                                                       0,
                                                       Reference::getInstance()->nextId(),
                                                       Access::Type::WRITE,
                                                       Access::State::READ_WRITE_SHARED);

#if defined TRACING_LEVEL2
  BOOST_LOG_TRIVIAL(trace) << "[Trace][Lock] After leave Critical Section: " << refIdCString << " "
                           << memStateCString;
#endif
}

void beforeDeleteCriticalSection(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                                 THREADID THREADID,
                                 void **csName,
                                 ADDRINT returnIP)
{
  if (!threadLibraryAnalysisObject->mainSeen) return;

  if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;

  ThreadAnalyze *threadAnalysisObject = threadLibraryAnalysisObject->threadAnalyzeObject;
  ThreadTLS *threadTLSObject          = threadAnalysisObject->getTLSData(THREADID);
  threadTLSObject->csName             = *csName;
}

void afterDeleteCriticalSection(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                                THREADID THREADID,
                                ADDRINT returnIP)
{
  if (!threadLibraryAnalysisObject->mainSeen) return;

  if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;

  ThreadAnalyze *threadAnalysisObject = threadLibraryAnalysisObject->threadAnalyzeObject;
  ThreadTLS *threadTLSObject          = threadAnalysisObject->getTLSData(THREADID);
  void *csName                        = threadTLSObject->csName;

  PIN_RWMutexWriteLock(&((*threadLibraryAnalysisObject).rwMutex));
  threadLibraryAnalysisObject->removeSyncPrimitiveVectorMap('C', &csName);
  PIN_RWMutexUnlock(&(threadLibraryAnalysisObject->rwMutex));
}

void afterCreateThread(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                       THREADID THREADID,
                       WINDOWS::HANDLE threadHandle)
{
  WINDOWS::DWORD parentThread = WINDOWS::GetCurrentThreadId();
  WINDOWS::DWORD childThread  = WINDOWS::GetThreadId(threadHandle);

  /* Insert an entry into the thread handle map. This is used by Fini function
   * to map the handles to the thread Id.
   */
  threadLibraryAnalysisObject->insertThreadHandleMap(threadHandle, childThread, parentThread);

  ThreadAnalyze *threadAnalysisObject = threadLibraryAnalysisObject->threadAnalyzeObject;

  ThreadData *tData = (*(threadLibraryAnalysisObject->threadDataNodeList))->getThreadData(THREADID);

  ROUTINE_TRACE currentTop         = tData->getThreadStackTop();
  unsigned long long currentCallID = currentTop.traceID;

  std::string callIdString = returnCallID(THREADID, currentCallID);
  char *callIdCString      = const_cast<char *>(callIdString.c_str());

  unsigned long long tempInstrId;
  std::string instType  = "THRCREATE";
  char *instTypeCString = const_cast<char *>(instType.c_str());
  threadLibraryAnalysisObject->query->processInstruction(
    THREADID, currentCallID, Instruction::Type::FORK, -1, &tempInstrId);

  threadLibraryAnalysisObject->query->processThread(THREADID, 0, tempInstrId);

  BOOST_LOG_TRIVIAL(trace) << "[Trace][CreateThread] Parent Thread: WT_" << parentThread
                           << " created child thread WT_" << childThread;
}

void beforeWaitForMultipleObjects(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                                  THREADID THREADID,
                                  unsigned int handleCount,
                                  WINDOWS::HANDLE **handles,
                                  WINDOWS::BOOL bWaitAll,
                                  ADDRINT returnIP)
{
  if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;
  ThreadAnalyze *threadAnalysisObject      = threadLibraryAnalysisObject->threadAnalyzeObject;
  ThreadTLS *threadTLSObject               = threadAnalysisObject->getTLSData(THREADID);
  ThreadDataThreadTLS *threadDataTLSObject = threadAnalysisObject->getThreadDataTLSData(THREADID);
  threadTLSObject->mutexName               = *handles;
  unsigned int ownerThread                 = WINDOWS::GetCurrentThreadId();
  threadDataTLSObject->bWaitAll            = bWaitAll ? true : false;
  threadDataTLSObject->handleCount         = handleCount;
}

void afterWaitForMultipleObjects(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
                                 THREADID THREADID,
                                 WINDOWS::DWORD retValue,
                                 ADDRINT returnIP)
{
  if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return;

  ThreadAnalyze *threadAnalysisObject      = threadLibraryAnalysisObject->threadAnalyzeObject;
  ThreadDataThreadTLS *threadDataTLSObject = threadAnalysisObject->getThreadDataTLSData(THREADID);

  S_UINT handleCount = threadDataTLSObject->handleCount;

  // Search for the mutex in the TLS
  // If false, then, search for it in the global map
  unsigned int returnCheck = WINDOWS::checkReturnWaitForMultipleObjects(retValue, handleCount);

  if (returnCheck == 0 || returnCheck == 1) {
    // The returned value is WAIT_OBJECT_0
    // (in case of WaitForMultipleObjects was successful)
    // Or, WAIT_ABANDONED in case of the thread capturing an abandoned mutex
    // An abandoned mutex is a mutex which was not released by a thread
    // before it terminated.

    ThreadTLS *threadTLSObject = threadAnalysisObject->getTLSData(THREADID);
    bool bWaitAll              = threadDataTLSObject->bWaitAll;
    WINDOWS::DWORD ownerThread = WINDOWS::GetCurrentThreadId();
    WINDOWS::HANDLE *handles   = reinterpret_cast<WINDOWS::HANDLE *>(threadTLSObject->mutexName);

    /* Currently the scenario, bWaitAll variable being true is handled.
     *
     * If bWaitAll is true, then the thread which invoked
     * WaitForMultipleObjects() waits for all the objects to be signalled.
     * If bWaitAll is false, then when atleast one of the objects are signalled,
     * WaitForMultipleObjects() returns. From the return value, we cannot
     * determine which objects were signalled and which were not. Hence,
     * handling only the case of bWaitAll being true.
     */

    if (bWaitAll) {
      for (unsigned int i = 0; i < handleCount; i++) {
        // WINDOWS::DWORD waitTHREADID = WINDOWS::GetTHREADID(handle);
        // Search the THREADID array, check whether the handle is a thread or
        // not. If it is not a thread, UINT_MAX is stored.
        WINDOWS::DWORD waitTHREADID;
        threadLibraryAnalysisObject->searchThreadHandleMap(handles[i], ownerThread, &waitTHREADID);
        if (waitTHREADID != UINT_MAX) {
          /* The handle is a threadHandle. Insert an entry into the database
           * related to the thread join.
           */
          ThreadData *threadData =
            (*(threadLibraryAnalysisObject->threadDataNodeList))->getThreadData(THREADID);

          ROUTINE_TRACE currentTop         = threadData->getThreadStackTop();
          unsigned long long currentCallID = currentTop.traceID;

          std::string callIdString = returnCallID(THREADID, currentCallID);
          char *callIdCString      = const_cast<char *>(callIdString.c_str());

          unsigned long long tempInstrId;
          std::string instType  = "THRJOIN";
          char *instTypeCString = const_cast<char *>(instType.c_str());
          threadLibraryAnalysisObject->query->processInstruction(
            THREADID, currentCallID, Instruction::Type::JOIN, -1, &tempInstrId);

          threadLibraryAnalysisObject->query->processThread(THREADID, 0, tempInstrId);

          BOOST_LOG_TRIVIAL(trace) << "[Trace][ThreadJoin] Thread: WT_" << waitTHREADID
                                   << " joined thread PT_" << THREADID;
        } else {
          /* The handle is not a threadHandle. The handle can be mutex,
           * semaphore, condition variable, events....
           */
          WINDOWS::HANDLE handle = handles[i];
          R_TIMERTYPE createTime;
          PIN_RWMutexReadLock(&((*threadLibraryAnalysisObject).rwMutex));
          bool retValue =
            threadLibraryAnalysisObject->searchSyncPrimitiveVectorMap('M', &handle, &createTime);
          PIN_RWMutexUnlock(&(threadLibraryAnalysisObject->rwMutex));
          if (retValue) {
            // Insert an entry into the database, that the mutex was locked
            unsigned long long instructionID;
            std::stringstream referenceId;
            referenceId << "M_" << handle << "_" << createTime;
            BOOST_LOG_TRIVIAL(trace)
              << "[Trace] The mutex " << handle << " was captured by thread id " << THREADID;
            ThreadData *threadData =
              (*(threadLibraryAnalysisObject->threadDataNodeList))->getThreadData(THREADID);
            ROUTINE_TRACE currentTop             = threadData->getThreadStackTop();
            unsigned long long currentCallID     = currentTop.traceID;
            std::string callIdString             = returnCallID(THREADID, currentCallID);
            char *callIdCString                  = const_cast<char *>(callIdString.c_str());
            callIdCString[callIdString.length()] = '\0';
            std::string instType                 = "LOCK";
            char *instTypeCString                = const_cast<char *>(instType.c_str());
            instTypeCString[instType.length()]   = '\0';
            threadLibraryAnalysisObject->query->processInstruction(
              THREADID, currentCallID, Instruction::Type::JOIN, -1, &instructionID);
            std::string refIdString               = referenceId.str();
            char *refIdCString                    = const_cast<char *>(refIdString.c_str());
            refIdCString[refIdString.length()]    = '\0';
            std::string memStateString            = "rw-shared";
            char *stateCString                    = const_cast<char *>(memStateString.c_str());
            stateCString[memStateString.length()] = '\0';
            threadLibraryAnalysisObject->query->processVarAccess(THREADID,
                                                                 instructionID,
                                                                 0,
                                                                 Reference::getInstance()->nextId(),
                                                                 Access::Type::WRITE,
                                                                 Access::State::READ_WRITE_SHARED);
#if defined TRACING_LEVEL2
            BOOST_LOG_TRIVIAL(trace) << "[Trace] Inserting WaitForSingleObject: " << refIdCString
                                     << " " << memStateCString;
#endif
          }
        }
      }
    } else {
      /* bWaitAll is false. WaitForMultipleObjects() returns the smallest
       * handle Id that was signalled. As of now, we check whether this handle
       * is thread or not. If it is a thread, then we enter it into the database
       * as thread join. Else, we do not write anything to the database.
       */
      S_UINT handleId = UINT_MAX;
      if (WINDOWS::getHandleID(retValue, handleCount, &handleId)) {
        /* The retValue is WAIT_OBJECT_0 or WAIT_OBJECT_HandleId
         *
         */
        WINDOWS::DWORD waitTHREADID;
        threadLibraryAnalysisObject->searchThreadHandleMap(
          handles[handleId], ownerThread, &waitTHREADID);
        if (waitTHREADID != UINT_MAX) {
          /* The handle is a threadHandle. Insert an entry into the database
           * related to the thread join.
           */
          ThreadData *threadData =
            (*(threadLibraryAnalysisObject->threadDataNodeList))->getThreadData(THREADID);

          ROUTINE_TRACE currentTop         = threadData->getThreadStackTop();
          unsigned long long currentCallID = currentTop.traceID;

          std::string callIdString = returnCallID(THREADID, currentCallID);
          char *callIdCString      = const_cast<char *>(callIdString.c_str());

          unsigned long long tempInstrId;
          std::string instType  = "THRJOIN";
          char *instTypeCString = const_cast<char *>(instType.c_str());
          threadLibraryAnalysisObject->query->processInstruction(
            THREADID, currentCallID, Instruction::Type::JOIN, -1, &tempInstrId);

          threadLibraryAnalysisObject->query->processThread(THREADID, 0, tempInstrId);

          BOOST_LOG_TRIVIAL(trace) << "[Trace][ThreadJoin] Thread: WT_" << waitTHREADID
                                   << " joined thread PT_" << THREADID;
        } else {
          /* The handle is not a threadHandle. The handle can be mutex,
           * semaphore, condition variable, events....
           */
          WINDOWS::HANDLE handle = handles[handleId];
          R_TIMERTYPE createTime;
          PIN_RWMutexReadLock(&((*threadLibraryAnalysisObject).rwMutex));
          bool retValue =
            threadLibraryAnalysisObject->searchSyncPrimitiveVectorMap('M', &handle, &createTime);
          PIN_RWMutexUnlock(&(threadLibraryAnalysisObject->rwMutex));
          if (retValue) {
            // Insert an entry into the database, that the mutex was locked
            unsigned long long instructionID;
            std::stringstream referenceId;
            referenceId << "M_" << handle << "_" << createTime;
            BOOST_LOG_TRIVIAL(trace)
              << "[Trace] The mutex " << handle << " was captured by thread id " << THREADID;
            ThreadData *threadData =
              (*(threadLibraryAnalysisObject->threadDataNodeList))->getThreadData(THREADID);
            ROUTINE_TRACE currentTop             = threadData->getThreadStackTop();
            unsigned long long currentCallID     = currentTop.traceID;
            std::string callIdString             = returnCallID(THREADID, currentCallID);
            char *callIdCString                  = const_cast<char *>(callIdString.c_str());
            callIdCString[callIdString.length()] = '\0';
            std::string instType                 = "LOCK";
            char *instTypeCString                = const_cast<char *>(instType.c_str());
            instTypeCString[instType.length()]   = '\0';
            threadLibraryAnalysisObject->query->processInstruction(
              THREADID, currentCallID, Instruction::Type::JOIN, -1, &instructionID);
            std::string refIdString               = referenceId.str();
            char *refIdCString                    = const_cast<char *>(refIdString.c_str());
            refIdCString[refIdString.length()]    = '\0';
            std::string memStateString            = "rw-shared";
            char *stateCString                    = const_cast<char *>(memStateString.c_str());
            stateCString[memStateString.length()] = '\0';
            threadLibraryAnalysisObject->query->processVarAccess(THREADID,
                                                                 instructionID,
                                                                 0,
                                                                 Reference::getInstance()->nextId(),
                                                                 Access::Type::WRITE,
                                                                 Access::State::READ_WRITE_SHARED);
#if defined TRACING_LEVEL2
            BOOST_LOG_TRIVIAL(trace)
              << "[Trace] WaitForMultipleObjects: " << refIdCString << " LOCK";
#endif
          }
        }
      } else {
        /* The retValue is WAIT_ABANDONED_0 or WAIT_ABANDONED_HandleId
         *
         */
      }
      /* The signalled state of other handles are unknown. Hence, we are writing
       * into the database, that the state of the handle is unknown.... It
       * depends
       * on the interpreter, to interpret it as a signalled handle or not.
       */
      for (S_UINT i = 0; i < handleCount; i++) {
        if (i == handleId) {
          continue;
        } else {
          WINDOWS::DWORD waitTHREADID;
          threadLibraryAnalysisObject->searchThreadHandleMap(
            handles[i], ownerThread, &waitTHREADID);
          if (waitTHREADID != UINT_MAX) {
            /* The handle is a threadHandle. Insert an entry into the database
             * related to the unknown thread join.
             */
            ThreadData *threadData =
              (*(threadLibraryAnalysisObject->threadDataNodeList))->getThreadData(THREADID);

            ROUTINE_TRACE currentTop         = threadData->getThreadStackTop();
            unsigned long long currentCallID = currentTop.traceID;

            std::string callIdString = returnCallID(THREADID, currentCallID);
            char *callIdCString      = const_cast<char *>(callIdString.c_str());

            unsigned long long tempInstrId;
            std::string instType  = "UTHRJOIN";
            char *instTypeCString = const_cast<char *>(instType.c_str());
            threadLibraryAnalysisObject->query->processInstruction(
              THREADID, currentCallID, Instruction::Type::JOIN, -1, &tempInstrId);

            threadLibraryAnalysisObject->query->processThread(THREADID, 0, tempInstrId);

            BOOST_LOG_TRIVIAL(trace) << "[Trace][UnknownThreadJoin] Thread: WT_" << waitTHREADID
                                     << " join state unknown thread PT_" << THREADID;
          } else {
            /* The handle is not a threadHandle. The handle can be mutex,
             * semaphore, condition variable, events....
             */
            WINDOWS::HANDLE handle = handles[handleId];
            R_TIMERTYPE createTime;
            PIN_RWMutexReadLock(&((*threadLibraryAnalysisObject).rwMutex));
            bool retValue =
              threadLibraryAnalysisObject->searchSyncPrimitiveVectorMap('M', &handle, &createTime);
            PIN_RWMutexUnlock(&(threadLibraryAnalysisObject->rwMutex));
            if (retValue) {
              // Insert an entry into the database, that the mutex state is
              // unknown
              unsigned long long instructionID;
              std::stringstream referenceId;
              referenceId << "M_" << handle << "_" << createTime;
              BOOST_LOG_TRIVIAL(trace)
                << "[Trace] The mutex " << handle << " state is unknown thread id " << THREADID;
              ThreadData *threadData =
                (*(threadLibraryAnalysisObject->threadDataNodeList))->getThreadData(THREADID);
              ROUTINE_TRACE currentTop           = threadData->getThreadStackTop();
              unsigned long long currentCallID   = currentTop.traceID;
              std::string callId                 = returnCallID(THREADID, currentCallID);
              char *callIdCString                = const_cast<char *>(callId.c_str());
              callIdCString[callId.length()]     = '\0';
              std::string instType               = "UNKNOWN";
              char *instTypeCString              = const_cast<char *>(instType.c_str());
              instTypeCString[instType.length()] = '\0';
              threadLibraryAnalysisObject->query->processInstruction(
                THREADID, currentCallID, Instruction::Type::JOIN, -1, &instructionID);
              std::string refIdString                  = referenceId.str();
              char *refIdCString                       = const_cast<char *>(refIdString.c_str());
              refIdCString[refIdString.length()]       = '\0';
              std::string memStateString               = "rw-shared";
              char *memStateCString                    = const_cast<char *>(memStateString.c_str());
              memStateCString[memStateString.length()] = '\0';
              threadLibraryAnalysisObject->query->processVarAccess(
                THREADID,
                instructionID,
                0,
                Reference::getInstance()->nextId(),
                Access::Type::WRITE,
                Access::State::READ_WRITE_SHARED);
#if defined TRACING_LEVEL2
              BOOST_LOG_TRIVIAL(trace)
                << "[Trace] WaitForMultipleObjects: " << refIdCString << " UNKNOWN STATE";
#endif
            }
          }
        }
      }
    }
  }
}

WINDOWS::DWORD waitForSingleObjectWrapper(
  PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject,
  CONTEXT *ctxt,
  AFUNPTR pf_waitForSingleObject,
  THREADID THREADID,
  WINDOWS::HANDLE handle,
  WINDOWS::DWORD time,
  S_ADDRINT returnIP)
{
  WINDOWS::DWORD retValue = 0;

  // todo: make it work
  /* doesn't work
  PIN_CallApplicationFunction(ctxt, THREADID, CALLINGSTD_STDCALL,
  (AFUNPTR)pf_waitForSingleObject,
  PIN_PARG(WINDOWS::DWORD), &retValue,
  PIN_PARG(WINDOWS::HANDLE), handle,
  PIN_PARG(WINDOWS::DWORD), time,
  PIN_PARG_END());	*/

  if (!threadLibraryAnalysisObject->checkReturnIP(returnIP)) return retValue;

  unsigned int returnCheck = WINDOWS::checkReturn(retValue);

  if (returnCheck == 0 || returnCheck == 1) {
    // The returned value is WAIT_OBJECT_0
    // (in case of WaitForSingleObject was successful)
    // Or, WAIT_ABANDONED in case of the thread capturing an abandoned mutex
    // An abandoned mutex is a mutex which was not released by a thread
    // before it terminated.
    ThreadAnalyze *threadAnalysisObject      = threadLibraryAnalysisObject->threadAnalyzeObject;
    ThreadTLS *threadTLSObject               = threadAnalysisObject->getTLSData(THREADID);
    ThreadDataThreadTLS *threadDataTLSObject = threadAnalysisObject->getThreadDataTLSData(THREADID);

    R_TIMERTYPE createTime;
    PIN_RWMutexReadLock(&((*threadLibraryAnalysisObject).rwMutex));
    bool retValue =
      threadLibraryAnalysisObject->searchSyncPrimitiveVectorMap('M', &handle, &createTime);
    PIN_RWMutexUnlock(&(threadLibraryAnalysisObject->rwMutex));
    WINDOWS::DWORD waitTHREADID;
    // Insert into the database....
    // That the mutex was captured.....
    // As of now do not distinguish between abandoned mutex and otherwise...
    // cout<<"[Parceive-DEBUG] The mutex "<<*mutexName
    //     <<" was captured by thread id "<<THREADID<<endl;

    if (retValue) {
      // Adding an entry into the database.....
      unsigned long long instructionID;
      std::stringstream referenceId;
      referenceId << "M_" << handle << "_" << createTime;
      BOOST_LOG_TRIVIAL(trace) << "[Trace]The mutex " << handle << " was captured by thread id "
                               << THREADID;
      ThreadData *threadData =
        (*(threadLibraryAnalysisObject->threadDataNodeList))->getThreadData(THREADID);
      ROUTINE_TRACE currentTop             = threadData->getThreadStackTop();
      unsigned long long currentCallID     = currentTop.traceID;
      std::string callIdString             = returnCallID(THREADID, currentCallID);
      char *callIdCString                  = const_cast<char *>(callIdString.c_str());
      callIdCString[callIdString.length()] = '\0';
      std::string instType                 = "LOCK";
      char *instTypeCString                = const_cast<char *>(instType.c_str());
      instTypeCString[instType.length()]   = '\0';
      threadLibraryAnalysisObject->query->processInstruction(
        THREADID, currentCallID, Instruction::Type::JOIN, -1, &instructionID);
      std::string refIdString                  = referenceId.str();
      char *refIdCString                       = const_cast<char *>(refIdString.c_str());
      refIdCString[refIdString.length()]       = '\0';
      std::string memStateString               = "rw-shared";
      char *memStateCString                    = const_cast<char *>(memStateString.c_str());
      memStateCString[memStateString.length()] = '\0';
      threadLibraryAnalysisObject->query->processVarAccess(THREADID,
                                                           instructionID,
                                                           0,
                                                           Reference::getInstance()->nextId(),
                                                           Access::Type::WRITE,
                                                           Access::State::READ_WRITE_SHARED);
#if defined TRACING_LEVEL2
      BOOST_LOG_TRIVIAL(trace) << "[Trace]WaitForSingleObject: " << refIdCString << " Locked";
#endif
    } else {
      threadLibraryAnalysisObject->searchThreadHandleMap(
        handle, WINDOWS::GetCurrentThreadId(), &waitTHREADID);
      if (waitTHREADID != UINT_MAX) {
        /* Check if the handle is a thread handle. If it is, then it is a thread
         * join operation. Write an entry into the database, that it was a
         * thread join.
         */
        ThreadData *threadData =
          (*(threadLibraryAnalysisObject->threadDataNodeList))->getThreadData(THREADID);

        ROUTINE_TRACE currentTop         = threadData->getThreadStackTop();
        unsigned long long currentCallID = currentTop.traceID;

        std::string callIdString = returnCallID(THREADID, currentCallID);
        char *callIdCString      = const_cast<char *>(callIdString.c_str());

        unsigned long long tempInstrId;
        std::string instType  = "THRJOIN";
        char *instTypeCString = const_cast<char *>(instType.c_str());
        threadLibraryAnalysisObject->query->processInstruction(
          THREADID, currentCallID, Instruction::Type::JOIN, -1, &tempInstrId);

        threadLibraryAnalysisObject->query->processThread(THREADID, 0, tempInstrId);

        BOOST_LOG_TRIVIAL(trace) << "[Trace][ThreadJoin] Thread: WT_" << waitTHREADID
                                 << " joined thread PT_" << THREADID;
      }
    }
  }

  return retValue;
}

void tlibMainEnter(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject)
{
  threadLibraryAnalysisObject->mainSeen = true;
}

void tlibMainLeave(PinToolAnalyzeWin32ThreadLibrary *threadLibraryAnalysisObject)
{
  threadLibraryAnalysisObject->mainSeen = false;
}

}  // namespace pcv

#endif
