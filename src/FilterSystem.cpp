#include "FilterSystem.h"
#include "Common.h"
#include "DebugOptions.h"

#ifdef TRACING_LEVEL1
#define TRACELOG(msg, ...) PLOGP(msg, __VA_ARGS__)  // NOLINT(runtime/printf)
#else
#define TRACELOG(msg, ...)
#endif

using std::string;

namespace pcv {

string toString(const Filter::Action action)
{
  switch (action) {
    case Filter::PARTIAL:
      return "PARTIAL";
    case Filter::INCLUDE:
      return "INCLUDE";
    case Filter::EXCLUDE:
      return "EXCLUDE";
    case Filter::ENABLE:
      return "ENABLE";
    case Filter::DISABLE:
      return "DISABLE";
    case Filter::START:
      return "START";
    case Filter::STOP:
      return "STOP";
    case Filter::NONE:
      return "NONE";
  }
  return "<UNKNOWN>";
}

bool FilterSystem::isImageIncluded(const string& imgName) const
{
  const bool isExcluded = images.isExcluded(imgName);
  TRACELOG("[Filter] %s image %s\n", isExcluded ? "Excluding" : "Including", imgName.c_str());
  return !isExcluded;
}

bool FilterSystem::isRoutineExcluded(const string& routineName) const
{
  const bool isExcluded = regions.isExcluded(routineName);
  TRACELOG("[Filter] %s routine %s\n", isExcluded ? "Excluding" : "Including", routineName.c_str());
  return isExcluded;
}

bool FilterSystem::isRoutinePartial(const std::string& routineName) const
{
  const bool isPartial = regions.isPartial(routineName);
  TRACELOG("[Filter] %s routine %s\n", isPartial ? "Partial" : "Non-Partial", routineName.c_str());
  return isPartial;
}

bool FilterSystem::isFileExcluded(const string& fileName) const
{
  const bool isExcluded = files.isExcluded(fileName);
  TRACELOG("[Filter] %s file %s\n", isExcluded ? "Excluding" : "Including", fileName.c_str());
  return isExcluded;
}

Filter::Action FilterSystem::getDynamicFilterAction(const string& routineName) const
{
  const Filter::Action action = regions.getDynamicFilterAction(routineName);
  TRACELOG(
    "[Filter] Dynamic action for routine %s: %s\n", routineName.c_str(), toString(action).c_str());
  return action;
}

Filter::Action FilterSystem::getLineFilterAction(const string& filePath, const int line) const
{
  const Filter::Action action = files.getLineFilterAction(filePath, line);
  TRACELOG("[Filter] Line filter action for file %s:%d: %s\n",
           filePath.c_str(),
           line,
           toString(action).c_str());
  return action;
}

}  // namespace pcv
