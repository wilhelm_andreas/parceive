/* Copyright 2014 Siemens Technology and Services*/

#include "ThreadEmulate.h"
#include <Resolver.h>

#include "DataModel.h"
#include "Parceive.h"

namespace pcv {

const std::string mutex_lock_str     = "pthread_mutex_lock";
const std::string mutex_unlock_str   = "pthread_mutex_unlock";
const std::string mutex_lock_str_u   = "__pthread_mutex_lock";
const std::string mutex_unlock_str_u = "__pthread_mutex_unlock";

// private types
using start_routine_t = void *(*)(void *);

using namespace model;

ThreadEmulate::ThreadEmulate(Parceive *parceive)
  : Thread(parceive), pModel(nullptr), pResolver(nullptr)
{}

ThreadEmulate::~ThreadEmulate(void) {}

void ThreadEmulate::initialize()
{
  Thread::initialize();
  pModel    = pParceive->getModel();
  pResolver = pParceive->getResolver();
}

void ThreadEmulate::finalize()
{
  // todo: finalize threads from map
  Thread::finalize();
}

void ThreadEmulate::beforeInstrumentation()
{
  PIN_AddThreadStartFunction(ThreadEmulate::cb_threadStart, nullptr);
  PIN_AddThreadFiniFunction(ThreadEmulate::cb_threadFini, nullptr);
}

model::Thread::Id_t ThreadEmulate::getThreadId(const THREADID & /*threadId*/)
{
  return sHandler.threadStack.top();
}

int ThreadEmulate::w_pthreadCreate(ThreadEmulate *pThis,
                                   CONTEXT *context,
                                   pthread_t *thread,
                                   pthread_attr_t * /*attr*/,
                                   void *(*start_routine)(void *),
                                   void *arg,
                                   AFUNPTR /*orig_fptr*/)
{
  THREADID pin_tid = PIN_ThreadId();

  // write a new instruction
  ThreadData *threadTLSObject = pThis->getThreadData(pin_tid);
  Instruction::Id_t insId     = 0;
  pThis->pModel->processInstruction(
    pin_tid, Call::NO_CALL, Instruction::Type::FORK, threadTLSObject->lineNo, &insId);

  // handle thread start routine without pthread call
  std::unique_ptr<threadInfo_t> info(new threadInfo_t());
  info->threadId          = model::Thread::getInstance()->nextId();
  info->processId         = 0;
  info->createInstruction = insId;
  info->start             = getTimer();
  info->result            = malloc(sizeof(void *));

  // set current thread for correct call entries
  pThis->sHandler.threadStack.push(info->threadId);

  // call start routine under PIN instrumentation
  PIN_CallApplicationFunction(context,
                              pin_tid,
                              CALLINGSTD_DEFAULT,
                              (AFUNPTR)start_routine,
                              nullptr,  // do the instrumentation
                              PIN_PARG(void *),
                              info->result,
                              PIN_PARG(void *),
                              arg,
                              PIN_PARG_END());

  // reset current thread for correct call entries
  pThis->sHandler.threadStack.pop();

  info->callId = Call::getInstance()->currentId();
  info->end    = getTimer();

  // set thread id to map pthread_join
  *thread = static_cast<pthread_t>(info->threadId);

  pThis->sHandler.threadMap[info->threadId] = std::move(info);

  return 0;
}

int ThreadEmulate::w_pthreadJoin(ThreadEmulate *pThis,
                                 CONTEXT * /*context*/,
                                 pthread_t thread,
                                 void **result,
                                 AFUNPTR /*orig_funptr*/)
{
  auto entry = pThis->sHandler.threadMap.find(thread);
  if (entry != pThis->sHandler.threadMap.end()) {
    THREADID pin_tid   = PIN_ThreadId();
    threadInfo_t *info = entry->second.get();

    // write a new instruction
    ThreadData *threadTLSObject = pThis->getThreadData(pin_tid);
    Instruction::Id_t insId     = 0;
    pThis->pModel->processInstruction(
      pin_tid, Call::NO_CALL, Instruction::Type::JOIN, threadTLSObject->lineNo, &insId);

    // write the new thread
    pThis->pModel->processThread(pin_tid,
                                 //  info->threadId,
                                 info->start,
                                 info->end,
                                 info->createInstruction,
                                 insId,
                                 pin_tid,
                                 info->callId);

    // emulate pthread_join return
    *result = info->result;
    pThis->sHandler.threadMap.erase(entry);
  }

  return 0;
}

void ThreadEmulate::cb_threadStart(THREADID pin_tid, CONTEXT *context, INT32 code, VOID *v)
{
  Thread::cb_threadStart(pin_tid, context, code, v);

  std::unique_ptr<threadInfo_t> info(new threadInfo_t());
  auto *pThis = static_cast<ThreadEmulate *>(v);

  info->threadId                                 = model::Thread::MAIN;
  info->processId                                = 0;
  info->start                                    = getTimer();
  pThis->sHandler.threadMap[model::Thread::MAIN] = std::move(info);
}

void ThreadEmulate::cb_threadFini(THREADID pin_tid, const CONTEXT *context, INT32 code, VOID *v)
{
  auto *pThis = static_cast<ThreadEmulate *>(v);

  auto it = pThis->sHandler.threadMap.find(model::Thread::MAIN);
  if (it != pThis->sHandler.threadMap.end()) {
    threadInfo_t *info = it->second.get();
    info->end          = getTimer();

    pThis->pModel->processThread(pin_tid,  // model::Thread::MAIN,
                                 info->start,
                                 info->end,
                                 Instruction::EMPTY,
                                 Instruction::EMPTY,
                                 INVALID_THREADID,
                                 Call::MAIN_CALL);
  }
  Thread::cb_threadFini(pin_tid, context, code, v);
}

void ThreadEmulate::instrumentCreate(IMG img)
{
  RTN rtn_ptc = RTN_FindByName(img, "pthread_create");
  if (RTN_Valid(rtn_ptc)) {
    PROTO proto = PROTO_Allocate(PIN_PARG(int),
                                 CALLINGSTD_DEFAULT,
                                 "pthread_create",
                                 PIN_PARG(pthread_t *),
                                 PIN_PARG(pthread_attr_t *),
                                 PIN_PARG(void *(*)(void *)),
                                 PIN_PARG(void *),
                                 PIN_PARG_END());

    RTN_ReplaceSignature(rtn_ptc,
                         AFUNPTR(w_pthreadCreate),
                         IARG_PROTOTYPE,
                         proto,
                         IARG_PTR,
                         this,
                         IARG_CONTEXT,
                         IARG_FUNCARG_ENTRYPOINT_VALUE,
                         0,
                         IARG_FUNCARG_ENTRYPOINT_VALUE,
                         1,
                         IARG_FUNCARG_ENTRYPOINT_VALUE,
                         2,
                         IARG_FUNCARG_ENTRYPOINT_VALUE,
                         3,
                         IARG_ORIG_FUNCPTR,
                         IARG_END);
  }
}

void ThreadEmulate::instrumentJoin(IMG img)
{
  RTN rtn_ptj = RTN_FindByName(img, "pthread_join");
  if (RTN_Valid(rtn_ptj)) {
    PROTO proto = PROTO_Allocate(PIN_PARG(int),
                                 CALLINGSTD_DEFAULT,
                                 "pthread_join",
                                 PIN_PARG(pthread_t),
                                 PIN_PARG(void **),
                                 PIN_PARG_END());

    RTN_ReplaceSignature(rtn_ptj,
                         AFUNPTR(w_pthreadJoin),
                         IARG_PROTOTYPE,
                         proto,
                         IARG_PTR,
                         this,
                         IARG_CONTEXT,
                         IARG_FUNCARG_ENTRYPOINT_VALUE,
                         0,
                         IARG_FUNCARG_ENTRYPOINT_REFERENCE,
                         1,
                         IARG_ORIG_FUNCPTR,
                         IARG_END);
  }
}

void ThreadEmulate::instrumentLock(IMG img)
{
  RTN rtn = RTN_FindByName(img, mutex_lock_str.c_str());
  if (RTN_Valid(rtn)) {
    RTN_Open(rtn);
    RTN_InsertCall(rtn,
                   IPOINT_BEFORE,
                   (AFUNPTR)an_pthreadMutexLock,
                   IARG_PTR,
                   this,
                   IARG_THREAD_ID,
                   IARG_FUNCARG_ENTRYPOINT_REFERENCE,
                   0,
                   IARG_END);
    RTN_Close(rtn);

    // add routine to function table
    sHandler.mutex_lock_id = RTN_Id(rtn);
    pModel->processRoutine(
      0, mutex_lock_str, RTN_Name(rtn), "", Function::Type::ACQUIRE, 0, sHandler.mutex_lock_id, 0);
  }
}

void ThreadEmulate::instrumentUnlock(IMG img)
{
  RTN rtn = RTN_FindByName(img, mutex_unlock_str.c_str());
  if (RTN_Valid(rtn)) {
    RTN_Open(rtn);
    RTN_InsertCall(rtn,
                   IPOINT_BEFORE,
                   (AFUNPTR)an_pthreadMutexUnlock,
                   IARG_PTR,
                   this,
                   IARG_THREAD_ID,
                   IARG_FUNCARG_ENTRYPOINT_REFERENCE,
                   0,
                   IARG_END);
    RTN_Close(rtn);

    // add routine to function table
    sHandler.mutex_unlock_id = RTN_Id(rtn);
    pModel->processRoutine(0,
                           mutex_unlock_str,
                           RTN_Name(rtn),
                           "",
                           Function::Type::RELEASE,
                           0,
                           sHandler.mutex_unlock_id,
                           0);
  }
}

void ThreadEmulate::instrumentImage(IMG img)
{
  if (!IMG_Valid(img)) return;

  const std::string &imgName = IMG_Name(img);
  if (imgName.find("libpthread.so") != std::string::npos) {
    instrumentCreate(img);
    instrumentJoin(img);
    instrumentLock(img);
    instrumentUnlock(img);
  }
}

Reference::Id_t ThreadEmulate::getReferenceId(THREADID threadId, const ADDRINT *mutex)
{
  Reference_t *reference;

  if (!pResolver->getReference(*mutex, &reference)) {
    pResolver->addAllocatedReference(
      *mutex,
      std::unique_ptr<SymbolInfo>(
        new SymbolInfo("mutex", sizeof(pthread_mutex_t), entity::ImageScopedId::getNone(), 0, 0)),
      0,
      &reference);

    pModel->processReference(reference->id,
                             threadId,
                             sizeof(pthread_mutex_t),
                             Reference::Type::HEAP,
                             "mutex",
                             0,
                             reference->info->symbolId,
                             reference->info->classId.imageId);
  }

  return reference->id;
}

void ThreadEmulate::an_pthreadMutexLock(ThreadEmulate *pThis, THREADID threadId, ADDRINT *mutex)
{
  Reference::Id_t refId = pThis->getReferenceId(threadId, mutex);
  int lineNo            = 0;  // todo: resolve real line number by instruction
  Instruction::Id_t insId;

  // get call id from top of the trace stack
  auto threadData = pThis->getThreadData(threadId);
  auto callStack  = threadData->getCallStack();

  if (!callStack->empty()) {
    pThis->pModel->processInstruction(
      threadId, callStack->top()->callId, Instruction::Type::ACQUIRE, lineNo, &insId);

    pThis->pModel->processVarAccess(
      threadId, insId, 0, refId, Access::Type::WRITE, Access::State::READ_WRITE_SHARED);

    R_TIMERTYPE time = getTimer();
    pThis->pModel->processCall(
      Call::getInstance()->nextId(), insId, threadId, pThis->sHandler.mutex_lock_id, time, time);
  }
}

void ThreadEmulate::an_pthreadMutexUnlock(ThreadEmulate *pThis, THREADID threadId, ADDRINT *mutex)
{
  Reference::Id_t refId = pThis->getReferenceId(threadId, mutex);
  int lineNo            = 0;  // todo: resolve real line number by instruction
  Instruction::Id_t insId;

  // get call id from top of the trace stack
  auto threadData = pThis->getThreadData(threadId);
  auto callStack  = threadData->getCallStack();

  if (!callStack->empty()) {
    pThis->pModel->processInstruction(
      threadId, callStack->top()->callId, Instruction::Type::RELEASE, lineNo, &insId);

    pThis->pModel->processVarAccess(
      threadId, insId, 0, refId, Access::Type::WRITE, Access::State::READ_WRITE_SHARED);

    R_TIMERTYPE time = getTimer();
    pThis->pModel->processCall(
      Call::getInstance()->nextId(), insId, threadId, pThis->sHandler.mutex_unlock_id, time, time);
  }
}

}  // namespace pcv
