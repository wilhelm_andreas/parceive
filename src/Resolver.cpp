#include "Resolver.h"
#include <iostream>
#include "Parceive.h"

namespace pcv {

using namespace model;
using pcv::entity::Class;

Resolver::Resolver(Parceive *parceive, SymInfo *symInfo)
  : Module(parceive),
    pSymInfo(nullptr),
    _singleStackReferences(pParceive->getKnob("single") == "on")
{
  PIN_RWMutexInit(&_stackLock);
  PIN_RWMutexInit(&_globalLock);
  PIN_RWMutexInit(&_allocLock);
  PIN_RWMutexInit(&_refClassMapLock);
}

Resolver::~Resolver()
{
  PIN_RWMutexFini(&_stackLock);
  PIN_RWMutexFini(&_globalLock);
  PIN_RWMutexFini(&_allocLock);
  PIN_RWMutexFini(&_refClassMapLock);
}

void Resolver::initialize()
{
  pSymInfo = pParceive->getSymInfo();
  pModel   = pParceive->getModel();

  // if tracing unresolvable variables: add symbol info, variable, and reference
  if (pParceive->traceUnknownSymbols()) {
    _unknownSymbol = std::unique_ptr<SymbolInfo>(
      new SymbolInfo("unknown", 1, entity::ImageScopedId::getNone(), 0, 0));
    _unknownReference     = std::unique_ptr<Reference_t>(new Reference_t(
      Reference::getInstance()->nextId(), model::Reference::Type::REST, _unknownSymbol.get()));
    _unknownSymbol->refId = _unknownReference->id;
    pModel->processReference(_unknownReference->id,
                             0,
                             static_cast<unsigned>(_unknownSymbol->size),
                             Reference::Type::UNKNOWN,
                             _unknownSymbol->name,
                             0,
                             0,
                             0);
  }
}

void Resolver::finalize() {}

void Resolver::instrumentRoutine(IMG img, RTN rtn)
{
  auto rtnName       = RTN_Name(rtn);
  const auto rtnId   = RTN_Id(rtn);
  const auto imageId = IMG_Id(img);

  INT32 adr_line{0};
  std::string adr_file{};

  /* Locking the Pin client to obtain debug information */
#ifdef DISABLE_SOURCE_LOCATION
  adr_line = 0;
  adr_file = "none";
  (void)address;
#else
  pParceive->getSymInfo()->getSourceLocation(RTN_Address(rtn), &adr_line, &adr_file);
#endif

  const auto symbolId = matchRtn(imageId, rtnId, rtnName);

  pModel->processRoutine(imageId,
                         refineRtnName(imageId, rtnName),
                         rtnName,
                         adr_file,
                         model::Function::Type::METHOD,
                         static_cast<unsigned int>(adr_line),
                         static_cast<unsigned int>(rtnId),
                         static_cast<unsigned int>(symbolId));
}

pcv::entity::Id_t Resolver::matchRtn(const unsigned imageId,
                                     const unsigned rtnId,
                                     const std::string &rtnName)
{
  const auto routine = pSymInfo->getRoutine(imageId, rtnName);

  if (routine) {
    for (auto &local : routine->locals) {
      const auto localClsId =
        local->cls ? local->cls->getUniqueId() : entity::ImageScopedId::getNone();

      addStackSymbolInfo(rtnId,
                         local->offset,
                         std::unique_ptr<SymbolInfo>{new SymbolInfo(
                           local->name, local->size, localClsId, local->id, imageId)});
    }
  }
  return routine ? routine->id : 0;
}

string Resolver::refineRtnName(const unsigned imageId, const std::string &rtnName)
{
  std::string rtnUName{PIN_UndecorateSymbolName(rtnName, UNDECORATION_NAME_ONLY)};
  return pSymInfo->refineRtnName(imageId, rtnUName);
}

entity::ImageScopedId Resolver::getUniqueClassId(uint32_t imgId, const std::string &rtnName) const
{
  return pSymInfo->getUniqueClassId(imgId, rtnName);
}

void Resolver::addStackSymbolInfo(UINT32 rtnId, ADDRINT offset, std::unique_ptr<SymbolInfo> info)
{
  PIN_RWMutexWriteLock(&_stackLock);
  _stackSymbols[rtnId].insert(std::make_pair(offset, std::move(info)));
  PIN_RWMutexUnlock(&_stackLock);
}

void Resolver::addGlobalSymbolInfo(ADDRINT address, std::unique_ptr<SymbolInfo> info)
{
  PIN_RWMutexWriteLock(&_globalLock);
  _globalSymbols[address] = std::move(info);
  PIN_RWMutexUnlock(&_globalLock);
}

void Resolver::addAllocatedSymbolInfo(ADDRINT address, unique_ptr<SymbolInfo> info)
{
  _allocSymbols[address] = std::move(info);
}

SymbolInfo *Resolver::getStackSymbolInfo(UINT32 rtnId, S_ADDRINT offset)
{
  SymbolInfo *result = nullptr;
  PIN_RWMutexReadLock(&_stackLock);

  auto rtnSymbols = _stackSymbols.find(rtnId);
  if (rtnSymbols != end(_stackSymbols)) {
    auto symbol = rtnSymbols->second.upper_bound(offset);
    if (symbol != begin(rtnSymbols->second)) {
      symbol--;
      SymbolInfo *info = symbol->second.get();
      if (offset <= symbol->first && offset > static_cast<S_ADDRINT>(symbol->first - info->size)) {
        result = symbol->second.get();
      }
    }
  }

  PIN_RWMutexUnlock(&_stackLock);

  return result;
}

SymbolInfo *Resolver::getGlobalSymbolInfo(S_ADDRUINT address, S_ADDRUINT *startAddress)
{
  SymbolInfo *result = nullptr;
  PIN_RWMutexReadLock(&_globalLock);
  auto symbol = _globalSymbols.upper_bound(address);
  if (symbol != begin(_globalSymbols)) {
    symbol--;
    SymbolInfo *info = symbol->second.get();
    if (address >= symbol->first && address < symbol->first + info->size) {
      *startAddress = symbol->first;
      result        = symbol->second.get();
    }
  }
  PIN_RWMutexUnlock(&_globalLock);

  return result;
}

bool Resolver::addStackReference(ADDRINT address,
                                 SymbolInfo *info,
                                 Call::Id_t callId,
                                 Reference_t **reference)
{
  bool added = true;
  Reference::Id_t refId;

  if (_singleStackReferences) {
    refId = Reference::getInstance()->nextId();
  } else {
    if (info->refId != Reference::EMPTY) {
      refId = info->refId;
      added = false;
    } else {
      refId       = Reference::getInstance()->nextId();
      info->refId = refId;
    }
  }

  // add stack reference to maps
  PIN_RWMutexWriteLock(&_stackLock);
  auto *stackRef            = new StackReference_t(refId, info);
  _stackReferences[address] = std::unique_ptr<StackReference_t>(stackRef);
  _addressMap[callId].insert(address);
  *reference = stackRef;
  PIN_RWMutexUnlock(&_stackLock);

  return added;
}

bool Resolver::addGlobalReference(S_ADDRUINT address, SymbolInfo *info, Reference_t **ref)
{
  bool added = true;
  Reference::Id_t refId;

  if (info->refId != Reference::EMPTY) {
    refId = info->refId;
    added = false;
  } else {
    refId       = Reference::getInstance()->nextId();
    info->refId = refId;
  }

  PIN_RWMutexWriteLock(&_globalLock);
  auto *gRef                 = new GlobalReference_t(refId, info);
  _globalReferences[address] = std::unique_ptr<GlobalReference_t>(gRef);
  *ref                       = gRef;
  PIN_RWMutexUnlock(&_globalLock);

  return added;
}

bool Resolver::addAllocatedReference(S_ADDRUINT address,
                                     std::unique_ptr<SymbolInfo> info,
                                     S_ADDRUINT allocationIP,
                                     Reference_t **reference)
{
  Reference::Id_t refId = Reference::getInstance()->nextId();

  PIN_RWMutexWriteLock(&_allocLock);
  auto *allocRef            = new AllocatedReference_t(refId, info.get(), allocationIP);
  _allocReferences[address] = std::unique_ptr<AllocatedReference_t>(allocRef);
  addAllocatedSymbolInfo(address, std::move(info));
  *reference = allocRef;
  PIN_RWMutexUnlock(&_allocLock);

  return true;
}

bool Resolver::getStackReference(ADDRINT address, StackReference_t **var)
{
  bool found = false;

  PIN_RWMutexReadLock(&_stackLock);
  if (!_stackReferences.empty()) {
    auto reference = _stackReferences.upper_bound(address);
    if (reference != begin(_stackReferences)) {
      reference--;
      StackReference_t *sRef = reference->second.get();
      if (address >= reference->first && address < (reference->first + sRef->info->size)) {
        sRef->startAddress = reference->first;
        *var               = sRef;
        found              = true;
      }
    }
  }
  PIN_RWMutexUnlock(&_stackLock);

  return found;
}

bool Resolver::getGlobalReference(ADDRINT address, GlobalReference_t **var)
{
  bool found = false;

  PIN_RWMutexReadLock(&_globalLock);
  if (!_globalReferences.empty()) {
    auto reference = _globalReferences.upper_bound(address);
    if (reference != begin(_globalReferences)) {
      reference--;
      GlobalReference_t *gRef = reference->second.get();
      if (address >= reference->first && address < (reference->first + gRef->info->size)) {
        gRef->startAddress = reference->first;
        *var               = gRef;
        found              = true;
      }
    }
  }
  PIN_RWMutexUnlock(&_globalLock);

  return found;
}

bool Resolver::getAllocatedReference(ADDRINT address, AllocatedReference_t **var)
{
  bool found = false;

  PIN_RWMutexReadLock(&_allocLock);
  if (!_allocReferences.empty()) {
    auto reference = _allocReferences.upper_bound(address);
    if (reference != begin(_allocReferences)) {
      reference--;
      auto aRef = reference->second.get();

      if ((aRef != nullptr) && address >= reference->first &&
          address < (reference->first + aRef->info->size)) {
        aRef->startAddress = reference->first;
        *var               = aRef;
        found              = true;
      }
    }
  }
  PIN_RWMutexUnlock(&_allocLock);

  return found;
}

bool Resolver::getReference(ADDRINT address, Reference_t **var)
{
  {  // 1. check for a stack reference
    StackReference_t *sRef;

    if (getStackReference(address, &sRef)) {
      *var = static_cast<Reference_t *>(sRef);
      return true;
    }
  }

  {  // 2. check for a global reference
    GlobalReference_t *gRef;

    if (getGlobalReference(address, &gRef)) {
      *var = static_cast<Reference_t *>(gRef);
      return true;
    }
  }

  {  // 3. check for a allocated reference
    AllocatedReference_t *hRef;
    if (getAllocatedReference(address, &hRef)) {
      *var = static_cast<Reference_t *>(hRef);
      return true;
    }
  }

  return false;
}

void Resolver::refineReference(S_ADDRUINT address,
                               const Reference_t &reference,
                               model::Reference::Id_t *referenceId,
                               S_ADDRUINT *variableSize,
                               std::string *varName,
                               uint32_t *symbol,
                               model::Image::Id_t *imageId)
{
  // set size and name of reference in case of non-members
  *variableSize = reference.info->size;
  *varName      = static_cast<string>(reference.info->name);

  // resolve member variables
  PIN_RWMutexReadLock(&_refClassMapLock);
  const auto classIt = _refClassVecMap.find(reference.id);
  if (classIt != _refClassVecMap.end()) {
    const auto &csVec = classIt->second;
    for (auto cvIt = csVec.crbegin(); cvIt != csVec.crend(); cvIt++) {
      const auto &classStruct = *cvIt;
      if (!classStruct.classId.isValid()) continue;
      const auto var =
        pSymInfo->getMemberVariable(classStruct.classId, (address - classStruct.address));
      if (var) {
        *referenceId  = getRefId(address);
        *variableSize = var->size;
        *varName      = var->name;
        *symbol       = static_cast<uint32_t>(var->id);
        *imageId      = var->img->id;
        break;
      }
    }
  }
  PIN_RWMutexUnlock(&_refClassMapLock);
}

void Resolver::assignReferenceToClass(S_ADDRUINT address,
                                      S_ADDRUINT referenceId,
                                      const entity::ImageScopedId &classId)
{
  PIN_RWMutexWriteLock(&_refClassMapLock);
  _refClassVecMap[referenceId].emplace_back(address, classId);
  PIN_RWMutexUnlock(&_refClassMapLock);
}

void Resolver::removeStackReferences(Call::Id_t callId)
{
  PIN_RWMutexWriteLock(&_stackLock);
  auto addresses = _addressMap.find(callId);
  if (addresses != end(_addressMap)) {
    for (auto address : addresses->second) {
      auto ref = _stackReferences.find(address);
      if (ref != std::end(_stackReferences)) {
        if (ref->second->info->classId.isValid()) {
          if (ref != std::end(_stackReferences)) {
            auto f = _addrRefMap.lower_bound(address);
            auto l = _addrRefMap.upper_bound(address + ref->second->info->size - 1);
            if (f != std::end(_addrRefMap) && l != std::end(_addrRefMap)) {
              _addrRefMap.erase(f, l);
            } else if (f != std::end(_addrRefMap)) {
              _addrRefMap.erase(f);
            }
          }
        }
        _stackReferences.erase(ref);
      }
    }
    _addressMap.erase(addresses);
  }
  PIN_RWMutexUnlock(&_stackLock);
}

void Resolver::removeAllocatedReference(S_ADDRUINT address)
{
  AllocatedReference_t *ref;
  if (getAllocatedReference(address, &ref)) {
    PIN_RWMutexWriteLock(&_allocLock);
    auto variable = _allocReferences.find(ref->startAddress);
    if (variable != end(_allocReferences)) {
      auto first = _addrRefMap.lower_bound(ref->startAddress);
      auto last  = _addrRefMap.upper_bound(ref->startAddress + ref->info->size - 1);
      if (first != end(_addrRefMap) && last != end(_addrRefMap)) {
        _addrRefMap.erase(first, last);
      } else if (first != end(_addrRefMap)) {
        _addrRefMap.erase(first);
      }

      _allocSymbols.erase(address);
      _allocReferences.erase(address);

      PIN_RWMutexWriteLock(&_refClassMapLock);
      const auto rcvmIt = _refClassVecMap.find(ref->id);
      if (rcvmIt != _refClassVecMap.end()) _refClassVecMap.erase(rcvmIt);
      PIN_RWMutexUnlock(&_refClassMapLock);
    }
    PIN_RWMutexUnlock(&_allocLock);
  }
}

Reference::Id_t Resolver::getRefId(ADDRINT address)
{
  auto itm = _addrRefMap.find(address);
  if (itm != end(_addrRefMap)) {
    return itm->second;
  }
  Reference::Id_t refId = Reference::getInstance()->nextId();
  _addrRefMap[address]  = refId;
  return refId;
}

void Resolver::fillGlobalSymbolInfo(Id_t imgId)
{
  const auto globals = pSymInfo->getGlobalVariables(imgId);
  for (const auto &g : *globals) {
    const auto globalClassId = g->cls ? g->cls->getUniqueId() : entity::ImageScopedId::getNone();
    const auto imageId       = g->img ? g->img->id : pcv::entity::NO_ID;
    addGlobalSymbolInfo(
      g->offset,
      std::unique_ptr<SymbolInfo>{new SymbolInfo(g->name, g->size, globalClassId, g->id, imageId)});
  }
}

void Resolver::fillStaticSymbolInfo(Id_t imgId)
{
  const auto statics = pSymInfo->getStaticVariables(imgId);
  for (const auto &s : *statics) {
    addGlobalSymbolInfo(s->offset,
                        std::unique_ptr<SymbolInfo>{new SymbolInfo(
                          s->name, s->size, entity::ImageScopedId::getNone(), s->id, imgId)});
  }
}

Reference_t *Resolver::getUnknownReference() const { return _unknownReference.get(); }

void Resolver::resetAccessesInCalls() { accessesInCall_.clear(); }

void Resolver::addAccessInCall(ADDRINT address) { accessesInCall_.insert(address); }

bool Resolver::accessInCallExists(ADDRINT address)
{
  return (accessesInCall_.find(address) != std::end(accessesInCall_));
}

}  // namespace pcv
