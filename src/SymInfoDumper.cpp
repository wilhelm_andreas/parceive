#include <algorithm>
#include <cstdarg>
#include <cstring>
#include <map>
#include <stack>

#include "Common.h"
#include "SymInfo.h"
#include "SymInfoDumper.h"
#include "entities/Class.h"
#include "entities/Namespace.h"
#include "entities/Routine.h"
#include "entities/Variable.h"

#define PRINT(format, ...) printf(format, __VA_ARGS__)
//#define PRINT(msg)

using namespace pcv;
using namespace std;
using pcv::entity::Variable;

namespace pcv {

size_t indent = 0;
string indentString;
stack<string> blockNames;

void updateIndent() { indentString = string(indent, '\t'); }

void increaseIndent()
{
  ++indent;
  updateIndent();
}

void decreaseIndent()
{
  indent = max(size_t(0), indent - 1);
  updateIndent();
}

void log(const char* const format, va_list args)
{
  char buf[4096] = {0};
  vsprintf(buf, format, args);
  size_t len   = strlen(buf);
  buf[len]     = '\n';
  buf[len + 1] = '\0';
  PRINT("%s%s", indentString.c_str(), buf);
}

void log(const char* const format, ...)
{
  va_list args;
  va_start(args, format);
  log(format, args);
  va_end(args);
}

void newBlock(const string& blockName)
{
  log("");
  increaseIndent();
  log(">** %s", blockName.c_str());
  blockNames.push(blockName);
}

void endBlock()
{
  const auto& blockName = blockNames.top();
  log("<** %s", blockName.c_str());
  blockNames.pop();
  decreaseIndent();
}

const char* typeToString(Variable::Type tp)
{
  switch (tp) {
    case Variable::Type::STACK:
      return "STACK";
    case Variable::Type::HEAP:
      return "HEAP";
    case Variable::Type::STATICVAR:
      return "STATICVAR";
    case Variable::Type::GLOBAL:
      return "GLOBAL";
    case Variable::Type::UNKNOWN:
      return "UNKNOWN";
  }
  return "<unknown Type value>";
}

void dumpSoftwareEntity(const pcv::entity::SoftwareEntity* entity)
{
  std::string namespaceName = "<no namespace>";
  if (entity->nmsp != nullptr && !entity->nmsp->name.empty()) {
    namespaceName = entity->nmsp->name;
  }
  std::string className = "<no class name>";
  if (entity->cls != nullptr && !entity->cls->name.empty()) {
    className = entity->cls->name;
  }

  log("name:\t%s", entity->name.c_str());
  log("id:\t%d", entity->id);
  log("size:\t%d", entity->size);
  log("namespace:\t%s", namespaceName.c_str());
  log("class name:\t%s", className.c_str());
}

void dumpVariable(const Variable* variable)
{
  dumpSoftwareEntity(variable);

  log("offset:\t%ll", variable->offset);
  log("type:\t%s", typeToString(variable->type));
  if (variable->declaringClass != nullptr) {
    log("classtype:\t%s", variable->declaringClass->name.c_str());
  }
}

static string getFullName(const entity::Namespace* nmsp)
{
  return (nmsp->parent ? getFullName(nmsp->parent) + "::" : "") + nmsp->name;
}

static string getFullName(const entity::Class* cls)
{
  return (cls->cls ? getFullName(cls->cls) + "::" : "") + cls->name;
}

static string getFullName(const entity::SoftwareEntity* entity)
{
  std::string fullName;
  if (entity->nmsp) {
    fullName = getFullName(entity->nmsp);
    if (!fullName.empty()) {
      fullName += "::";
    }
  }
  if (entity->cls) {
    fullName += getFullName(entity->cls) + "::";
  }
  fullName += entity->name;
  return fullName;
}

void dumpRoutine(const pcv::entity::Routine* routine)
{
  const auto fullName = getFullName(routine);
  if (fullName != routine->name) {
    log("full name:\t%s", getFullName(routine).c_str());
  }
  dumpSoftwareEntity(routine);
  const char* const isConstr = routine->isConstructor ? "true" : "false";
  log("is constructor:\t%s", isConstr);

  if (routine->locals.size() > 0) {
    newBlock("LOCAL VARIABLES");
    for (const auto local : routine->locals) {
      dumpVariable(local);
    }
    endBlock();
  }
  log("");
}

void dumpClass(const entity::Class* cls)
{
  const auto fullName = getFullName(cls);
  if (fullName != cls->name) {
    log("full name:\t%s", getFullName(cls).c_str());
  }
  dumpSoftwareEntity(cls);

  if (cls->methods.size() > 0) {
    newBlock("METHODS");
    for (const auto method : cls->methods) {
      dumpRoutine(method);
    }
    endBlock();
  }
  if (cls->members.size() > 0) {
    newBlock("MEMBERS");
    for (const auto member : cls->members) {
      dumpVariable(member);
    }
    endBlock();
  }
  if (cls->baseClasses.size() > 0) {
    newBlock("BASE CLASSES");
    for (const auto baseCls : cls->baseClasses) {
      log("name:\t%s", baseCls->name.c_str());
    }
    endBlock();
  }
  if (cls->inheritClasses.size() > 0) {
    newBlock("INHERITED CLASSES");
    for (const auto icls : cls->inheritClasses) {
      log("name:\t%s", icls->name.c_str());
    }
    endBlock();
  }
  if (cls->composites.size() > 0) {
    newBlock("COMPOSITE CLASSES");
    for (const auto ccls : cls->composites) {
      log("name:\t%s", ccls->name.c_str());
    }
    endBlock();
  }
  log("");
}

void dumpVariables(const vector<Variable*>& variables)
{
  newBlock("VARIABLES");
  for (auto var : variables) {
    dumpVariable(var);
  }
  endBlock();
}

void dumpRoutines(const vector<unique_ptr<entity::Routine>>& routines)
{
  newBlock("ROUTINES");
  for (const auto& routine : routines) {
    dumpRoutine(routine.get());
  }
  endBlock();
}

void dumpClasses(const vector<unique_ptr<entity::Class>>& classes)
{
  newBlock("CLASSES");
  for (const auto& cls : classes) {
    dumpClass(cls.get());
  }
  endBlock();
}

void dumpSymInfo(SymInfo* symInfo)
{
  const auto& ctxt = symInfo->getContext();

  // const auto globalVars = symInfo->getGlobalVariables();
  // if (globalVars->size() > 0) {
  //  log("** GLOBAL VARIABLES **");
  //  dumpVariables(*globalVars);
  //}

  // const auto staticVars = symInfo->getStaticVariables();
  // if (staticVars->size() > 0) {
  //  log("** STATIC VARIABLES **");
  //  dumpVariables(*staticVars);
  //}

  const auto& routines = ctxt.routines;
  if (routines.size() > 0) {
    dumpRoutines(routines);
  }

  const auto& classes = ctxt.classes;
  if (classes.size() > 0) {
    dumpClasses(classes);
  }
}
}  // namespace pcv
