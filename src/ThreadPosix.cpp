/* Copyright 2014 Siemens Technology and Services*/

#include "ThreadPosix.h"

#include <Resolver.h>
#include <sys/syscall.h>
#include <cassert>
#include <memory>

#include "Filter.h"
#include "Parceive.h"
#include "Routine.h"

// private types
typedef void *(*start_routine_t)(void *);
typedef struct {
  void *arg;
  pcv::model::Instruction::Id_t createInstruction;
  THREADID parentThreadId;
} ptc_struct;

namespace pcv {

ThreadPosix::ThreadPosix(Parceive *parceive) : Thread(parceive) { PIN_RWMutexInit(&_lock); }

ThreadPosix::~ThreadPosix() { PIN_RWMutexFini(&_lock); }

void ThreadPosix::initialize() { Thread::initialize(); }

int ThreadPosix::w_pthreadCreate(ThreadPosix *pThis,
                                 CONTEXT *context,
                                 pthread_t *thread,
                                 pthread_attr_t *attr,
                                 void *(*start_routine)(void *),
                                 void *arg,
                                 AFUNPTR orig_fptr)
{
  THREADID pin_tid = PIN_ThreadId();

  // write a new instruction
  auto threadData                = pThis->getThreadData(pin_tid);
  model::Instruction::Id_t insId = 0;
  pThis->pModel->processInstruction(
    pin_tid, model::Call::NO_CALL, model::Instruction::Type::FORK, threadData->lineNo, &insId);

  pThis->lock();
  pThis->threadStartAddresses.insert(reinterpret_cast<ADDRINT>(start_routine));
  pThis->insPtMap[insId] = thread;
  pThis->unlock();

  // handle original pthread_create call with extended arguments
  auto *ptcs              = new ptc_struct;
  ptcs->arg               = arg;
  ptcs->createInstruction = insId;
  ptcs->parentThreadId    = pin_tid;

  int ret;
  // clang-format off
  PIN_CallApplicationFunction(context,
                              pin_tid,
                              CALLINGSTD_DEFAULT,
                              orig_fptr,
                              nullptr,
                              PIN_PARG(int), &ret,
                              PIN_PARG(pthread_t *), thread,
                              PIN_PARG(pthread_attr_t *), attr,
                              PIN_PARG(void *(*)(void *)), start_routine,
                              PIN_PARG(void *), reinterpret_cast<void *>(ptcs),
                              PIN_PARG_END());
  // clang-format on
  return ret;
}

int ThreadPosix::w_pthreadJoin(
  ThreadPosix *pThis, CONTEXT *context, pthread_t thread, void **result, AFUNPTR orig_funptr)
{
  THREADID joiningThreadId = PIN_ThreadId();

  // wait for join
  int res;
  // clang-format off
  PIN_CallApplicationFunction(context,
                              joiningThreadId,
                              CALLINGSTD_DEFAULT,
                              orig_funptr,
                              nullptr,
                              PIN_PARG(int), &res,
                              PIN_PARG(pthread_t), thread,
                              PIN_PARG(void **), result,
                              PIN_PARG_END());
  // clang-format on

  // write a new instruction
  ThreadData *joiningThreadData  = pThis->getThreadData(joiningThreadId);
  model::Instruction::Id_t insId = model::Instruction::EMPTY;
  pThis->pModel->processInstruction(joiningThreadId,
                                    model::Call::NO_CALL,
                                    model::Instruction::Type::JOIN,
                                    joiningThreadData->lineNo,
                                    &insId);

  // write the new call and thread (finally)
  pThis->lock();
  auto it = pThis->ptTidMap.find(thread);
  if (it != pThis->ptTidMap.end()) {
    THREADID childThreadId = it->second;
    pThis->ptTidMap.erase(it);

    ThreadData *childThreadData            = pThis->getThreadData(childThreadId);
    childThreadData->model.joinInstruction = insId;

    pThis->finalizeThread(childThreadId);
  }
  pThis->unlock();
  return res;
}

void ThreadPosix::an_threadStart(
  ThreadPosix *pThis, THREADID threadId, ADDRINT *arg, S_ADDRUINT returnIP, UINT32 rtnId)
{
  auto *ptcs = reinterpret_cast<ptc_struct *>(*arg);

  pThis->lock();
  auto it = pThis->insPtMap.find(ptcs->createInstruction);
  if (it != pThis->insPtMap.end()) {
    pthread_t *thread = it->second;

    // After this, we no longer need the insPtMap entry.
    pThis->insPtMap.erase(it);

    // Instead, we need one in ptTidMap to find the ThreadData
    // once we reach pthread_join
    // This must be a new entry in the map.
    assert(pThis->ptTidMap.find(*thread) == pThis->ptTidMap.end());
    pThis->ptTidMap[*thread] = threadId;

    // Fill in the model fields we know so far
    auto threadData = pThis->getThreadData(threadId);
    auto callStack  = threadData->getCallStack();

    if (callStack->empty()) {
      // This can happen if the Routine module did not record the function
      // entry (a) because the analysis is not active, or (b) because there
      // is no Routine module.
      // The routine being filtered is actually not a problem, because
      // our instrumentTrace() makes sure all thread start routines are
      // instrumented.
      // Either way, we need to create the first stack frame and function
      // entry here!
      auto calleeId         = model::Call::getInstance()->nextId();
      R_TIMERTYPE startTime = getTimer();

      callStack->addToStack(std::unique_ptr<StackFrame>{
        new StackFrame(rtnId, calleeId, returnIP, startTime, model::Instruction::EMPTY, 0)});

      pThis->pModel->processCallEntry(model::Call::EMPTY, calleeId, threadId);
    }

    threadData->model.parentThread      = ptcs->parentThreadId;
    threadData->model.createInstruction = ptcs->createInstruction;
    threadData->model.call              = callStack->top()->callId;
  }
  pThis->unlock();

  *arg = reinterpret_cast<ADDRINT>(ptcs->arg);
  delete ptcs;
}

void ThreadPosix::instrumentCreate(IMG img)
{
  RTN rtn_ptc = RTN_FindByName(img, "pthread_create");
  if (RTN_Valid(rtn_ptc)) {
    // avoid filtering pthread_create
    auto ip = RTN_Address(rtn_ptc);
    pParceive->getFilter()->addExpectedRoutineAddress(ip);
    pParceive->getFilter()->setRtnInfo(
      ip, std::unique_ptr<RtnInfo>{new RtnInfo(RTN_Id(rtn_ptc), std::string("pthread_create"))});

    // change signature of pthread_create to call the wrapper instead
    PROTO proto = PROTO_Allocate(PIN_PARG(int),
                                 CALLINGSTD_DEFAULT,
                                 "pthread_create",
                                 PIN_PARG(pthread_t *),
                                 PIN_PARG(pthread_attr_t *),
                                 PIN_PARG(void *(*)(void *)),
                                 PIN_PARG(void *),
                                 PIN_PARG_END());

    // clang-format off
    RTN_ReplaceSignature(rtn_ptc, AFUNPTR(w_pthreadCreate),
                         IARG_PROTOTYPE, proto,
                         IARG_PTR, this,
                         IARG_CONTEXT,
                         IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
                         IARG_FUNCARG_ENTRYPOINT_VALUE, 1,
                         IARG_FUNCARG_ENTRYPOINT_VALUE, 2,
                         IARG_FUNCARG_ENTRYPOINT_VALUE, 3,
                         IARG_ORIG_FUNCPTR,
                         IARG_END);
    // clang-format on
  }
}

void ThreadPosix::instrumentJoin(IMG img)
{
  RTN rtn_ptj = RTN_FindByName(img, "pthread_join");
  if (RTN_Valid(rtn_ptj)) {
    PROTO proto = PROTO_Allocate(PIN_PARG(int),
                                 CALLINGSTD_DEFAULT,
                                 "pthread_join",
                                 PIN_PARG(pthread_t),
                                 PIN_PARG(void **),
                                 PIN_PARG_END());

    // clang-format off
    RTN_ReplaceSignature(rtn_ptj, AFUNPTR(w_pthreadJoin),
                         IARG_PROTOTYPE, proto,
                         IARG_PTR, this,
                         IARG_CONTEXT,
                         IARG_FUNCARG_ENTRYPOINT_VALUE, 0,
                         IARG_FUNCARG_ENTRYPOINT_VALUE, 1,
                         IARG_ORIG_FUNCPTR,
                         IARG_END);
    // clang-format on
  }
}

void ThreadPosix::instrumentLock(IMG img)
{
  RTN rtn_ptl = RTN_FindByName(img, "pthread_mutex_lock");
  if (RTN_Valid(rtn_ptl)) {
    RTN_Open(rtn_ptl);
    // clang-format off
    RTN_InsertCall(rtn_ptl, IPOINT_BEFORE, AFUNPTR(an_mutexLock),
                   IARG_PTR, this,
                   IARG_THREAD_ID,
                   IARG_FUNCARG_ENTRYPOINT_REFERENCE, 0,
                   IARG_END);
    // clang-format on
    RTN_Close(rtn_ptl);
  }
}

void ThreadPosix::instrumentUnlock(IMG img)
{
  RTN rtn_ptl = RTN_FindByName(img, "pthread_mutex_unlock");
  if (RTN_Valid(rtn_ptl)) {
    RTN_Open(rtn_ptl);
    // clang-format off
    RTN_InsertCall(rtn_ptl, IPOINT_BEFORE, AFUNPTR(an_mutexUnlock),
                   IARG_PTR, this,
                   IARG_THREAD_ID,
                   IARG_FUNCARG_ENTRYPOINT_REFERENCE, 0,
                   IARG_END);
    // clang-format on
    RTN_Close(rtn_ptl);
  }
}

void ThreadPosix::beforeInstrumentImage(IMG img)
{
  const string &imgName = IMG_Name(img);
  if (imgName.find("libpthread.so") != string::npos) {
    Filter *filter = pParceive->getFilter();
    filter->forceInclusion(img, true);
  }
}

void ThreadPosix::instrumentImage(IMG img)
{
  const std::string &imgName = IMG_Name(img);
  if (imgName.find("libpthread.so") != std::string::npos) {
    instrumentCreate(img);
    instrumentJoin(img);
    instrumentLock(img);
    instrumentUnlock(img);
  }
}

void ThreadPosix::instrumentTrace(IMG img, TRACE trace, bool routine_is_istrumented)
{
  if (!routine_is_istrumented) return;

  INS firstIns    = BBL_InsHead(TRACE_BblHead(trace));
  ADDRINT address = INS_Address(firstIns);

  auto it = threadStartAddresses.find(address);
  if (it != threadStartAddresses.end()) {
    RTN rtn = RTN_FindByAddress(address);  // internal lock (since in instrum.)
    threadStartAddresses.erase(it);

    if (!routine_is_istrumented) {
      // Instrument routine and (more importantly) make sure it
      // gets recorded in the database.
      pParceive->instrumentRoutine(img, rtn);
    }

    // clang-format off
    INS_InsertCall(firstIns, IPOINT_BEFORE, (AFUNPTR)ThreadPosix::an_threadStart,
                   IARG_PTR, this,
                   IARG_THREAD_ID,
                   IARG_FUNCARG_ENTRYPOINT_REFERENCE, 0,
                   IARG_RETURN_IP,
                   IARG_UINT32, RTN_Id(rtn),
                   IARG_CALL_ORDER, CALL_ORDER_LAST,
                   IARG_END);
    // clang-format on
  }
}

model::Reference::Id_t ThreadPosix::getReferenceId(THREADID threadId, ADDRINT *mutex)
{
  Reference_t *reference;

  auto resolver = pParceive->getResolver();

  if (!resolver->getReference(*mutex, &reference)) {
    resolver->addAllocatedReference(
      *mutex,
      std::unique_ptr<SymbolInfo>(
        new SymbolInfo("lock", sizeof(pthread_mutex_t), entity::ImageScopedId::getNone(), 0, 0)),
      0,
      &reference);

    pModel->processReference(reference->id,
                             threadId,
                             sizeof(pthread_mutex_t),
                             model::Reference::Type::HEAP,
                             "lock",
                             0,
                             reference->info->symbolId,
                             reference->info->classId.imageId);
  }

  return reference->id;
}

namespace {
void analyzeMutex(ThreadPosix *pThis,
                  DataModel *model,
                  const model::Reference::Id_t &refId,
                  const THREADID &threadId,
                  const model::Instruction::Type &type)
{
  THREADID pin_tid = PIN_ThreadId();
  auto threadData  = pThis->getThreadData(pin_tid);
  auto callStack   = threadData->getCallStack();

  if (!callStack->empty()) {
    model::Instruction::Id_t insId{};
    model->processInstruction(pin_tid, callStack->top()->callId, type, threadData->lineNo, &insId);

    model->processVarAccess(
      threadId, insId, 0, refId, model::Access::Type::WRITE, model::Access::State::READ_WRITE);
  }
}
}  // unnamed namespace

void ThreadPosix::an_mutexLock(ThreadPosix *pThis, THREADID threadId, ADDRINT *arg)
{
  model::Reference::Id_t refId = pThis->getReferenceId(threadId, arg);
  analyzeMutex(pThis, pThis->pModel, refId, threadId, model::Instruction::Type::ACQUIRE);
}

void ThreadPosix::an_mutexUnlock(ThreadPosix *pThis, THREADID threadId, ADDRINT *arg)
{
  model::Reference::Id_t refId = pThis->getReferenceId(threadId, arg);
  analyzeMutex(pThis, pThis->pModel, refId, threadId, model::Instruction::Type::RELEASE);
}

}  // namespace pcv
