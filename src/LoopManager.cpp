/*! \file loopmanager.cpp
 *  \brief This file contains the definition of the loop manager.
 *
 * Copyright 2015 Siemens Technology and Services
 */
#include "LoopManager.h"

namespace pcv {

using pcv::model::Call;

LoopManager *LoopManager::getInstance()
{
  static LoopManager instance;
  return &instance;
}

bool LoopManager::enterLoop(Call::Id_t callId, Loop::Id_t loopId)
{
  bool newLoop = false;

  // a new loop execution has been started
  if (loopStack_[callId].empty() || loopStack_[callId].top().loopId != loopId) {
    // a completely new loop has been entered
    if (loops_.find(loopId) == loops_.end()) {
      loops_.insert(loopId);
      executionCount_[loopId] = LoopExecution::INITVAL;
      newLoop                 = true;
    }

    loopStack_[callId].push(LoopState(loopId,
                                      loopExecutionI_->nextId(),
                                      loopIterationI_->nextId(),
                                      executionCount_[loopId]++,
                                      LoopIteration::INITVAL,
                                      getTimer()));
  } else {
    // a new iteration has been started
    loopStack_[callId].top().iterationId = loopIterationI_->nextId();
    loopStack_[callId].top().iterationNo++;
  }

  // reset touched state
  loopStack_[callId].top().touched = false;

  return newLoop;
}

void LoopManager::exitLoop(Call::Id_t callId, Loop::Id_t /*loopId*/) { loopStack_[callId].pop(); }
const LoopState &LoopManager::top(Call::Id_t callId) const
{
  auto entry = loopStack_.find(callId);
  return entry->second.top();
}

void LoopManager::touch(Call::Id_t callId)
{
  auto entry = loopStack_.find(callId);
  if (entry != loopStack_.end() && !entry->second.empty()) entry->second.top().touched = true;
}

bool LoopManager::empty(Call::Id_t callId /**< [IN] The call id */) const
{
  auto entry = loopStack_.find(callId);
  return (entry == loopStack_.end() || entry->second.empty());
}

}  // namespace pcv
