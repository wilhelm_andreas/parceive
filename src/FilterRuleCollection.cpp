#include <regex>

#include "Common.h"
#include "FilterRuleCollection.h"

namespace pcv {

void FilterRuleCollection::addJsonRule(const Filter::Action &action, const string &rule)
{
  if (action >= Filter::NONE) return;

  if (action >= Filter::START) {
    addLineRule(action, rule);
  } else {
    switch (action) {
      case Filter::INCLUDE:
        defaultStaticAction = Filter::EXCLUDE;
        _includes.emplace(rule);
        break;
      case Filter::EXCLUDE:
        _excludes.emplace(rule);
        break;
      case Filter::PARTIAL:
        _partial.emplace(rule);
        break;
      default:
        dynamicRules.emplace_back(action, rule);
        break;
    }
  }
}

void FilterRuleCollection::addXMLRule(const pcv::Filter::Action &action, const string &rule)
{
  if (!isValidRegEx(rule) || action >= Filter::NONE) {
    return;
  }

  if (action >= Filter::START) {
    addLineRule(action, rule);
  } else {
    if (action == Filter::INCLUDE) {
      defaultStaticAction = Filter::EXCLUDE;
    }
    auto& list = action <= Filter::EXCLUDE ? staticRules : dynamicRules;
    list.emplace_back(action, rule);
  }
}


void FilterRuleCollection::addLineRule(const Filter::Action& action, const string& rule)
{
  const int colonIdx = lastIndexOf(rule, ':');
  if (colonIdx >= 0 && colonIdx < static_cast<int>(rule.length() - 1)) {
    try {
      const string regex(rule.substr(0, static_cast<size_t>(colonIdx)));
      const int line = stoi(rule.substr(1 + colonIdx));
      lineRules.emplace_back(action, regex, line);
      sortLineRules();
    } catch (invalid_argument&) {
      // this can happen when the line can't be converted correctly
      PLOGP("Warning: Ignoring rule %s: invalid argument.\n", rule.c_str());
    }
  } else {
    PLOGP("Warning: Ignoring rule %s: no line number given.\n", rule.c_str());
  }
}

bool FilterRuleCollection::isExcluded(const string& name) const
{
  return getStaticFilterAction(name) == Filter::EXCLUDE;
}

bool FilterRuleCollection::isPartial(const string& name) const
{
  return getStaticFilterAction(name) == Filter::PARTIAL;
}

Filter::Action FilterRuleCollection::getLineFilterAction(const string& filePath,
                                                         const int line) const
{
  Filter::Action action = Filter::NONE;

  for (const auto lineRule : lineRules) {
    const auto ruleAction = get<0>(lineRule);
    const auto& ruleRegex = get<1>(lineRule);
    const auto ruleLine   = get<2>(lineRule);
    if (ruleLine <= line) {
      if (matchesRegEx(ruleRegex, filePath)) { action = ruleAction; }
    }
  }
  return action;
}

Filter::Action FilterRuleCollection::getStaticFilterAction(const string& name) const
{
  Filter::Action action = defaultStaticAction;

  // first consider non-regexp rules
  if (_partial.find(name) != std::end(_partial)) return Filter::PARTIAL;
  if (_excludes.find(name) != std::end(_excludes)) return Filter::EXCLUDE;
  if (_includes.find(name) != std::end(_includes)) return Filter::INCLUDE;

  // then regexp rules
  for (const auto it : staticRules) {
    if (matchesRegEx(it.second, name)) { action = it.first; }
  }

  return action;
}

Filter::Action FilterRuleCollection::getDynamicFilterAction(const string& name) const
{
  Filter::Action action = defaultStaticAction;

  // then regexp rules
  for (const auto it : dynamicRules) {
    if (matchesRegEx(it.second, name)) { action = it.first; }
  }

  return action;
}

void FilterRuleCollection::sortLineRules()
{
  sort(lineRules.begin(), lineRules.end(), [](const line_rule_t& lhs, const line_rule_t& rhs) {
    return get<2>(lhs) < get<2>(rhs);
  });
}

}  // namespace pcv