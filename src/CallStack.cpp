//
// Created by wilhelma on 7/12/17.
//

#include "CallStack.h"

#include <algorithm>
#include <iostream>

#include "Resolver.h"

namespace pcv {

void CallStack::adjustStackEnter(ADDRINT sp, Resolver* resolver)
{
  if (callStack_.empty()) return;

  while (sp >= callStack_.back()->sp) {
    pop(resolver);
    if (callStack_.size() <= 1) return;
  }
}

void CallStack::adjustStackLeave(ADDRINT sp, ADDRINT returnIP, pcv::Resolver* resolver)
{
  if (callStack_.empty()) return;

  if (sp == callStack_.back()->sp && returnIP == callStack_.back()->returnIP)
    resolver->removeStackReferences(callStack_.back()->callId);

  while (sp > callStack_.back()->sp) {
    pop(resolver);
    if (callStack_.size() <= 1) return;
  }
}

void CallStack::addToStack(std::unique_ptr<StackFrame> framePtr)
{
  callStack_.push_back(std::move(framePtr));
}

void CallStack::pop(Resolver* resolver)
{
  resolver->removeStackReferences(callStack_.back()->callId);
  callStack_.pop_back();
}

StackFrame* CallStack::top() { return callStack_.back().get(); }
StackFrame* CallStack::beforeTop()
{
  if (callStack_.size() > 1) return callStack_[callStack_.size() - 2].get();
  return nullptr;
}

bool CallStack::empty() { return callStack_.empty(); }
bool StackFrame::isRecentlyAccessed(int lineNo, ADDRINT address, model::Access::Type type)
{
  Location l{lineNo, address, type};
  if (accessCacheSet.find(l) == std::end(accessCacheSet)) {
    accessCacheSet.insert(l);
    return false;
  }

  return true;
}

}  // namespace pcv
