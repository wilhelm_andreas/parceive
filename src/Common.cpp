#include <regex>
#include <string>

#include "Common.h"

using std::regex;
using std::regex_error;
using std::string;

namespace pcv {

bool matchesRegEx(const string &regex, const string &pattern)
{
  const std::regex expression(regex);
  return regex_match(pattern, expression);
}

/*! \brief
 * Validates if a supplied regular expression is a valid std::regex
 * expression
 * \return true if regular expression is valid else false
 */
bool isValidRegEx(const string &regEx)
{
  try {
    if (!regEx.empty()) {
      regex test(regEx);
      return true;
    }
  } catch (regex_error &) {
  }
  return false;
}

}  // namespace pcv
