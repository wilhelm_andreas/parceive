/* Copyright 2014 Siemens Technology and Services*/
#include "LoopDetector.h"

#include <sstream>

#include "DataModel.h"
#include "Routine.h"
#include "SymInfo.h"
#include "Thread.h"

namespace pcv {

dissmap_t dissmap;

BasicBlock *BasicBlock::root     = nullptr;
BasicBlock::id_t BasicBlock::gID = 0;
BasicBlock::bbls_t BasicBlock::gBasicBlocks;

using namespace model;

BasicBlock::BasicBlock(ADDRINT entry_ins, const BasicBlock *predecessor)
  : id(gID++), entry_ins(entry_ins), successor(nullptr), target(nullptr), iDom(nullptr)
{
  if (predecessor != nullptr) predecessors.insert(predecessor->id);

  gBasicBlocks.push_back(this);
}

void BasicBlock::reset()
{
  gID = 0;
  gBasicBlocks.clear();
}

void BasicBlock::buildStaticBBLs(RTN rtn, const leaders_t &leaders, const jumps_t &jumps)
{
  std::map<ADDRINT, BasicBlock *> leaderBBLMap, exitBBLMap;
  BasicBlock *curBBL = nullptr;

  RTN_Open(rtn);

  // head instruction
  INS in = RTN_InsHead(rtn);
  if (INS_Valid(in)) {
    ADDRINT insAddr = INS_Address(in);
    root = curBBL         = new BasicBlock(insAddr, nullptr);
    leaderBBLMap[insAddr] = curBBL;
    curBBL->exit_ins      = insAddr;
    in                    = INS_Next(in);
  }

  // other instructions
  bool hasFallThrough = true;
  while (INS_Valid(in)) {
    ADDRINT insAddr = INS_Address(in);
    if (leaders.find(insAddr) != leaders.end()) {
      exitBBLMap[curBBL->exit_ins] = curBBL;
      if (hasFallThrough) {
        BasicBlock **successor = &curBBL->successor;
        curBBL                 = new BasicBlock(insAddr, curBBL);
        *successor             = curBBL;
      } else {
        curBBL = new BasicBlock(insAddr, nullptr);
      }
      leaderBBLMap[insAddr] = curBBL;
    }
    curBBL->exit_ins = insAddr;
    hasFallThrough   = INS_HasFallThrough(in) || INS_IsCall(in);
    in               = INS_Next(in);
  }
  RTN_Close(rtn);

  // consider jump instructions (for targets/predecessors)
  for (auto edge : jumps) {
    auto source = exitBBLMap.find(edge.first);
    auto target = leaderBBLMap.find(edge.second);

    if (source != exitBBLMap.end()) {
      if (target != leaderBBLMap.end()) {
        target->second->predecessors.insert(source->second->id);
        source->second->target = target->second;
      }
    }
  }
}

void BasicBlock::getDominators(BasicBlock * /*root*/)
{
  const size_t nBBL = size();
  auto *domBitSets  = new std::unique_ptr<boost::dynamic_bitset<> >[nBBL];

  // for each bbl, create a dominator bitset for efficient set operations
  for (size_t i = 0; i < nBBL; ++i)
    domBitSets[i] = std::unique_ptr<boost::dynamic_bitset<> >(new boost::dynamic_bitset<>(nBBL));
  domBitSets[0]->reset();
  domBitSets[0]->set(0);
  for (size_t i = 1; i < nBBL; ++i) domBitSets[i]->set();

  // compute dominators (fixpoint algorithm)
  bool changed = true;
  while (changed) {
    changed = false;
    for (size_t i = 1; i < nBBL; ++i) {
      boost::dynamic_bitset<> tmp(*domBitSets[i].get());
      for (id_t pred : get(i)->predecessors) *domBitSets[i].get() &= *domBitSets[pred].get();
      domBitSets[i]->set(i);
      if (tmp != *domBitSets[i].get()) changed = true;
    }
  }

  // fill dominators in bbl's
  for (size_t i = 0; i < nBBL; ++i)
    for (size_t j = 0; j < nBBL; ++j)
      if (domBitSets[i]->test(j)) get(i)->dominators.insert(j);

  // compute immediate dominators
  for (size_t i = 1; i < nBBL; ++i) {
    for (id_t m : get(i)->dominators) {
      bool isIdom = true;
      if (m == i) continue;

      for (id_t p : get(i)->dominators) {
        if (p == i || p == m) continue;
        if (!domBitSets[m]->test(p)) {
          isIdom = false;
          break;
        }
      }

      if (isIdom) {
        get(i)->iDom = get(m);
        break;
      }
    }
  }
}

void BasicBlock::collectLoop(id_t header, id_t current, BasicBlockLoop *loop)
{
  if (loop->nodes.find(current) != loop->nodes.end()) return;

  loop->nodes.insert(current);
  BasicBlock *currentBBL = BasicBlock::get(current);
  for (auto predecessor : currentBBL->predecessors) collectLoop(header, predecessor, loop);
}

void BasicBlock::identifyLoops(BasicBlock::loops_t *loops)
{
  for (size_t i = 0; i < BasicBlock::size(); ++i) {
    BasicBlock *iBBL = BasicBlock::get(i);
    if (iBBL->successor && iBBL->dominators.find(iBBL->successor->id) != iBBL->dominators.end()) {
      auto *loop = new BasicBlockLoop();
      loop->head = iBBL->successor->id;
      loop->tail = iBBL->id;
      loop->nodes.insert(loop->head);
      loop->nodes.insert(loop->tail);

      for (auto predecessor : BasicBlock::get(loop->tail)->predecessors)
        collectLoop(loop->head, predecessor, loop);

      for (id_t node : loop->nodes) {
        BasicBlock *target    = BasicBlock::get(node)->target;
        BasicBlock *successor = BasicBlock::get(node)->successor;

        if (target && loop->nodes.find(target->id) == loop->nodes.end()) {
          loop->exits.insert(target->id);
          (*loops)[target->entry_ins] = loop;
        }
        if (successor && loop->nodes.find(successor->id) == loop->nodes.end()) {
          loop->exits.insert(successor->id);
          (*loops)[successor->entry_ins] = loop;
        }
      }

      (*loops)[BasicBlock::get(loop->head)->entry_ins] = loop;
    }
  }
}

void BasicBlock::identifyLoops(RTN rtn,
                               const leaders_t &leaders,
                               const jumps_t &jumps,
                               loops_t *loops)
{
  BasicBlock::buildStaticBBLs(rtn, leaders, jumps);
  BasicBlock::getDominators(root);
  identifyLoops(loops);
}

std::string getNodeStr(BasicBlock *node)
{
  std::stringstream ss;
  ss << "\t\"" << node->id << ": " << dissmap[node->entry_ins] << "\"";
  return ss.str();
}

void BasicBlock::printCFG(std::ofstream *ofs) { printCFG(root, ofs, 0); }

void BasicBlock::printDOM(std::ofstream *ofs) { printDOM(root, ofs, 0); }

void BasicBlock::printCFG(BasicBlock *node, std::ofstream *ofs, int level)
{
  static std::set<id_t> dumpedNodes;

  if (dumpedNodes.find(node->id) != dumpedNodes.end())
    return;
  else
    dumpedNodes.insert(node->id);

  if (level == 0) {
    *ofs << "digraph CFG {" << std::endl;
    *ofs << "\tgraph [fontname=\"fixed\"];" << std::endl;
    *ofs << "\tnode [fontname=\"fixed\"];" << std::endl;
    *ofs << "\tedge [fontname=\"fixed\"];" << std::endl;
  }
  *ofs << getNodeStr(node) << ";" << std::endl;

  if (node->successor) {
    printCFG(node->successor, ofs, level + 1);
    *ofs << getNodeStr(node) << "->" << getNodeStr(node->successor) << ";" << std::endl;
  }

  if (node->target) {
    printCFG(node->target, ofs, level + 1);
    *ofs << getNodeStr(node) << "->" << getNodeStr(node->target) << " [style=dotted];" << std::endl;
  }

  if (level == 0) *ofs << "}";
}

void BasicBlock::printDOM(BasicBlock *node, std::ofstream *ofs, int level)
{
  static std::set<id_t> dumpedNodes;

  if (dumpedNodes.find(node->id) != dumpedNodes.end())
    return;
  else
    dumpedNodes.insert(node->id);

  if (level == 0) {
    *ofs << "digraph DOM {" << std::endl;
    *ofs << "\tgraph [fontname=\"fixed\"];" << std::endl;
    *ofs << "\tnode [fontname=\"fixed\"];" << std::endl;
    *ofs << "\tedge [fontname=\"fixed\"];" << std::endl;
  }
  *ofs << getNodeStr(node) << ";" << std::endl;

  if (node->iDom) {
    *ofs << "\t" << getNodeStr(node->iDom) << "->" << getNodeStr(node) << ";" << std::endl;
  }

  if (node->successor) BasicBlock::printDOM(node->successor, ofs, level + 1);

  if (node->target) BasicBlock::printDOM(node->target, ofs, level + 1);

  if (level == 0) *ofs << "}";
}

/*******************************************************************************
 * global functions
 ******************************************************************************/

/* getBBLFrontiers()
 *
 * iterates over all instruction in a given routine rtn to:
 *	- identify the entry instruction of every static (compiler) BBL (=leaders)
 * - identify all unconditional jumps (=jumps)
 */
void getBBLFrontiers(RTN rtn, leaders_t *leaders, jumps_t *jumps)
{
  bool isFirst = true;

  RTN_Open(rtn);
  for (INS in = RTN_InsHead(rtn); INS_Valid(in); in = INS_Next(in)) {
    dissmap[INS_Address(in)] = INS_Disassemble(in);

    if (isFirst) {
      leaders->insert(INS_Address(in));
      isFirst = false;
    }

    if (INS_IsDirectBranchOrCall(in)) {
      if (!INS_IsCall(in)) {
        ADDRINT target = INS_DirectBranchOrCallTargetAddress(in);
        leaders->insert(target);
        (*jumps)[INS_Address(in)] = target;
      }
      isFirst = true;
    }
  }
  RTN_Close(rtn);
}

uint32_t buildLoopNo(Loop::Id_t loopId, uint32_t rtnId)
{
  typedef std::pair<uint32_t, Loop::Id_t> ident_t;
  static map<ident_t, int> loopMap;
  static atomic<uint32_t> loopCounter(0);

  ident_t identifier = std::make_pair(rtnId, loopId);
  if (loopMap.find(identifier) == loopMap.end()) loopMap[identifier] = loopCounter++;

  return loopMap[identifier];
}

LoopDetector::LoopDetector(Parceive *parceive) : Module(parceive), pModel(nullptr), pThread(nullptr)
{}

LoopDetector::~LoopDetector() {}

void LoopDetector::initialize()
{
  pModel  = pParceive->getModel();
  pThread = pParceive->getThread();
}

void LoopDetector::finalize() {}

void LoopDetector::callOnLoopEntry(
  LoopDetector *pThis, THREADID THREADID, Loop::Id_t loopId, uint32_t rtnId, uint32_t line)
{
  if (!pThis->pParceive->getAnalysisEnabled()) return;

  Loop::Id_t loopNo = buildLoopNo(loopId, rtnId);

  auto threadData = pThis->pThread->getThreadData(THREADID);
  auto callStack  = threadData->getCallStack();

  pThis->pModel->processLoopEntry(THREADID, loopNo, line, callStack->top()->callId);
}

void LoopDetector::callOnLoopExit(LoopDetector *pThis,
                                  THREADID THREADID,
                                  Loop::Id_t loopId,
                                  uint32_t rtnId)
{
  if (!pThis->pParceive->getAnalysisEnabled()) return;

  loopNo_t loopNo = buildLoopNo(loopId, rtnId);

  auto threadData = pThis->pThread->getThreadData(THREADID);
  auto callStack  = threadData->getCallStack();

  pThis->pModel->processLoopExit(THREADID, loopNo, callStack->top()->callId);
}

void LoopDetector::instrumentLoops(RTN rtn, BasicBlock::loops_t *loops, unsigned int eRtnId)
{
  RTN_Open(rtn);
  int rtnId = RTN_Id(rtn);

  for (INS in = RTN_InsHead(rtn); INS_Valid(in); in = INS_Next(in)) {
    ADDRINT addr = INS_Address(in);
    if (loops->find(addr) != loops->end()) {
      BasicBlockLoop *loop = (*loops)[addr];

      // check entry
      BasicBlock *head = BasicBlock::get(loop->head);
      if (head->entry_ins == addr) {
        // Locking the Pin client to obtain the line number
        int line = 0;
        if (pParceive->getLookupSourceLocations()) {
          pParceive->getSymInfo()->getSourceLocation(addr, &line, nullptr);
        }

        INS_InsertCall(in,
                       IPOINT_BEFORE,
                       (AFUNPTR)callOnLoopEntry,
                       IARG_PTR,
                       this,
                       IARG_THREAD_ID,
                       IARG_UINT32,
                       loop->head,
                       IARG_UINT32,
                       rtnId,
                       IARG_UINT32,
                       line,
                       IARG_END);
      } else {
        for (BasicBlock::id_t exit : loop->exits) {
          BasicBlock *tmp = BasicBlock::get(exit);
          if (tmp->entry_ins == addr)
            INS_InsertCall(in,
                           IPOINT_BEFORE,
                           (AFUNPTR)callOnLoopExit,
                           IARG_PTR,
                           this,
                           IARG_THREAD_ID,
                           IARG_UINT32,
                           loop->head,
                           IARG_UINT32,
                           rtnId,
                           IARG_CALL_ORDER,
                           CALL_ORDER_FIRST,  // cons. accesses
                           IARG_END);
        }
      }
    }
  }

  RTN_Close(rtn);
}

void LoopDetector::instrumentRoutine(IMG img, RTN rtn)
{
  int rtnId = RTN_Id(rtn);
  std::unique_ptr<leaders_t> leaders(new leaders_t);
  std::unique_ptr<jumps_t> jumps(new jumps_t);
  BasicBlock::reset();

  getBBLFrontiers(rtn, leaders.get(), jumps.get());
  BasicBlock::loops_t loops;
  BasicBlock::identifyLoops(rtn, *leaders.get(), *jumps.get(), &loops);

  instrumentLoops(rtn, &loops, rtnId);

  /* uncommented to have an example for printing the cfg and dom of foo()
        if (RTN_Name(rtn) == "foo") {
          std::ofstream ofs;
          std::ofstream ofsdom;
          ofs.open ("fct.dot", std::ofstream::out);
          ofsdom.open ("fctdom.dot", std::ofstream::out);
          BasicBlock::printCFG(&ofs);
          BasicBlock::printDOM(&ofsdom);
          ofs.close();
          ofsdom.close();
        }*/
}

}  // namespace pcv
