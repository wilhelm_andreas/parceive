/*
 * Module.cpp
 *
 *  Created on: Nov 17, 2016
 *      Author: adriaan
 */

#include "Module.h"
#include "Parceive.h"

namespace pcv {

Module::Module(Parceive *parceive) : pParceive(parceive) { parceive->registerModule(this); }

}  // namespace pcv
