//
// Created by wilhelma on 11/24/17.
//

#include "entities/SoftwareEntity.h"
#include "entities/File.h"
#include "entities/Image.h"
#include "entities/Namespace.h"

namespace pcv {
namespace entity {

SoftwareEntity::SoftwareEntity(const Id_t id,
                               const name_t &name,
                               Image *img,
                               Namespace *nmsp,
                               Class *cls,
                               File *file,
                               const line_t line,
                               const name_t &full_name)
  : id(id),
    name(std::move(name)),
    full_name(full_name),
    img(img),
    nmsp(nmsp),
    cls(cls),
    file(file),
    line(line)
{
  if (nmsp != nullptr) nmsp->entities.push_back(this);
  if (img != nullptr) img->entities.push_back(this);
}

ImageScopedId SoftwareEntity::getUniqueId() const { return {(img ? img->id : NO_ID), this->id}; }

}  // namespace entity
}  // namespace pcv
