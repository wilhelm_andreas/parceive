#include <fstream>
#include <iostream>
#include <json11.hpp>

#include "JsonFilterRuleDeserializer.h"

using json11::Json;
using std::ifstream;
using std::string;
using std::vector;

namespace pcv {

Json readJsonFrom(const string& jsonFilepath, string& errors)
{
  ifstream jsonStream(jsonFilepath);
  const string jsonText((std::istreambuf_iterator<char>(jsonStream)),
                        std::istreambuf_iterator<char>());
  return Json::parse(jsonText, errors);
}

Filter::Action getFilterAction(const string& type)
{
  if (type == "partial") return Filter::PARTIAL;
  if (type == "include") return Filter::INCLUDE;
  if (type == "exclude") return Filter::EXCLUDE;
  if (type == "enable") return Filter::ENABLE;
  if (type == "disable") return Filter::DISABLE;
  if (type == "start") return Filter::START;
  if (type == "stop") return Filter::STOP;
  return Filter::NONE;
}

string getNamedStringOrEmpty(const Json& node, const string& name)
{
  auto attribute = node[name];
  return attribute.is_string() ? attribute.string_value() : string();
}

static void throwInvalidFilterException(const string& name, const string& type)
{
  string err("Invalid filter pattern: Missing '");
  err += name + "' attribute of type " + type + ".";
  throw std::runtime_error(err.c_str());
}

string getNamedString(const Json& node, const string& name)
{
  auto attribute = node[name];
  if (attribute.is_string()) {
    return attribute.string_value();
  }
  throwInvalidFilterException(name, "string");
  return string();  // strictly to prevent warnings
}

int getNamedInt(const Json& node, const string& name)
{
  auto attribute = node[name];
  if (attribute.is_number()) {
    return attribute.int_value();
  }
  throwInvalidFilterException(name, "int");
  return 0;  // strictly to prevent warnings
}

JsonFilterData getFilterPattern(const Json& patternNode)
{
  // this function deliberately skips any 'children' nodes we could encounter since
  // we're assuming that all of these entries are covered by filtering on the parent
  // (=this) scope.

  JsonFilterData data;
  data.scope = static_cast<JsonFilterScope>(getNamedInt(patternNode, "type"));
  if (data.scope == JsonFilterScope::NONE) {
    throw std::runtime_error(
      "Invalid filter pattern: Value of 'type' attribute must not be None (0).");
  }
  data.pattern   = getNamedString(patternNode, "name");
  data.nameSpace = getNamedStringOrEmpty(patternNode, "namespace");

  return data;
}

void iterateFilterDefinitions(const Json& filterChild, JsonFilterRule *definitions)
{
  if (filterChild.is_array()) {
    for (const auto &patternNode : filterChild.array_items()) {
      std::string err;
      if (patternNode.has_shape({{"type", Json::Type::NUMBER}}, err)) {
        definitions->entries.emplace_back(getFilterPattern(patternNode));
      } else if (patternNode.has_shape({{"children", Json::Type::ARRAY}}, err)) {
        iterateFilterDefinitions(patternNode["children"], definitions);
      }
    }
  }
}

JsonFilterRule getFilterDefinitions(const Json& filterChild)
{
  JsonFilterRule definitions;
  definitions.action = getFilterAction(getNamedString(filterChild, "name"));

  std::string err;
  if (!filterChild.has_shape({{"children", Json::Type::ARRAY}}, err)) {
    throw std::runtime_error(
        "Invalid filter definition: missing 'children' attribute of type array.");
  }

  iterateFilterDefinitions(filterChild["children"], &definitions);

  return definitions;
}

JsonFilterRules deserializeFilters(const Json& json)
{
  JsonFilterRules rules;
  string errors;

  if (json.is_array()) {
    for (const auto& filterChild : json.array_items()) {
      auto filterDef = getFilterDefinitions(filterChild);
      rules.emplace_back(filterDef);
    }
  }
  return rules;
}

JsonFilterRules deserializeJsonFilterRules(const string& filterRuleFilepath)
{
  string errors;
  const auto json = readJsonFrom(filterRuleFilepath, errors);
  if (!errors.empty()) {
    cerr << "Errors while loading" << filterRuleFilepath << ":" << errors << endl;
  }

  return deserializeFilters(json);
}

}  // namespace pcv
