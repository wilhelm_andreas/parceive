#include "Context.h"
#include <cassert>

using namespace pcv;
using namespace pcv::entity;
using std::unique_ptr;
using std::vector;

const Class* Context::getClass(const ImageScopedId& id) const
{
  assert(id.imageId <= classesPerImage.size());

  const auto& classes = classesPerImage[id.imageId - 1];
  const auto it       = classes.find(id.id);
  return it == classes.end() ? nullptr : it->second;
}

ImageScopedId Context::getClassId(const pcv::Id_t imageId, const std::string& fullRtnName) const
{
  const auto& rtn = getRoutine(imageId, fullRtnName);
  if (rtn && rtn->cls && rtn->isConstructor) {
    return rtn->cls->getUniqueId();
  }
  return ImageScopedId::getNone();
  ;
}

const Routine* Context::getRoutine(const pcv::Id_t imageId, const std::string& fullRtnName) const
{
  assert(imageId <= routinesPerImage.size());

  const auto& routines = routinesPerImage[imageId - 1];
  const auto it        = routines.find(fullRtnName);
  return it == routines.end() ? nullptr : it->second;
}

unique_ptr<vector<const Variable*>> Context::getGlobalVariables(pcv::Id_t imageId) const
{
  assert(imageId <= globalVariablesPerImage.size());

  const auto& globals = globalVariablesPerImage[imageId - 1];
  unique_ptr<vector<const Variable*>> vec{new vector<const Variable*>()};
  vec->reserve(globals.size());
  for (const auto& kvp : globals) {
    vec->emplace_back(kvp.second);
  }
  return vec;
}

unique_ptr<vector<const Variable*>> Context::getStaticVariables(pcv::Id_t imageId) const
{
  assert(imageId <= staticVariablesPerImage.size());

  const auto& statics = staticVariablesPerImage[imageId - 1];
  unique_ptr<vector<const Variable*>> vec{new vector<const Variable*>()};
  vec->reserve(statics.size());
  for (const auto& kvp : statics) {
    vec->emplace_back(kvp.second);
  }
  return vec;
}

const Variable* Context::getMemberVariable(const ImageScopedId& classId,
                                           Class::offset_t offset) const
{
  const auto cls = getClass(classId);

  if (cls != nullptr) {
    auto it =
      std::upper_bound(cls->members.begin(),
                       cls->members.end(),
                       offset,
                       [](const Class::offset_t& off, Variable* var) { return off < var->offset; });
    if (it != cls->members.begin()) {
      return *(--it);
    }
  }
  return nullptr;
}

void Context::prepareForNewImage(pcv::Id_t imageId)
{
  // encoded assumption: image ids start with 1 and increment by 1 for each successor
  routinesPerImage.resize(imageId);
  globalVariablesPerImage.resize(imageId);
  staticVariablesPerImage.resize(imageId);
  classesPerImage.resize(imageId);
}

void Context::addImage(Image* const img)
{
  images.emplace_back(img);
  if (img->file) {
    files.emplace_back(img->file);
  }
}

void Context::addNamespace(Namespace* const ns)
{
  namespaces.emplace_back(ns);
  if (ns->file) {
    files.emplace_back(ns->file);
  }
}

void Context::addRoutine(Routine* const rtn)
{
  routines.emplace_back(rtn);
  if (rtn->img) {
    routinesPerImage[rtn->img->id - 1][rtn->getFullName()] = rtn;
  }
  if (rtn->file) {
    files.emplace_back(rtn->file);
  }
}

void Context::addClass(Class* const cls)
{
  classes.emplace_back(cls);
  if (cls->img) {
    classesPerImage[cls->img->id - 1][cls->id] = cls;
  }
  if (cls->file) {
    files.emplace_back(cls->file);
  }
}

void Context::addVariable(Variable* const var)
{
  variables.emplace_back(var);
  if (var->img) {
    if (Variable::Type::STATICVAR == (var->type & Variable::Type::STATICVAR)) {
      staticVariablesPerImage[var->img->id - 1][var->id] = var;
    }
    if (Variable::Type::GLOBAL == (var->type & Variable::Type::GLOBAL)) {
      globalVariablesPerImage[var->img->id - 1][var->id] = var;
    }
  }
  if (var->file) {
    files.emplace_back(var->file);
  }
}