#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <fstream>
#include <string>

#include "Common.h"
#include "DebugOptions.h"
#include "XmlFilterSystem.h"

#ifdef TRACING_LEVEL1
#define TRACELOG(msg, ...) PLOGP(msg, __VA_ARGS__)  // NOLINT(runtime/printf)
#else
#define TRACELOG(msg, ...)
#endif

using std::istream;

namespace pcv {

XmlFilterSystem::XmlFilterSystem(const string &filename)
{
  if (boost::filesystem::exists(filename)) {
    const ifstream input(filename);
    readXml(input);
  }
}

bool XmlFilterSystem::readXml(const istream &is)
{
  // Populate tree structure from XML
  using boost::property_tree::ptree;
  ptree pt;

  // ************Need to apply PIN EXCEPTION HANDLER
  try {
    read_xml(const_cast<istream &>(is), pt);
  } catch (exception &) {
    return false;
  }
  // *******************************

  typedef struct BlockData {
    FilterRuleCollection *ruleBlock;
    const char *path;
  } BlockData;

  BlockData blocks[]        = {{
                          &regions,
                          "filter.region",
                        },
                        {
                          &images,
                          "filter.image",
                        },
                        {&files, "filter.filepath"}};
  const char *actionNames[] = {
    "partial", "include", "exclude", "enable", "disable", "start", "stop"};

  for (const auto &data : blocks) {
    if (pt.get_child_optional(data.path)) {
      BOOST_FOREACH (ptree::value_type const &v, pt.get_child(data.path)) {
        const int actionIdx = indexOf(actionNames, v.first);
        if (actionIdx >= 0) {
          auto action = static_cast<Filter::Action>(actionIdx);
          data.ruleBlock->addXMLRule(action, v.second.data());
        } else {
          TRACELOG("[XmlFilter] Warning: Skipping unexpected action '%s'.", v.first.c_str());
        }
      }
    }
  }
  return true;
}

}  // namespace pcv
