#include <iostream>
#include <string>
#include <utility>

#include "SQLWriter.h"

#include "Exception.h"

namespace pcv {

using namespace model;

// This enables DB commits after each insert
//#define SLOW_COMMITS

SQLWriter::SQLWriter(const std::string &file, bool createDb)
  : _db(std::make_shared<SQLite::Connection>(file.c_str(), true))
{
  PIN_MutexInit(&_mutex);

  runPragmas();

  if (createDb) createDatabase();

  clearDatabase();

  prepareStatements();

  begin();
}

SQLWriter::SQLWriter(std::shared_ptr<SQLite::Connection> db, bool createDb)
  : _db(std::move(std::move(db)))
{
  PIN_MutexInit(&_mutex);

  runPragmas();

  if (createDb) {
    createDatabase();
  }

  clearDatabase();

  prepareStatements();

  begin();
}

void SQLWriter::prepareStatements()
{
  _beginTransactionStmt  = _db->makeStatement("BEGIN EXCLUSIVE TRANSACTION");
  _commitTransactionStmt = _db->makeStatement("COMMIT TRANSACTION");

  _insertAccessStmt = _db->makeStatement(
    "INSERT INTO Access(Id, Instruction, Position, Reference, Type, State, Offset) "
    "VALUES(?, ?, ?, ?, ?, ?, ?);");
  _insertCallStmt = _db->makeStatement(
    "INSERT INTO Call(Id, Thread, Function, Instruction, Start, End) "
    "VALUES(?, ?, ?, ?, ?, ?);");
  _insertFileStmt     = _db->makeStatement("INSERT INTO File(Id, Path, Image) VALUES(?, ?, ?);");
  _insertImageStmt    = _db->makeStatement("INSERT INTO Image(Id, Name) VALUES(?, ?);");
  _insertFunctionStmt = _db->makeStatement(
    "INSERT INTO Function(Id, Name, Prototype, File, Line, Type, Symbol, Image) "
    "VALUES(?, ?, ?, ?, ?, ?, ?, ?);");
  _insertInstructionStmt = _db->makeStatement(
    "INSERT INTO Instruction(Id, Segment, Type, Line, Column) "
    "VALUES(?, ?, ?, ?, ?);");
  _insertSegmentStmt = _db->makeStatement(
    "INSERT INTO Segment(Id, Call, Type, LoopIteration) "
    "VALUES(?, ?, ?, ?);");
  _insertLoopStmt          = _db->makeStatement("INSERT INTO Loop(Id, Line) VALUES(?, ?);");
  _insertLoopExecutionStmt = _db->makeStatement(
    "INSERT INTO LoopExecution(Id, Loop, ParentIteration, Start, End) "
    "VALUES(?, ?, ?, ?, ?);");
  _insertLoopIterationStmt =
    _db->makeStatement("INSERT INTO LoopIteration(Id, Execution, Iteration) VALUES(?, ?, ?);");
  _insertMemberStmt    = _db->makeStatement("INSERT INTO Member(Id, Name) VALUES(?, ?);");
  _insertReferenceStmt = _db->makeStatement(
    "INSERT INTO Reference(Id, Size, Type, Name, Allocator, Symbol, Image) "
    "VALUES(?, ?, ?, ?, ?, ?, ?);");
  _insertThreadStmt = _db->makeStatement(
    "INSERT INTO Thread(Id, Start, End,"
    "CreateInstruction, JoinInstruction, ParentThread, Process, Call) "
    "VALUES(?, ?, ?, ?, ?, ?, ?, ?);");
  _removeLoopIterationStmt = _db->makeStatement("DELETE FROM LoopIteration WHERE Id = ?;");
}

SQLWriter::~SQLWriter()
{
  commit();

  PIN_MutexFini(&_mutex);
}

void SQLWriter::begin() { _beginTransactionStmt->execute(); }

void SQLWriter::commit() { _commitTransactionStmt->execute(); }

void SQLWriter::createDatabase()
{
  _db->execute(
#include "create.sql.h"
  );
}

void SQLWriter::runPragmas()
{
  _db->execute(
#include "writePragmas.sql.h"
  );
}

void SQLWriter::clearDatabase()
{
  _db->execute(
#include "clear.sql.h"
  );
}

void SQLWriter::lock() { PIN_MutexLock(&_mutex); }

void SQLWriter::unlock() { PIN_MutexUnlock(&_mutex); }

void SQLWriter::insert(const Image &&image)
{
  lock();
  _insertImageStmt << image.id << image.name;
  _insertImageStmt->executeInsert();
#ifdef SLOW_COMMITS
  commit();
#endif
  unlock();
}

void SQLWriter::insert(const File &&file)
{
  lock();
  _insertFileStmt << file.id << file.name << file.image;
  _insertFileStmt->executeInsert();
#ifdef SLOW_COMMITS
  commit();
#endif
  unlock();
}

void SQLWriter::insert(const Function &&function)
{
  lock();
  _insertFunctionStmt << function.id << function.name << function.prototype << function.file
                      << function.line << static_cast<int>(function.type) << function.symbol
                      << function.image;
  _insertFunctionStmt->executeInsert();
#ifdef SLOW_COMMITS
  commit();
#endif
  unlock();
}

void SQLWriter::insert(const model::Thread &&thread)
{
  lock();
  if (thread.createInstruction != Instruction::EMPTY)
    _insertThreadStmt << thread.id << thread.start << thread.end << thread.createInstruction
                      << thread.joinInstruction << thread.parentThread << thread.processId
                      << thread.call;
  else
    _insertThreadStmt << thread.id << thread.start << thread.end << SQLite::SQLNULL
                      << SQLite::SQLNULL << SQLite::SQLNULL << thread.processId << thread.call;
  _insertThreadStmt->executeInsert();
#ifdef SLOW_COMMITS
  commit();
#endif
  unlock();
}

void SQLWriter::insert(const Call &&call)
{
  lock();
  if (call.instruction != Instruction::EMPTY)
    _insertCallStmt << call.id << call.thread << call.function << call.instruction << call.start
                    << call.end;
  else
    _insertCallStmt << call.id << call.thread << call.function << SQLite::SQLNULL << call.start
                    << call.end;
  _insertCallStmt->executeInsert();
#ifdef SLOW_COMMITS
  commit();
#endif
  unlock();
}

void SQLWriter::insert(const Segment &&segment)
{
  lock();
  if (segment.loopIteration != LoopIteration::NULLVAL)
    _insertSegmentStmt << segment.id << segment.call << static_cast<int>(segment.type)
                       << segment.loopIteration;
  else
    _insertSegmentStmt << segment.id << segment.call << static_cast<int>(segment.type)
                       << SQLite::SQLNULL;
  _insertSegmentStmt->executeInsert();
#ifdef SLOW_COMMITS
  commit();
#endif
  unlock();
}

void SQLWriter::insert(const Instruction &&instruction)
{
  lock();
  if (instruction.column != Instruction::NOCOLUMN)
    _insertInstructionStmt << instruction.id << instruction.segment
                           << static_cast<int>(instruction.type) << instruction.line
                           << instruction.column;
  else
    _insertInstructionStmt << instruction.id << instruction.segment
                           << static_cast<int>(instruction.type) << instruction.line
                           << SQLite::SQLNULL;
  _insertInstructionStmt->executeInsert();
#ifdef SLOW_COMMITS
  commit();
#endif
  unlock();
}

void SQLWriter::insert(const Loop &&loop)
{
  lock();
  _insertLoopStmt << loop.id << loop.line;
  _insertLoopStmt->executeInsert();
#ifdef SLOW_COMMITS
  commit();
#endif
  unlock();
}

void SQLWriter::insert(const LoopExecution &&execution)
{
  lock();
  if (execution.parentIteration != LoopIteration::NULLVAL)
    _insertLoopExecutionStmt << execution.id << execution.loop << execution.parentIteration
                             << execution.start << execution.end;
  else
    _insertLoopExecutionStmt << execution.id << execution.loop << SQLite::SQLNULL << execution.start
                             << execution.end;
  _insertLoopExecutionStmt->executeInsert();
#ifdef SLOW_COMMITS
  commit();
#endif
  unlock();
}

void SQLWriter::insert(const LoopIteration &&iteration)
{
  lock();
  _insertLoopIterationStmt << iteration.id << iteration.execution << iteration.iteration;
  _insertLoopIterationStmt->executeInsert();
#ifdef SLOW_COMMITS
  commit();
#endif
  unlock();
}

void SQLWriter::insert(const Member &&member)
{
  lock();
  _insertMemberStmt << member.id << member.name;
  _insertMemberStmt->executeInsert();
#ifdef SLOW_COMMITS
  commit();
#endif
  unlock();
}

void SQLWriter::insert(const Access &&access)
{
  lock();
  _insertAccessStmt << access.id << access.instruction << access.position << access.reference
                    << static_cast<int>(access.type) << static_cast<int>(access.state)
                    << access.offset;
  _insertAccessStmt->executeInsert();
#ifdef SLOW_COMMITS
  commit();
#endif
  unlock();
}

void SQLWriter::insert(const Reference &&reference)
{
  lock();
  if (reference.allocInstruction == 0) {
    _insertReferenceStmt << reference.id << reference.size << static_cast<int>(reference.type)
                         << reference.name << SQLite::SQLNULL << reference.symbol
                         << reference.image;
  } else {
    _insertReferenceStmt << reference.id << reference.size << static_cast<int>(reference.type)
                         << reference.name << reference.allocInstruction << reference.symbol
                         << reference.image;
  }
  _insertReferenceStmt->executeInsert();
#ifdef SLOW_COMMITS
  commit();
#endif
  unlock();
}

void SQLWriter::remove(const LoopIteration &&iteration)
{
  lock();
  _removeLoopIterationStmt << iteration.id;
  _removeLoopIterationStmt->execute();
  unlock();
}

}  // namespace pcv
