#include <string>
#include <vector>

#include "Common.h"
#include "JsonFilterSystem.h"

using std::string;

namespace pcv {

JsonFilterSystem::JsonFilterSystem(const string& jsonFilepath)
{
  const auto& filterRules = deserializeJsonFilterRules(jsonFilepath);
  fillFilterRuleCollections(filterRules);
}

void JsonFilterSystem::fillFilterRuleCollections(const JsonFilterRules& rules)
{
  for (const auto& rule : rules) {
    const auto action            = rule.action;
    FilterRuleCollection* target = nullptr;
    for (const auto& data : rule.entries) {
      const auto& scope = data.scope;
      if (scope == JsonFilterScope::CLASS || scope == JsonFilterScope::NAMESPACE ||
          scope == JsonFilterScope::ROUTINE) {
        target = &regions;
      } else if (scope == JsonFilterScope::IMAGE) {
        target = &images;
      } else if (scope == JsonFilterScope::VARIABLE) {
        PLOG("Ignoring filter scope variable: not currently implemented.");
        continue;
      }

      addRule(target, action, data, scope);
    }
  }
}

void JsonFilterSystem::addRule(FilterRuleCollection* target,
                               const Filter::Action action,
                               const JsonFilterData& data,
                               const JsonFilterScope& scope)
{
//  string pattern;
//  if (target == &regions) {
//    if (scope == JsonFilterScope::CLASS) {
//      auto nameSpace = data.nameSpace;
//      if (!nameSpace.empty()) {
//        pattern = nameSpace + "::";
//      }
//      pattern += data.pattern + ".*";
//    } else if (scope == JsonFilterScope::NAMESPACE) {
//      pattern = data.pattern + ".*";
//    } else {
//      pattern = data.pattern;
//      ;
//    }
//  } else {
//    pattern = data.pattern;
//    ;
//  }

//  target->addRule(action, pattern);
  target->addJsonRule(action, data.pattern);
}

}  // namespace pcv
