#pragma once

#ifndef DC_CPP_DBGHELP_INCLUDE_DBGHELPTYPES_H_
#define DC_CPP_DBGHELP_INCLUDE_DBGHELPTYPES_H_

#define _NO_CVCONST_H
#define NOMINMAX
#include <DbgHelp.h>
#include <Windows.h>

namespace pcv {
namespace dbghelp {
typedef BOOL(__stdcall* SymInitializeProc)(_In_ HANDLE hProcess,
                                           _In_opt_ PCSTR UserSearchPath,
                                           _In_ BOOL fInvadeProcess);
typedef DWORD(__stdcall* SymSetOptionsProc)(_In_ DWORD SymOptions);
typedef DWORD(__stdcall* SymGetOptionsProc)(VOID);
typedef BOOL(__stdcall* SymCleanupProc)(_In_ HANDLE hProcess);
typedef BOOL(__stdcall* SymEnumTypesByNameProc)(_In_ HANDLE hProcess,
                                                _In_ ULONG64 BaseOfDll,
                                                _In_opt_ PCSTR mask,
                                                _In_ PSYM_ENUMERATESYMBOLS_CALLBACK
                                                  EnumSymbolsCallback,
                                                _In_opt_ PVOID UserContext);
typedef BOOL(__stdcall* SymEnumTypesByNameWProc)(_In_ HANDLE hProcess,
                                                 _In_ ULONG64 BaseOfDll,
                                                 _In_opt_ PCWSTR mask,
                                                 _In_ PSYM_ENUMERATESYMBOLS_CALLBACKW
                                                   EnumSymbolsCallback,
                                                 _In_opt_ PVOID UserContext);
typedef BOOL(__stdcall* SymEnumSymbolsProc)(_In_ HANDLE hProcess,
                                            _In_ ULONG64 BaseOfDll,
                                            _In_opt_ PCSTR Mask,
                                            _In_ PSYM_ENUMERATESYMBOLS_CALLBACK EnumSymbolsCallback,
                                            _In_opt_ PVOID UserContext);
typedef BOOL(__stdcall* SymEnumLinesProc)(_In_ HANDLE hProcess,
                                          _In_ ULONG64 Base,
                                          _In_opt_ PCSTR Obj,
                                          _In_opt_ PCSTR File,
                                          _In_ PSYM_ENUMLINES_CALLBACK EnumLinesCallback,
                                          _In_opt_ PVOID UserContext);
typedef BOOL(__stdcall* SymSetContextProc)(_In_ HANDLE hProcess,
                                           _In_ PIMAGEHLP_STACK_FRAME StackFrame,
                                           _In_opt_ PIMAGEHLP_CONTEXT Context);
typedef BOOL(__stdcall* SymGetTypeInfoProc)(_In_ HANDLE hProcess,
                                            _In_ DWORD64 ModBase,
                                            _In_ ULONG TypeId,
                                            _In_ IMAGEHLP_SYMBOL_TYPE_INFO GetType,
                                            _Out_ PVOID pInfo);
typedef BOOL(__stdcall* SymFromAddrProc)(_In_ HANDLE hProcess,
                                         _In_ DWORD64 Address,
                                         _Out_opt_ PDWORD64 Displacement,
                                         _Inout_ PSYMBOL_INFO Symbol);
#ifdef _WIN64
typedef BOOL(__stdcall* SymGetLineFromAddr64Proc)(_In_ HANDLE hProcess,
                                                  _In_ DWORD64 qwAddr,
                                                  _Out_ PDWORD pdwDisplacement,
                                                  _Out_ PIMAGEHLP_LINE Line);
typedef DWORD64(__stdcall* SymLoadModule64Proc)(_In_ HANDLE hProcess,
                                                _In_opt_ HANDLE hFile,
                                                _In_opt_ PCSTR ImageName,
                                                _In_opt_ PCSTR ModuleName,
                                                _In_ DWORD64 BaseOfDll,
                                                _In_ DWORD SizeOfDll);
typedef BOOL(__stdcall* SymUnloadModule64Proc)(_In_ HANDLE hProcess, _In_ DWORD64 BaseOfDll);
#else
typedef BOOL(__stdcall* SymGetLineFromAddrProc)(_In_ HANDLE hProcess,
                                                _In_ DWORD dwAddr,
                                                _Out_ PDWORD pdwDisplacement,
                                                _Out_ PIMAGEHLP_LINE Line);
typedef DWORD64(__stdcall* SymLoadModuleProc)(_In_ HANDLE hProcess,
                                              _In_opt_ HANDLE hFile,
                                              _In_opt_ PCSTR ImageName,
                                              _In_opt_ PCSTR ModuleName,
                                              _In_ DWORD BaseOfDll,
                                              _In_ DWORD SizeOfDll);
typedef BOOL(__stdcall* SymUnloadModuleProc)(_In_ HANDLE hProcess, _In_ DWORD BaseOfDll);
#endif
}  // namespace dbghelp
}  // namespace pcv

#endif  // DC_CPP_DBGHELP_INCLUDE_DBGHELPTYPES_H_
