#pragma once
#ifndef DC_CPP_DBGHELP_INCLUDE_DBGHELPAPI_H_
#define DC_CPP_DBGHELP_INCLUDE_DBGHELPAPI_H_

#include "DbgHelpTypes.h"

namespace pcv {
namespace dbghelp {

bool loadDbgHelpApi();
void unloadDbgHelpApi();

#define FUNCTIONS_BASE   \
  X(SymInitialize)       \
  X(SymSetOptions)       \
  X(SymGetOptions)       \
  X(SymCleanup)          \
  X(SymEnumTypesByName)  \
  X(SymEnumTypesByNameW) \
  X(SymEnumSymbols)      \
  X(SymEnumLines)        \
  X(SymSetContext)       \
  X(SymGetTypeInfo)      \
  X(SymFromAddr)

#ifdef _WIN64
#define FUNCTIONS      \
  FUNCTIONS_BASE       \
  X(SymLoadModule64)   \
  X(SymUnloadModule64) \
  X(SymGetLineFromAddr64)

#define DbgSymLoadModule DbgSymLoadModule64
#define DbgSymUnloadModule DbgSymUnloadModule64
#define DbgSymGetLineFromAddr DbgSymGetLineFromAddr64

#else
#define FUNCTIONS    \
  FUNCTIONS_BASE     \
  X(SymLoadModule)   \
  X(SymUnloadModule) \
  X(SymGetLineFromAddr)
#endif

#define X(NAME) extern NAME##Proc Dbg##NAME;
FUNCTIONS
#undef X
}  // namespace dbghelp
}  // namespace pcv

#endif  // DC_CPP_DBGHELP_INCLUDE_DBGHELPAPI_H_
