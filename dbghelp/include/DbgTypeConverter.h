#pragma once

#include <algorithm>
#include <string>
#include <unordered_map>
#include <vector>

#include "Common.h"
#include "Context.h"
#include "DbgCollector.h"
#include "entities/Class.h"
#include "entities/Image.h"
#include "entities/ImageScopedId.h"
#include "entities/Namespace.h"
#include "entities/Routine.h"
#include "entities/Variable.h"

namespace pcv {
namespace dbghelp {

class DbgTypeConverter
{
 public:
  DbgTypeConverter();

  void convertDbgHelpTypes(const DbgCollector& coll, pcv::Id_t associatedImageId);
  const Context& getContext() const;

 private:
  void convertType(DbgType* type);
  void fillClassMembers(DbgType* type);
  entity::Class* DbgTypeConverter::createClassFrom(DbgType* type) const;
  entity::Variable* createVariableFrom(DbgVariable* var, const entity::Variable::Type type) const;
  entity::Routine* createRoutineFrom(DbgFunction* func) const;
  void storeRoutine(entity::Routine* routine, const std::string& fullName);
  void fillContext();
  void DbgTypeConverter::buildNamespaceConversionLookup(const DbgNamespace* dbgParent,
                                                        entity::Namespace* parent);
  entity::Namespace* DbgTypeConverter::lookupNamespace(const DbgNamespace* ns) const;

  entity::Image* image_;
  entity::Namespace* empty_namespace_;
  std::vector<pcv::entity::Variable*> global_vars_;
  std::vector<pcv::entity::Variable*> locals_;
  std::unordered_map<std::string, entity::Routine*> routines_;
  std::unordered_map<entity::ImageScopedId, entity::Class*> index_to_class_;
  std::unordered_map<std::string, entity::Class*> name_to_class_;
  std::unordered_map<const DbgNamespace*, entity::Namespace*> namespace_conversion_lookup_;
  Context ctxt_;
};

}  // namespace dbghelp
}  // namespace pcv
