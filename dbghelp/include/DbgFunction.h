#pragma once

#ifndef DC_CPP_DBGHELP_INCLUDE_DBGFUNCTION_H_
#define DC_CPP_DBGHELP_INCLUDE_DBGFUNCTION_H_

#include <cstdint>
#include <set>
#include <string>
#include <vector>

#include "DbgNamespace.h"
#include "DbgUtil.h"
#include "DbgVariable.h"

namespace pcv {
namespace dbghelp {

struct DbgType;

struct DbgFunction {
  void* process;
  std::string full_name;
  std::string name;
  std::string file_name;
  uint32_t index;
  uint32_t type_index;
  size_t offset;
  size_t size;
  size_t flags;
  uint32_t line;
  DbgType* method_of;
  DbgNamespace* owningNamespace;
  std::vector<DbgVariable*> locals;
  std::set<uint32_t> collectedLocalIndices;
  bool isConstructor;

  DbgFunction(const std::string& full_name,
              const std::string& file_name,
              const uint32_t index,
              const uint32_t type_index,
              const size_t offset,
              const size_t size,
              const size_t flags,
              const bool isStructor)
    : full_name(full_name),
      file_name(file_name),
      index(index),
      type_index(type_index),
      offset(offset),
      size(size),
      flags(flags),
      isConstructor(isStructor),
      line(0),
      method_of(nullptr),
      owningNamespace(nullptr)
  {
    name = getUnqualifiedName(full_name);
  }

  ~DbgFunction() { deleteContainerEntries(locals); }
};

}  // namespace dbghelp
}  // namespace pcv

#endif  // DC_CPP_DBGHELP_INCLUDE_DBGFUNCTION_H_
