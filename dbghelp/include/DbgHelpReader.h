#ifndef DC_CPP_DBGHELP_INCLUDE_DBGHELPREADER_H_
#define DC_CPP_DBGHELP_INCLUDE_DBGHELPREADER_H_

#include <json11.hpp>
#include <memory>
#include <string>
#include <vector>

#include "DbgCollector.h"
#include "DbgTypeConverter.h"
#include "FileFilter.h"
#include "SymInfo.h"

namespace pcv {
namespace dbghelp {

struct ImageInfo {
  Id_t imageId;
  std::string filePath;
  ImageInfo(Id_t imageId, std::string filePath) : imageId(imageId), filePath(filePath) {}
};

/*!
 * @brief DbgHelpReader is responsible for reading DbgHelp information and providing an API for
 * providing symbol information.
 */
class DbgHelpReader : public SymInfo
{
 public:
  DbgHelpReader();
  explicit DbgHelpReader(const std::string &fileName, const FileFilter *filter);
  ~DbgHelpReader();

  /* disable copy/move construction and assignment operators */
  DbgHelpReader(const DbgHelpReader &) = delete;
  DbgHelpReader(DbgHelpReader &&)      = delete;
  DbgHelpReader &operator=(const DbgHelpReader &) = delete;
  DbgHelpReader &operator=(DbgHelpReader &&) = delete;

  void start(const json11::Json &rules);

  const Context &getContext() const override;
  bool readSymInfo(const std::string &fileName,
                   const Id_t imageId,
                   const SymInfo::offset_t offset) override;
  void clearSymInfo() override;
  bool isBinaryOptimized(const std::string &fileName) override;
  bool getSourceLocation(const S_ADDRUINT &ip, int32_t *line, std::string *fileName) const override;

  std::unique_ptr<std::vector<const entity::Variable *>> getGlobalVariables(
    Id_t imageId) const override;
  std::unique_ptr<std::vector<const entity::Variable *>> getStaticVariables(
    Id_t imageId) const override;
  const entity::Variable *getMemberVariable(
    const entity::ImageScopedId &classId,
    entity::Class::offset_t offset /**< [IN] The offset to look for the member var.*/)
    const override;

  std::string refineRtnName(const Id_t imageId, const std::string &rtnName) override;
  const entity::Routine *getRoutine(const Id_t imageId, const std::string rtnName) const override;
  entity::ImageScopedId getUniqueClassId(
    Id_t imageId, /**< [IN] The corresponding image id*/
    const std::string &rtnName /**< [IN] Routine name of the constructor.*/) const override;
  std::vector<const entity::File *> getFiles(Id_t imageId) override;

 private:
  DbgCollector coll;
  DbgTypeConverter converter;
  const std::string nodeJsFileName;
  const FileFilter *filter;
};

}  // namespace dbghelp
}  // namespace pcv

#endif  // DC_CPP_DBGHELP_INCLUDE_DBGHELPREADER_H_
