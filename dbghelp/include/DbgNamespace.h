#pragma once

#ifndef DC_CPP_DBGHELP_INCLUDE_DBGNAMESPACE_H_
#define DC_CPP_DBGHELP_INCLUDE_DBGNAMESPACE_H_

#include <string>
#include <unordered_map>
#include "DbgUtil.h"

namespace pcv {
namespace dbghelp {

struct DbgNamespace {
  std::string name;
  DbgNamespace* parent;
  std::unordered_map<std::string, DbgNamespace*> children;

  std::string getFullName() const
  {
    std::string res = name;

    DbgNamespace* parent = this->parent;
    while (parent != nullptr) {
      if (!parent->name.empty()) {
        res = parent->name + "::" + res;
      }
      parent = parent->parent;
    }
    return res;
  }

  DbgNamespace* getChildOrNullptr(const std::string& name) const
  {
    return getValueOrNullptr(children, name);
  }

  bool hasChildNamed(const std::string& name) const { return getChildOrNullptr(name) != nullptr; }

  DbgNamespace() : parent(nullptr)
  {
    // empty
  }

  explicit DbgNamespace(std::string const& name) : name(name), parent(nullptr)
  {
    // empty
  }

  ~DbgNamespace()
  {
    for (auto& pair : children) {
      delete pair.second;
    }
  }
};

}  // namespace dbghelp
}  // namespace pcv

#endif  // DC_CPP_DBGHELP_INCLUDE_DBGNAMESPACE_H_
