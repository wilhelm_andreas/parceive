#pragma once

#include "Context.h"
#include "DbgHelpReader.h"

namespace pcv {
class Filter;

namespace dbghelp {

class ParceiveDbgHelpReader : public DbgHelpReader
{
 public:
  explicit ParceiveDbgHelpReader(Filter* filter);

  bool getSourceLocation(const S_ADDRUINT& ip, int32_t* line, std::string* fileName) const override;
  bool readSymInfo(const std::string& modulePath,
                   const Id_t imageId,
                   const SymInfo::offset_t offset) override;
  const Context& getContext() const override;

  void setParceivePath(const std::string& path) { parceivePath = path; }
  void setSkipLocalBlocks(bool value) { skipLocalBlocks = value; }

  static bool runSymbolCollectionProcess(const std::string& modulePath,
                                         Id_t imageId,
                                         const std::string& symbolsPath,
                                         const std::string& symbolsDb,
                                         bool skipLocalBlocks);

 private:
  Filter* filter;
  std::string mainModulePath;
  std::string parceivePath;
  Context outOfProcessContext;
  bool skipLocalBlocks{};
  static bool isFirstSymbolCollection;
};

}  // namespace dbghelp
}  // namespace pcv