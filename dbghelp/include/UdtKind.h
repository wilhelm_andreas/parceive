#pragma once

#ifndef DC_CPP_DBGHELP_INCLUDE_UDTKIND_H_
#define DC_CPP_DBGHELP_INCLUDE_UDTKIND_H_

namespace pcv {
namespace dbghelp {

enum UdtKind {
  UdtStruct,
  UdtClass,
  UdtUnion,
  UdtInterface,

  UdtNone
};

}  // namespace dbghelp
}  // namespace pcv

#endif  //  DC_CPP_DBGHELP_INCLUDE_UDTKIND_H_
