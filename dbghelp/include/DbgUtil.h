#pragma once

#ifndef DC_CPP_DBGHELP_INCLUDE_DBGUTIL_H_
#define DC_CPP_DBGHELP_INCLUDE_DBGUTIL_H_

#include <algorithm>
#include <string>
#include <unordered_map>
#include <vector>

namespace pcv {
namespace dbghelp {

template <typename T>
static void deleteObject(T* object)
{
  delete object;
  object = nullptr;
}

template <typename T>
static void deleteContainerEntries(T& container)
{
  for (auto& entry : container) {
    deleteObject(entry);
  }
  container.clear();
}

template <typename KEY, typename VALUE>
static VALUE getValueOrNullptr(const std::unordered_map<KEY, VALUE>& map, const KEY& key)
{
  auto it = map.find(key);
  return it == map.end() ? nullptr : it->second;
}

template <typename KEY, typename VALUE>
static bool containsKey(const std::unordered_map<KEY, VALUE>& map, const KEY& key)
{
  return map.find(key) != map.end();
}

template <typename TVAL, typename UVAL>
void appendVector(std::vector<TVAL>& dst, const std::vector<UVAL>& src)
{
  dst.reserve(dst.size() + src.size());
  for (auto& value : src) {
    dst.emplace_back(value);
  }
}

template <typename TVAL, typename UVAL>
void copyVector(std::vector<TVAL>& dst, const std::vector<UVAL>& src)
{
  dst.clear();
  dst.reserve(src.size());
  appendVector(dst, src);
}

template <typename TVAL, typename KVAL, typename VVAL>
void fillVector(std::vector<TVAL>& dst, const std::unordered_map<KVAL, VVAL>& src)
{
  dst.clear();
  dst.reserve(src.size());
  for (auto& kvp : src) {
    dst.emplace_back(kvp.second);
  }
}

static int findBracketEnd(const std::string& symbolName, const int bracketStartIdx)
{
  int bracketEndIx     = bracketStartIdx + 1;
  int openBrackets     = 1;
  const size_t strSize = symbolName.size();

  while (bracketEndIx < strSize) {
    if (symbolName[bracketEndIx] == '<') {
      ++openBrackets;
    } else if (symbolName[bracketEndIx] == '>') {
      --openBrackets;
    }
    if (openBrackets == 0) {
      break;
    }
    ++bracketEndIx;
  }

  return (openBrackets == 0) ? bracketEndIx : -1;
}

static void addSubstring(std::vector<std::string>& vec,
                         const std::string& str,
                         int firstIdx,
                         int lastIdx)
{
  if (str[lastIdx] == '`' || str[lastIdx] == '�') {
    --lastIdx;
  }
  if (str[firstIdx] == '`' || str[firstIdx] == '�') {
    ++firstIdx;
  }
  auto subString = str.substr(firstIdx, 1 + lastIdx - firstIdx);
  if (!subString.empty()) {
    vec.emplace_back(subString);
  }
}

static std::vector<std::string> splitName(std::string const& name)
{
  const int strSize = static_cast<int>(name.size());
  int idx = 0, startIdx = 0;
  std::vector<std::string> res;

  while (idx < strSize) {
    char c = name[idx];
    // guard against possibly templated operator<< and operator>>
    // e.g.: `std::operator<<<std::char_traits<char> >'::`1'::catch$0
    if (c == 'o' && strSize >= idx + 10) {
      const auto substr = name.substr(idx, 10);
      if (substr == "operator<<" || substr == "operator>>") {
        idx += 10;
        continue;
      }
    }
    if (c == '<') {
      const int endIdx = findBracketEnd(name, idx);
      if (endIdx == -1) {
        break;
      }
      if (endIdx > startIdx) {
        addSubstring(res, name, startIdx, endIdx);
      }
      idx      = endIdx + 1;
      startIdx = idx;
    } else if (c == ':') {
      if (idx >= 1 && idx - 1 >= startIdx) {
        addSubstring(res, name, startIdx, idx - 1);
      }
      idx++;
      startIdx = idx;
    } else {
      ++idx;
    }
  }
  if (strSize - 1 >= startIdx) {
    addSubstring(res, name, startIdx, strSize - 1);
  }

  return res;
}

static std::string getUnqualifiedName(std::string const& fullName)
{
  return splitName(fullName).back();
}
}  // namespace dbghelp
}  // namespace pcv

#endif  // DC_CPP_DBGHELP_INCLUDE_DBGUTIL_H_
