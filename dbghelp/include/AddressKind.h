#pragma once

#ifndef DC_CPP_DBGHELP_INCLUDE_ADDRESSKIND_H_
#define DC_CPP_DBGHELP_INCLUDE_ADDRESSKIND_H_

namespace pcv {
namespace dbghelp {

enum AddressKind {
  AddressIsInvalid,

  AddressIsAbsolute,
  AddressIsStructOffset,
  AddressIsFrameRelative,
  AddressIsRegisterRelative
};

}  // namespace dbghelp
}  // namespace pcv

#endif  // DC_CPP_DBGHELP_INCLUDE_ADDRESSKIND_H_
