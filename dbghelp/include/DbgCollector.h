#pragma once

#include <cstdint>
#include <map>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "DbgFunction.h"
#include "DbgNamespace.h"
#include "DbgType.h"
#include "DbgVariable.h"
#include "FileFilter.h"

namespace pcv {
class Filter;

namespace dbghelp {

struct CodeLocation {
  std::string fileName;
  uint32_t line;
};
typedef std::map<uint64_t, CodeLocation> AddressToCodeLocationMap;

struct SymbolLoadDirectives {
  // if true the collector will try to fetch locals in all blocks
  // of a function. This extra step may be quite time consuming.
  bool deepScan                        = true;
  uint64_t linkToLoadtimeAddressOffset = 0;
};

struct DbgCollector {
  DbgCollector() : processHandle(nullptr), functionContext(nullptr), filter_(nullptr) {}

  DbgCollector(const FileFilter* filter)
    : processHandle(nullptr), functionContext(nullptr), filter_(filter)
  {}

  ~DbgCollector();

  // members to be reset before every call of loadSymbols():
  std::vector<DbgType*> types;
  std::vector<DbgVariable*> globalVariables;
  std::vector<DbgFunction*> functions;
  std::unordered_map<size_t, DbgType*> map_index_to_type;
  std::unordered_map<std::string, DbgType*> map_name_to_type;
  std::unordered_map<std::string, DbgFunction*> map_name_to_function;
  std::unordered_map<std::string, DbgVariable*> map_name_to_variable;
  std::unordered_set<std::string> symbols;
  void* processHandle;
  DbgFunction* functionContext;

  // members retaining their state throughout the object's life time:
  DbgNamespace namespaceRoot;
  AddressToCodeLocationMap map_address_to_context;

  bool loadSymbols(const std::string& fileName, const SymbolLoadDirectives& directives);

  bool hasLoadedSymbols() const;
  std::string getLoadedModuleFileName() const;

  bool getCodeLocation(uint64_t address, std::string& file, uint32_t& line) const;
  DbgType* getType(const size_t& index) const;

  bool isAddressIncluded(const uint64_t address) const;
  bool isSourceLocationIncluded(const std::string& fileName, uint32_t line) const;

  static bool canLoadSymbols(const std::string& fileName);

  const SymbolLoadDirectives& getLoadDirectives() const { return directives_; }

 private:
  void resetCollectorState();
  void collectNamespaces();
  void collectLocals();

  const FileFilter* filter_;
  // the path of the last successfully loaded  module
  std::string loadedModuleFileName_;
  SymbolLoadDirectives directives_;
};

}  // namespace dbghelp
}  // namespace pcv
