Windows DbgHelp Reader
======================
This component is responsible for parsing COFF based debug information of Windows executables and
shared libraries.

The main class is `DbgHelpReader`, which inherits from the abstract class `SymInfo` used by Parceive
for symbol resolving.

