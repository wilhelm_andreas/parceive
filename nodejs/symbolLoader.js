var symbols = require("./build/Debug/symbols");

var filename = process.argv[2];

var ast = 
[
   {
      "type": "FilterStatement",
      "left": "partial",
      "right": {
         "type": "AtomExpression",
         "rule": "namespace",
         "regex": "std"
      }
   },
   {
      "type": "FilterStatement",
      "left": "partial",
      "right": {
         "type": "AtomExpression",
         "rule": "namespace",
         "regex": "__gnu_cxx"
      }
   }
];


var symbolLoader  = new symbols.loader(filename, ast);

console.log(symbolLoader.start(JSON.stringify(ast)));
