#! /bin/bash

BASEDIR=$(dirname "$0")
cd $BASEDIR

rm -rf cmake_build*
rm -rf build
rm -rf 3rdparty

mkdir -p cmake_build
cd cmake_build

cmake ..
cmake --build . --target NodeModule
