//
// Created by wilhelma on 1/10/17.
//

#include "RegexFileRule.h"
#include "entities/SoftwareEntity.h"

namespace pcv {

RegexFileRule::RegexFileRule(const std::string &artifactName,
                             EntityType type,
                             const std::string &regexString)
  : RegexRule(artifactName, type, regexString)
{}

const std::string &RegexFileRule::getMatchString(const SoftwareEntity &entity) const
{
  if (!entity.file || entity.file->name.find("<built-in>") != std::string::npos) {
    return EMPTYSTRING;
  }

  return entity.file->name;
}

RegexFileRule::RegexFileRule(const RegexFileRule &rule) : RegexRule(new Artifact_t(*rule.artifact_))
{}

}  // namespace pcv
