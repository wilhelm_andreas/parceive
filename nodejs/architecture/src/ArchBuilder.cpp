//
// Created by wilhelma on 1/10/17.
//

#include "../include/ArchBuilder.h"

#include <algorithm>
#include <unordered_map>
#include "entities/Class.h"
#include "entities/Namespace.h"

#include "../../ast/visitor/VisitorContext.h"
#include "../include/ArchRule.h"

namespace {

using pcv::Artifact_t;

void cloneArtifacts(const std::vector<std::unique_ptr<Artifact_t>> &original,
                    std::vector<std::unique_ptr<Artifact_t>> &copy,
                    Artifact_t *parent)
{
  for (const auto &child : original) {
    copy.push_back(std::unique_ptr<Artifact_t>(new Artifact_t(*child.get())));
    copy.back()->parent = parent;
  }
};

}  // namespace

namespace pcv {

using entity::Class;
using entity::EntityType;

void removeDuplicates(Artifact_t &as, Artifact_t &newArtifact, Artifact_t *root = nullptr)
{
  static std::unordered_map<const SoftwareEntity *, Artifact_t *> addedEntities;

  if (!root) root = &newArtifact;

  auto getCommonAncestor = [&newArtifact, &root](Artifact_t *ls, Artifact_t *rs) {
    auto getArchVector = [](Artifact_t *cs) {
      std::vector<Artifact_t *> res{cs};
      while (res.front()->parent && res.front()->parent->parent)
        res.insert(begin(res), res.front()->parent);
      return res;
    };

    auto buildArtifactName = [](const Artifact_t &lhs, const Artifact_t &rhs) {
      return lhs.name + DELIM + rhs.name;
    };

    auto getArtifact = [](const std::string &name,
                          const std::vector<std::unique_ptr<Artifact_t>> &v) {
      Artifact_t *s{nullptr};
      auto it = std::find_if(begin(v), end(v), [&name](const std::unique_ptr<Artifact_t> &s) {
        return s->name == name;
      });
      if (it != end(v)) s = it->get();

      return s;
    };

    auto appendArtifact = [](const std::string &name, Artifact_t *parent) {
      parent->children.emplace_back(std::unique_ptr<Artifact_t>{new Artifact_t(name, parent)});
      return parent->children.back().get();
    };

    /* begin of getCommonAncestor() */
    auto lhs = getArchVector(ls);
    auto rhs = getArchVector(rs);

    // prepare base artifact
    auto tSet = lhs[0];
    if (tSet != rhs[0]) {
      std::string setName = buildArtifactName(*rhs[0], *tSet);
      tSet                = getArtifact(setName, root->children);
      if (!tSet) tSet = appendArtifact(setName, root);
    }

    // prepare sub-artifacts
    for (uint32_t i = 1; i < std::min(lhs.size(), rhs.size()); ++i) {
      if (lhs[i]->name != rhs[i]->name) break;

      auto tmp = getArtifact(lhs[i]->name, tSet->children);
      if (!tmp) tmp = appendArtifact(lhs[i]->name, tSet);
      tSet = tmp;
    }

    return tSet;
  };

  auto s = addedEntities.find(as.entity);
  if (s == end(addedEntities)) {
    addedEntities[as.entity] = &as;
  } else {
    auto commonSet                   = getCommonAncestor(&as, s->second);
    commonSet->entity                = as.entity;
    addedEntities[as.entity]->entity = nullptr;
    addedEntities[as.entity]         = commonSet;
    as.entity                        = nullptr;
  }

  for (auto &child : as.children) removeDuplicates(*child.get(), newArtifact, root);
}

void removeDuplicatedsAndAppend(Artifact_t &as)
{
  Artifact_t newArtifact("newArtifact", nullptr);
  removeDuplicates(as, newArtifact);

  // relink parent of the new root artifacts
  std::for_each(begin(newArtifact.children),
                end(newArtifact.children),
                [&as](const std::unique_ptr<Artifact_t> &ptr) { ptr->parent = &as; });

  // move new root artifacts to root
  as.children.insert(end(as.children),
                     std::make_move_iterator(begin(newArtifact.children)),
                     std::make_move_iterator(end(newArtifact.children)));
}

void removeEmptyArtifacts(Artifact_t &as)
{
  for (auto i = begin(as.children); i != end(as.children);) {
    removeEmptyArtifacts(*i->get());
    if ((*i)->children.empty())
      i = as.children.erase(i);
    else
      ++i;
  }
}

void ArchBuilder::apply(ArchRule *rule, Artifact_t *artifact) noexcept
{
  rule->apply(*artifact, ctxt_);
}

void ArchBuilder::finish()
{
  // removeDuplicatedsAndAppend(artifact_);
  // removeEmptyArtifacts(artifact_);
}

Artifact_t::Artifact_t(const Artifact_t &artifact) : name(artifact.name), entity(artifact.entity)
{
  cloneArtifacts(artifact.children, children, this);
}

}  // namespace pcv
