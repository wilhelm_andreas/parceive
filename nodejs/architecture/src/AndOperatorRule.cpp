//
// Created by Faris Cakaric on 19.06.17.
//

#include <functional>
#include <set>

#include <NamespaceRule.h>
#include "AndOperatorRule.h"
#include "ClassRule.h"
#include "FunctionRule.h"
#include "VariableRule.h"

using pcv::entity::EntityType;
using pcv::entity::SoftwareEntity;

namespace {

template <typename T, pcv::entity::EntityType TYPE>
std::set<const T *> getEntitiesOfType(const pcv::Artifact_t &artifact)
{
  std::set<const T *> entities;

  if (artifact.entity != nullptr && artifact.entity->getEntityType() == TYPE) {
    entities.insert(static_cast<const T *>(artifact.entity));
  }
  for (auto &child : artifact.children) {
    auto tmpEntities = getEntitiesOfType<T, TYPE>(*child);
    entities.insert(std::begin(tmpEntities), std::end(tmpEntities));
  }

  return entities;
}

template <typename T, pcv::entity::EntityType TYPE>
void removeEntitiesOfTypeIf(pcv::Artifact_t *artifact, std::function<bool(const T *)> f)
{
  artifact->children.erase(std::remove_if(std::begin(artifact->children),
                                          std::end(artifact->children),
                                          [&f](const std::unique_ptr<pcv::Artifact_t> &child) {
                                            return (child->entity != nullptr &&
                                                    child->entity->getEntityType() == TYPE &&
                                                    f(static_cast<const T *>(child->entity)));
                                          }),
                           std::end(artifact->children));

  for (auto &child : artifact->children) { removeEntitiesOfTypeIf<T, TYPE>(child.get(), f); }
}

template <typename T, pcv::entity::EntityType TYPE>
void removeDifferentEntitiesOfType(pcv::Artifact_t *one, const pcv::Artifact_t &two)
{
  std::set<const T *> entitiesOne = getEntitiesOfType<T, TYPE>(*one),
                      entitiesTwo = getEntitiesOfType<T, TYPE>(two), intersection;

  std::set_intersection(std::begin(entitiesOne),
                        std::end(entitiesOne),
                        std::begin(entitiesTwo),
                        std::end(entitiesTwo),
                        std::inserter(intersection, std::begin(intersection)));

  removeEntitiesOfTypeIf<T, TYPE>(one, [&intersection](const T *e) {
    return (intersection.empty() || intersection.find(e) == std::end(intersection));
  });
};

bool isMandatory(const pcv::Artifact_t &artifact)
{
  using pcv::entity::EntityType;

  return (artifact.entity && (artifact.entity->getEntityType() == EntityType::Variable ||
                              artifact.entity->getEntityType() == EntityType::Routine));
}

void cloneTo(const pcv::Artifact_t &source, pcv::Artifact_t *target)
{
  for (auto &child : source.children) {
    target->children.emplace_back(
      std::unique_ptr<pcv::Artifact_t>{new pcv::Artifact_t(child->name, target)});
    target->children.back()->entity = child->entity;
    cloneTo(*child, target->children.back().get());
  }
}

void merge(const pcv::Artifact_t &one, const pcv::Artifact_t &two, pcv::Artifact_t *parent)
{
  cloneTo(one, parent);
  cloneTo(two, parent);
}

void removeEmptyArtifacts(pcv::Artifact_t *artifact)
{
  for (auto &child : artifact->children) { removeEmptyArtifacts(child.get()); }

  artifact->children.erase(
    std::remove_if(std::begin(artifact->children),
                   std::end(artifact->children),
                   [](const std::unique_ptr<pcv::Artifact_t> &child) {
                     return (!isMandatory(*child) && child->children.empty());
                   }),
    std::end(artifact->children));
}

void alignTopArtifacts(pcv::Artifact_t *top,
                       const std::vector<std::unique_ptr<pcv::Artifact_t>> &children,
                       const pcv::Artifact_t &artifactTwo)
{
  auto topChild = children.begin()->get();
  if (std::find_if(artifactTwo.children.begin(),
                   artifactTwo.children.end(),
                   [&topChild](const std::unique_ptr<pcv::Artifact_t> &ptr) {
                     return (topChild->entity == ptr->entity);
                   }) == artifactTwo.children.end()) {
    alignTopArtifacts(top, topChild->children, artifactTwo);
  } else {
    for (auto &i : children) {
      top->children.push_back(std::unique_ptr<pcv::Artifact_t>{new pcv::Artifact_t{*i.get()}});
    }
    top->children.erase(top->children.begin());
  }
}

}  // namespace

namespace pcv {
AndOperatorRule::AndOperatorRule(const std::string &artifactName,
                                 ArchRule *firstArtifact,
                                 ArchRule *secondArtifact)
  : artifactName_(artifactName), firstArtifact_(firstArtifact), secondArtifact_(secondArtifact)
{}

template <typename InIt1, typename InIt2, typename OutIt>
OutIt unordered_set_intersection(InIt1 b1, InIt1 e1, InIt2 b2, InIt2 e2, OutIt out)
{
  while (!(b1 == e1)) {
    if (!(std::find(b2, e2, *b1) == e2)) {
      *out = *b1;
      ++out;
    }
    ++b1;
  }

  return out;
}

std::unique_ptr<ArchRule::artifacts_t> AndOperatorRule::execute(Artifact_t &artifact,
                                                                const Context &ctxt)
{
  auto artifacts = std::unique_ptr<artifacts_t>{new artifacts_t};

  artifact_         = std::unique_ptr<Artifact_t>(new Artifact_t(artifactName_, &artifact));
  artifact_->entity = nullptr;

  Artifact_t *artifactOne = firstArtifact_->getArtifact();
  Artifact_t *artifactTwo = secondArtifact_->getArtifact();

  removeDifferentEntitiesOfType<Variable, entity::EntityType::Variable>(artifactOne, *artifactTwo);
  removeDifferentEntitiesOfType<Routine, entity::EntityType::Routine>(artifactOne, *artifactTwo);

  removeEmptyArtifacts(artifactOne);
  alignTopArtifacts(artifactOne, artifactOne->children, *artifactTwo);
  cloneTo(*artifactOne, artifact_.get());

  return artifacts;
}

std::unique_ptr<ArchRule::artifacts_t> AndOperatorRule::append(Artifact_t &artifact,
                                                               const Context &ctxt)
{
  return nullptr;
}

AndOperatorRule::AndOperatorRule(const AndOperatorRule &rule)
  : artifactName_(rule.artifactName_),
    firstArtifact_(rule.firstArtifact_->clone()),
    secondArtifact_(rule.secondArtifact_->clone())
{}

}  // namespace pcv