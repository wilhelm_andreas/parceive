//
// Created by Faris Cakaric on 19.07.17.
//

#include "NotOperatorRule.h"
#include <ClassRule.h>
#include <Context.h>
#include <FunctionRule.h>
#include <ImageRule.h>
#include <NamespaceRule.h>
#include <VariableRule.h>

namespace pcv {

NotOperatorRule::NotOperatorRule(const std::string &artifactName_, Artifact_t *operand_)
  : artifactName_(artifactName_), operand_(operand_)
{}

void removeArtifacts(Artifact_t *artifact,
                     const ArchRule::added_t &added)
{
  artifact->children.erase(std::remove_if(artifact->children.begin(), artifact->children.end(),
      [&added](const std::unique_ptr<Artifact_t>& ptr) {
    return (added.find(ptr->entity) != added.end());
  }), artifact->children.end());

  for (auto &child : artifact->children) { removeArtifacts(child.get(), added); }
}

std::unique_ptr<ArchRule::artifacts_t> NotOperatorRule::execute(Artifact_t &artifact,
                                                                const Context &ctxt)
{
  ArchRule::added_t added;

  // find namespaces, classes, functions and variables mapped in the operand
  std::vector<const Namespace *> namespacesInOperand;
  getNamespacesInArtifact(*operand_, namespacesInOperand);

  std::unordered_set<const Class *> classesInOperand;
  getClassesInArtifact(*operand_, classesInOperand);

  std::unordered_set<const Routine *> routinesInOperand;
  getRoutinesInArtifact(*operand_, routinesInOperand, added);

  std::unordered_set<const Variable *> variablesInOperand;
  getGlobalVariablesInArtifact(*operand_, variablesInOperand, added);

  std::copy(
    namespacesInOperand.begin(), namespacesInOperand.end(), std::inserter(added, added.end()));
  std::copy(classesInOperand.begin(), classesInOperand.end(), std::inserter(added, added.end()));
  std::copy(routinesInOperand.begin(), routinesInOperand.end(), std::inserter(added, added.end()));
  std::copy(
    variablesInOperand.begin(), variablesInOperand.end(), std::inserter(added, added.end()));

  // find namespaces, classes, functions and variables which are not in the operand
  Artifact_t tmp{"tmp", nullptr};
  ImageRule iRule{"iArtifact", ".*"};
  iRule.execute(tmp, ctxt);
  artifact_ = std::unique_ptr<Artifact_t>(new Artifact_t {*iRule.getArtifact()});

  removeArtifacts(artifact_.get(), added);

  return nullptr;
}

std::unique_ptr<ArchRule::artifacts_t> NotOperatorRule::append(Artifact_t &artifact,
                                                               const Context &ctxt)
{
  return nullptr;
}

NotOperatorRule::NotOperatorRule(const NotOperatorRule &rule)
  : artifactName_(rule.artifactName_), operand_(new Artifact_t(*rule.operand_))
{}
}  // namespace pcv