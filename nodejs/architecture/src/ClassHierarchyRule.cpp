//
// Created by Faris Cakaric on 08.10.17.
//

#include "ClassHierarchyRule.h"
#include <regex>
#include "Context.h"
#include "entities/Class.h"

namespace pcv {
ClassHierarchyRule::ClassHierarchyRule(const std::string &artifactName,
                                       const std::string &regex,
                                       const std::string &fileRegex)
  : ClassRule(artifactName, regex, fileRegex)
{}

std::unique_ptr<ArchRule::artifacts_t> ClassHierarchyRule::execute(Artifact_t &artifact,
                                                                   const Context &ctxt)
{
  artifact_         = std::unique_ptr<Artifact_t>(new Artifact_t(artifactName_, &artifact));
  artifact_->entity = nullptr;
  auto newArtifact  = artifact_.get();
  std::unordered_set<const Class *> classes;

  // consider only matching classes
  for (auto &c : ctxt.classes) {
    if (std::regex_match(c->name, rx_) && std::regex_match(c->file->name, fileRx_)) {
      classes.insert(c.get());
    }
  }

  apply(newArtifact, classes, false);
  return nullptr;
}

std::unique_ptr<ArchRule::artifacts_t> ClassHierarchyRule::append(Artifact_t &artifact,
                                                                  const Context &ctxt)
{
  return nullptr;
}
}  // namespace pcv
