//
// Created by wilhelma on 12/19/17.
//

#pragma once

#include <exception>
#include <stdexcept>

struct ArchError : public std::runtime_error {
  explicit ArchError(const std::string& arg) : std::runtime_error(arg) {}
};
