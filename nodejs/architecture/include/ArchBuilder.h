//
// Created by wilhelma on 1/10/17.
//

#ifndef DWARFLOADER_ARCHBUILDER_H
#define DWARFLOADER_ARCHBUILDER_H

#include <memory>
#include <unordered_set>
#include <vector>

#include <iostream>

#include "Context.h"
#include "entities/SoftwareEntity.h"

#ifdef _MSC_VER
#if _MSC_VER <= 1800  // VS 2013
#ifndef noexcept
#define noexcept throw()
#endif

#ifndef snprintf
#define snprintf _snprintf_s
#endif
#endif
#endif

class ArchBuilderFixture;

namespace pcv {

using pcv::Context;
using pcv::entity::SoftwareEntity;

const char DELIM{'/'};

class ArchRule;

/// @brief The artifact structure that builds the architecture.
typedef struct Artifact_t {
  std::string name;
  const SoftwareEntity *entity;
  std::vector<std::unique_ptr<Artifact_t>> children;
  Artifact_t *parent;

  /// @brief Constructor.
  /// @param name The name of the artifact.
  /// @param parent The parent artifact (nullptr if it is a root artifact).
  explicit Artifact_t(std::string name, Artifact_t *parent)
    : name(std::move(name)), entity(nullptr), parent(parent)
  {}
  Artifact_t(const Artifact_t &);
  ~Artifact_t() = default;
} Artifact_t;

/// @brief The architecture builder class. It is responsible to apply architecture rules
/// to build a hierarchy of artifacts (an architecture) based on the debug information.
class ArchBuilder
{
 public:
  /// @brief Constructor.
  /// @param [IN] ctxt A filled context with debug information.
  explicit ArchBuilder(const Context &ctxt) : ctxt_(ctxt) {}

  /// @brief Destructor.
  ~ArchBuilder() = default;

  /// @brief Applies an architecture rule to create new artifacts on the root level.
  /// @param [IN] rule The architecture rule to apply.
  void apply(ArchRule *rule, Artifact_t *artifact) noexcept;

  /// @brief Resolves duplicated software entities in the software artifacts by creating
  /// new artifacts on the root level.
  void finish();

 private:
  const Context &ctxt_;

  friend ArchBuilderFixture;
};

}  // namespace pcv

#endif  // DWARFLOADER_ARCHBUILDER_H
