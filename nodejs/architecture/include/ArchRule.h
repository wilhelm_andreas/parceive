//
// Created by wilhelma on 1/10/17.
//

#ifndef DWARFLOADER_COMMAND_H
#define DWARFLOADER_COMMAND_H

#include <memory>
#include <vector>
#include "ArchBuilder.h"

namespace pcv {

struct Artifact_t;

/// @brief The base class for a rule to build an architecture.
class ArchRule
{
 protected:
  using artifacts_t = std::vector<Artifact_t *>;
  std::unique_ptr<artifacts_t> artifacts;
  std::unique_ptr<Artifact_t> artifact_;

  virtual std::unique_ptr<artifacts_t> execute(Artifact_t &artifact, const Context &ctxt) = 0;
  virtual std::unique_ptr<artifacts_t> append(Artifact_t &artifact, const Context &ctxt)  = 0;

 public:
  using added_t = std::unordered_set<const SoftwareEntity *>;

  ArchRule() = default;
  explicit ArchRule(Artifact_t *artifact);

  virtual ArchRule *clone() const = 0;

  virtual ~ArchRule() = default;

  void setArtifactName(std::string artifactName) { artifact_->name = artifactName; }

  void apply(Artifact_t &artifact, const Context &ctxt)
  {
    artifacts = execute(artifact, ctxt);
    apply(artifacts, ctxt);
  }

  Artifact_t *getArtifact() { return artifact_.get(); }

  void apply(std::unique_ptr<artifacts_t> &artifacts, const Context &ctxt)
  {
    for (auto follower : followers_) {
      auto newArtifacts = std::unique_ptr<artifacts_t>(new artifacts_t);
      for (auto &artifact : *artifacts) {
        auto changedArtifacts = follower->append(*artifact, ctxt);
        for (auto ca : *changedArtifacts) {
          newArtifacts->emplace_back(ca);
        }
      }
      follower->apply(newArtifacts, ctxt);
    }
  }

  inline ArchRule *append(ArchRule *as)
  {
    followers_.emplace_back(as);
    return as;
  }

  template <typename T>
  friend ArchRule &operator<<(ArchRule &lhs, T &rhs);

 private:
  std::vector<ArchRule *> followers_;
};

template <typename T>
ArchRule &operator<<(ArchRule &lhs, T &rhs)
{
  lhs.followers_.emplace_back(reinterpret_cast<ArchRule *>(&rhs));
  return lhs;
}

template <class Derived, class Base = ArchRule>
class Cloneable : public Base
{
 public:
  virtual ArchRule *clone() const
  {
    auto clone       = new Derived(static_cast<const Derived &>(*this));
    clone->artifact_ = std::unique_ptr<Artifact_t>(new Artifact_t(*this->artifact_));
    return clone;
  }
};

}  // namespace pcv

#endif  // DWARFLOADER_COMMAND_H
