#pragma once

#include <regex>
#include <string>
#include <unordered_map>
#include "ArchRule.h"

namespace pcv {

using entity::Image;

class ImageRule : public Cloneable<ImageRule>
{
  const std::string artifactName_;
  const std::regex rx_;

 public:
  ImageRule() {}

  ImageRule(const ImageRule &);

  explicit ImageRule(const std::string &artifactName, const std::string &regexString);

  std::unique_ptr<artifacts_t> execute(Artifact_t &artifact, const Context &ctxt) override;

  std::unique_ptr<artifacts_t> append(Artifact_t &artifact, const Context &ctxt) override;

  /**
   * @brief Applies the image rule to build image hierarchies on the
   * given images and append them on the given artifact.
   * @returns the set of added software entities
   */
  std::unordered_map<const Image *, Artifact_t *> apply(
    Artifact_t *artifact,                     /** [in,out] The base artifact. */
    const std::vector<const Image *> &images, /** [in] The images to consider. */
    added_t *added = nullptr /**[out] The added software entities, if abstraction is wished. */);
};

}  // namespace pcv
