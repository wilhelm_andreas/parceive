#include <sstream>
#include <string>

#include "ArchException.h"
#include "SymbolLoader.h"
#include "ast/ast-from-json/CreateAstFromJson.h"
#include "ast/visitor/ASTVisitor.h"

#ifdef _MSC_VER
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#elif defined(__GNUC__)
#else
#endif

using std::cerr;
using std::endl;
using std::runtime_error;
using std::string;
using std::stringstream;

namespace pcv {

#ifdef _MSC_VER

SymbolLoader::SymbolLoader(const std::string& fileName, const std::string& filterPattern)
  : fileName_(fileName)
{
  filter_ = new FileFilter("(.+)" + filterPattern + "(.+)", "(.+)boost(.+)");
  reader_ = new DbgHelpReader(fileName, filter_);
}

unsigned SymbolLoader::getBinaryBits() const
{
  DWORD binaryType;
  if (GetBinaryType(fileName_.c_str(), &binaryType)) {
    switch (binaryType) {
      case SCS_32BIT_BINARY:
        return 32;
      case SCS_64BIT_BINARY:
        return 64;
      default:
        return 16;
    }
  }
  return 0;
}

bool SymbolLoader::isBinaryOptimized() const
{
  return reader_->isBinaryOptimized(fileName_.c_str());
}

#else  // #ifdef _MSC_VER

SymbolLoader::SymbolLoader(const std::string& fileName, const std::string& filterPattern)
  : fileName_(fileName), ast_("")
{
  filter_ = new FileFilter("(.+)" + filterPattern + "(.+)", "(.+)boost(.+)");
  reader_ = new DwarfReader(fileName, duplicate_, *filter_);
}

#endif  // #ifdef _MSC_VER

SymbolLoader::~SymbolLoader()
{
  delete filter_;
  delete reader_;
}

string SymbolLoader::start(const string& ast) const
{
  return process(ast, reader_);
}

string SymbolLoader::start(const std::string& filename, const string& ast) const
{
  DieDuplicate duplicate;
  std::unique_ptr<DwarfReader> reader = std::unique_ptr<DwarfReader>{
      new DwarfReader(filename, duplicate, *filter_)
  };
  return process(ast, reader.get());
}

std::string SymbolLoader::process(const std::string &ast, DwarfReader *reader) const
{
  stringstream buffer;
  string error;
  const auto json = Json::parse(ast, error);

  if (json.is_null()) {
    cerr << "Parsing the given AST failed with the following error message: " << error << endl;
  } else {
    if (reader->getContext().images.size() == 0) {
      try {
        reader->start(json);
      } catch (runtime_error& e) {
        cerr << "Loading symbols failed with the following error message: " << e.what() << endl;
      }
    }

    VisitorContext visitorContext(reader_->getContext(), buffer);
    ASTVisitor astVisitor(&visitorContext);

    Program program = CreateAstFromJson::generateAst(json);
    program.accept(astVisitor);
  }

  return buffer.str();
}

}  // namespace pcv
