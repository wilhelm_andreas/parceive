#pragma once

#include <string>

#include "ArchBuilder.h"
#include "ClassRule.h"
#include "FileFilter.h"
#include "FunctionRule.h"
#include "NamespaceRule.h"

#ifdef _MSC_VER
#include "DbgHelpReader.h"

using pcv::dbghelp::DbgHelpReader;
#elif defined(__GNUC__)
#include "Duplicate.h"
#include "DwarfReader.h"

using pcv::dwarf::DieDuplicate;
using pcv::dwarf::DwarfReader;
#else
#error "Unsupported compiler"
#endif

namespace pcv {

using pcv::ArchBuilder;
using pcv::ArchRule;
using pcv::ClassRule;
using pcv::FileFilter;
using pcv::FunctionRule;
using pcv::NamespaceRule;

class SymbolLoader
{
 public:
  explicit SymbolLoader(const std::string& fileName, const std::string& filterPattern);
  ~SymbolLoader();
  DieDuplicate duplicate_;

  std::string start(const std::string& ast) const;
  std::string start(const std::string& filename, const std::string& ast) const;

#ifdef _MSC_VER
  // only exposed on windows. Linux can use the output of 'file' for that
  unsigned getBinaryBits() const;
  bool isBinaryOptimized() const;
#endif

 private:
#ifdef _MSC_VER
  DbgHelpReader* reader_;
#else
  DwarfReader* reader_;
#endif

  FileFilter* filter_;
  std::string fileName_;

 private:
  const std::string ast_;

  std::string process(const std::string& ast, DwarfReader* reader) const;
};
}  // namespace pcv
