//
// Created by Faris Cakaric on 26.06.17.
//

#include "VisitorContext.h"

#include <json11.hpp>

#ifdef __linux__
#include <Duplicate.h>
#include "Type.h"
#endif

using json11::Json;

namespace {

using pcv::Artifact_t;
using pcv::DELIM;
using pcv::entity::Class;
using pcv::entity::EntityType;

static uint32_t artifactId{0};

void addEntity(Json::array *entities, Json::array *childObjects, const Artifact_t *child)
{
  Json::array baseClassesArray{};
  std::string name{}, nmsp{}, file{};
  pcv::Id_t imgId{};

  switch (child->entity->getEntityType()) {
    case EntityType::Routine: {
      name = child->entity->name;
      file  = child->entity->file->name;
      imgId = child->entity->img->id;
      nmsp  = child->entity->nmsp->name;
      break;
    }
    case EntityType::Image: {
      imgId = child->entity->id;
      auto startFilename =
        std::min(child->entity->name.find_last_of('/'), child->entity->name.find_last_of('\\'));
      name = child->entity->name.substr(startFilename + 1);
      file = child->entity->name;
      break;
    }
    case EntityType::Class: {
      if (child->entity->getEntityType() == EntityType::Class) {
        std::vector<Class *> baseClasses = (static_cast<const Class *>(child->entity))->baseClasses;
        for (auto &baseClass : baseClasses) {
          baseClassesArray.push_back(Json::object{{"id", static_cast<int>(baseClass->id)}});
        }
      }
      name  = child->entity->name;
      file  = (child->entity->file) ? child->entity->file->name : "";
      imgId = child->entity->img->id;
      nmsp  = child->entity->nmsp->name;
      break;
    }
    default: {
      name  = child->entity->name;
      file  = (child->entity->file) ? child->entity->file->name : "";
      imgId = child->entity->img->id;
      nmsp  = child->entity->nmsp->name;
    }
  }

  entities->emplace_back(Json::object{
    {"id", static_cast<int>(child->entity->id)},
    {"type", static_cast<int>(child->entity->getEntityType())},
    {"name", name},
    {"baseclasses", baseClassesArray},
    {"image", static_cast<int>(imgId)},
    {"namespace", nmsp},
    {"file", file},
    {"line", static_cast<int>(child->entity->line)},
    {"children", *childObjects},
  });
}

void addArtifact(Json::array *entities, const pcv::Artifact_t *artifact)
{
  for (auto &child : artifact->children) {
    auto childObjects = Json::array{};
    addArtifact(&childObjects, child.get());

    if (child->entity == nullptr) {
      /// add abstract artifact
      entities->emplace_back(Json::object{
        {"id", "a" + std::to_string(artifactId++)},
        {"name", child->name},
        {"children", childObjects},
      });
    } else {
      addEntity(entities, &childObjects, child.get());
    }
  }
}

void addFilterArtifact(Json::array *entities, const pcv::Artifact_t *artifact)
{
  for (auto &child : artifact->children) {
    auto childObjects = Json::array{};
    addFilterArtifact(&childObjects, child.get());

    if (child->entity == nullptr) {
      /// add abstract artifact
      entities->emplace_back(Json::object{
        {"id", "a" + std::to_string(artifactId++)},
        {"name", child->name},
        {"children", childObjects},
      });
    } else if (child->entity->getEntityType() == EntityType::Routine ||
               child->entity->getEntityType() == EntityType::Image) {
      entities->emplace_back(Json::object{
        {"id", static_cast<int>(child->entity->id)},
        {"type", static_cast<int>(child->entity->getEntityType())},
        {"name", child->entity->name},
        {"namespace", (child->entity->nmsp) ? child->entity->nmsp->name : ""},
        {"children", childObjects},
      });
    } else {
      for (const auto &o : childObjects) entities->emplace_back(o);
    }
  }
}

void addLinks(Json::array *links, const Artifact_t *set)
{
  auto isEmpty = [&set](const std::string &aName) {
    auto it = std::find_if(
      begin(set->children), end(set->children), [&aName](const std::unique_ptr<Artifact_t> &a) {
        return a->name == aName;
      });
    return it == end(set->children);
  };

  auto addLink = [&links](const std::string &lhs, const std::string &rhs) {
    links->emplace_back(Json::object{
      {"source", lhs},
      {"target", rhs},
      {"value", 5},
    });
  };

  for (auto &child : set->children) {
    std::vector<std::string> artifactIds;

    // first (maybe only) artifact
    auto pos = child->name.find(DELIM);
    auto tmp = child->name.substr(0, pos);
    if (!isEmpty(tmp)) artifactIds.emplace_back(tmp);

    if (pos != std::string::npos) {
      for (; pos++ != std::string::npos; pos = child->name.find(DELIM, pos + 1)) {
        auto tmp = child->name.substr(pos, child->name.find(DELIM, pos));
        if (!isEmpty(tmp)) artifactIds.emplace_back(tmp);
      }

      for (auto &id : artifactIds) addLink(id, child->name);
    }
  }
}

}  // namespace

namespace pcv {

using pcv::Artifact_t;

VisitorContext::VisitorContext(const Context &ctxt, std::ostream &out)
  : archBuilder_(ctxt), out_(out), artifact_("default", nullptr), filterArtifact_("filter", nullptr)
{
  switchToDefaultArtifact();
}

void VisitorContext::pushToArchRulesStack(ArchRule *archRule) { archRulesStack.push(archRule); }

ArchRule *VisitorContext::popFromArchRulesStack()
{
  ArchRule *topElement = archRulesStack.top();
  archRulesStack.pop();
  return topElement;
}

void VisitorContext::addArtifactToArchBuilder(std::string artifactName, ArchRule *archRule)
{
  archRule->setArtifactName(artifactName);
  currentArtifact_->children.push_back(std::unique_ptr<Artifact_t>(archRule->getArtifact()));
}

void VisitorContext::addCloneForFilter(std::string artifactName, ArchRule *archRule)
{
  auto newRule = archRule->clone();
  newRule->setArtifactName(artifactName);
  currentArtifact_->children.push_back(std::unique_ptr<Artifact_t>(newRule->getArtifact()));
}

Artifact_t *VisitorContext::getArtifactFromArchBuilder(std::string artifactName)
{
  for (auto &child : currentArtifact_->children) {
    if (child->name == artifactName) {
      return child.get();
    }
  }
  return nullptr;
}

void VisitorContext::outputBuilderJson()
{
  archBuilder_.finish();
  out_ << *this;
}

void VisitorContext::applyRuleToBuilder(ArchRule *archRule)
{
  archBuilder_.apply(archRule, currentArtifact_);
}

std::ostream &operator<<(std::ostream &os, const VisitorContext &obj)
{
  Json::array children{}, filters{}, links{};

  addArtifact(&children, &obj.artifact_);
  addLinks(&links, &obj.artifact_);
  addFilterArtifact(&filters, &obj.filterArtifact_);

  auto out =
    Json::object{{"id", "root"}, {"children", children}, {"links", links}, {"filters", filters}};

  os << Json(out).dump();
  return os;
}

}  // namespace pcv
