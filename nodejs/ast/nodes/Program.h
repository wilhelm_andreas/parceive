//
// Created by Faris Cakaric on 28/05/2017.
//

#pragma once

#include "ASTNode.h"
#include "Expression.h"

#include <memory>

namespace pcv {

using json11::Json;

class Program : public ASTNode
{
  std::vector<std::unique_ptr<Expression>> expressions;

 public:
  Program(std::vector<std::unique_ptr<Expression>>& expressions);

  const std::vector<std::unique_ptr<Expression>>& getExpressions() const;

  virtual void accept(Visitor& v) override;
};

}  // namespace pcv
