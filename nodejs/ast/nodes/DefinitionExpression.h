//
// Created by Faris Cakaric on 28/05/2017.
//

#pragma once

#include "Artifact.h"
#include "Expression.h"

#include <json11.hpp>

namespace pcv {

using json11::Json;

class DefinitionExpression : public Expression
{
  std::unique_ptr<Artifact> artifact;
  std::unique_ptr<Expression> expression;

 public:
  DefinitionExpression(std::unique_ptr<Artifact> &artifact,
                       std::unique_ptr<Expression> &expression);

  const std::unique_ptr<Artifact> &getArtifact() const;

  const std::unique_ptr<Expression> &getExpression() const;

  virtual void accept(Visitor &v) override;
};

}  // namespace pcv
