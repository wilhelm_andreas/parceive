//
// Created by Faris Cakaric on 16/05/2017.
//

#pragma once

#include <iostream>
#include <json11.hpp>
#include <string>
#include "ASTNode.h"

namespace pcv {

using json11::Json;

class Expression : public ASTNode
{
 public:
  virtual void accept(Visitor& v) override;
};

}  // namespace pcv
