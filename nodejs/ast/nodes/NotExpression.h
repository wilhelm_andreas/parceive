//
// Created by Faris Cakaric on 28/05/2017.
//

#pragma once

#include "Expression.h"

#include <json11.hpp>

namespace pcv {

using json11::Json;

class NotExpression : public virtual Expression
{
  std::unique_ptr<Expression> operand;

 public:
  NotExpression(std::unique_ptr<Expression> &operand);

  const std::unique_ptr<Expression> &getOperand() const;

  virtual void accept(Visitor &v) override;
};

}  // namespace pcv
