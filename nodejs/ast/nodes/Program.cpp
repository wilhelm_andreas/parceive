//
// Created by Faris Cakaric on 28/05/2017.
//

#include "Program.h"
#include "../visitor/Visitor.h"

namespace pcv {

Program::Program(std::vector<std::unique_ptr<Expression>> &expressions)
  : expressions(std::move(expressions))
{}

void Program::accept(Visitor &v) { v.visit(*this); }

const std::vector<std::unique_ptr<Expression>> &Program::getExpressions() const
{
  return expressions;
}

}  // namespace pcv
