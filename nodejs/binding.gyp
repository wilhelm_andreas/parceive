{
  "targets": [
    {
      "target_name": "symbols",
      "sources": [
        "module.cpp",
        "SymbolLoader.cpp",
        "SymbolLoaderWrapper.cpp",
        "architecture/src/AndOperatorRule.cpp",
        "architecture/src/ArchBuilder.cpp",
        "architecture/src/ArchRule.cpp",
        "architecture/src/BinaryOperatorRule.cpp",
        "architecture/src/ClassHierarchyRule.cpp",
        "architecture/src/ClassRule.cpp",
        "architecture/src/FunctionRule.cpp",
        "architecture/src/ImageRule.cpp",
        "architecture/src/NamespaceRule.cpp",
        "architecture/src/NotOperatorRule.cpp",
        "architecture/src/OrOperatorRule.cpp",
        "architecture/src/RegexFileRule.cpp",
        "architecture/src/RegexNameRule.cpp",
        "architecture/src/RegexRule.cpp",
        "architecture/src/SetOperatorRule.cpp",
        "architecture/src/TemplateHelper.cpp",
        "architecture/src/VariableRule.cpp",
        "ast/ast-from-json/CreateAstFromJson.cpp",
        "ast/nodes/AndExpression.cpp",
        "ast/nodes/Artifact.cpp",
        "ast/nodes/ASTNode.cpp",
        "ast/nodes/AssignmentExpression.cpp",
        "ast/nodes/AtomExpression.cpp",
        "ast/nodes/DefinitionExpression.cpp",
        "ast/nodes/Expression.cpp",
        "ast/nodes/NotExpression.cpp",
        "ast/nodes/OrExpression.cpp",
        "ast/nodes/FilterExpression.cpp",
        "ast/nodes/Program.cpp",
        "ast/nodes/SetExpression.cpp",
        "ast/visitor/ASTVisitor.cpp",
        "ast/visitor/Visitor.cpp",
        "ast/visitor/VisitorContext.cpp",
        "../src/SoftwareEntity.cpp",
        "../src/FileFilter.cpp",
        "../src/Context.cpp",
      ],
      "conditions": [
        ['OS=="linux"', {
          "sources": [
            "../dwarf/src/DwarfContext.cpp",
            "../dwarf/src/Duplicate.cpp",
            "../dwarf/src/DwarfHelper.cpp",
            "../dwarf/src/DwarfReader.cpp",
            "../dwarf/src/Type.cpp",
          ],
          "cflags": ["-std=c++11" ,"-fexceptions" ,"-fPIC", "-fno-omit-frame-pointer" ],
          "cflags_cc": [ "-fexceptions", "-fpermissive", "-fPIC" ],
          "cflags_cc!": [ "-fno-rtti" ],
          "include_dirs": [
            "3rdparty/libdwarf/libdwarf",
            "3rdparty/libelf/lib",
            "../dwarf/include",
          ],
          "link_settings": {
            "libraries": [
              "-L/usr/local/lib",
              "../3rdparty/libdwarf/libdwarf/libdwarf.a",
              "../3rdparty/libelf/lib/libelf.a",
              "../3rdparty/libz/libz.a",
              "../3rdparty/json11/libjson11.a"
            ]
          }
        },
        'OS=="win"', {
            "sources": [
              "3rdparty/json11/json11.cpp",
              "../dbghelp/src/DbgCollector.cpp",
              "../dbghelp/src/DbgHelpApi.cpp",
              "../dbghelp/src/DbgHelpReader.cpp",
              "../dbghelp/src/DbgTypeConverter.cpp"
            ],
            "include_dirs": [
              "../dbghelp/include",
            ],
            "configurations": {
              "Debug": {
                "msvs_settings": {
                  "VCCLCompilerTool": {
                    "RuntimeTypeInfo": 'true',
                    "AdditionalOptions": ['/EHsc'], 
                  }
                }
              },
              "Release": {                            
                "msvs_settings": {
                  "VCCLCompilerTool": {
                    "RuntimeTypeInfo": 'true',
                    "AdditionalOptions": ['/EHsc'], 
                  }
                }
              }
            }

        }],
      ],
      "include_dirs": [
        "3rdparty/json11",
        "3rdparty/libz",
	      "../include",
        "architecture/include",
      ],
      "defines": [
          "_HAS_EXCEPTIONS=1",
	        "NODEJS=1" 
      ],
    }
  ]
}
