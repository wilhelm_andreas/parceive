export npm_config_arch=x64
export npm_config_disturl=https://atom.io/download/electron
export npm_config_runtime=electron
export npm_config_target=1.6.11
export npm_config_build_from_source=true
export JOBS=8
set -e

node-gyp configure
HOME=~/.electron-gyp node-gyp build
cp -f build/Release/symbols.node ../../ui/assets/
