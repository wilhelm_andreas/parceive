//
// Created by wilhelma on 7/12/17.
//

#ifndef PARCEIVE_CALLSTACK_H
#define PARCEIVE_CALLSTACK_H

#include <array>
#include <memory>
#include <unordered_map>
#include <unordered_set>

#include "pin.H"

#include "model/Access.h"
#include "model/Call.h"
#include "model/Function.h"
#include "model/Instruction.h"
#include "model/Segment.h"

namespace pcv {

/*!
 * @brief Holds a access location containing the line number and the adress of the access.
 */
struct Location {
  int lineNo;
  ADDRINT address;
  model::Access::Type type;
  Location() : lineNo(0), address(0), type(model::Access::Type::READ) {}
  explicit Location(int l, ADDRINT a, model::Access::Type t) : lineNo(l), address(a), type(t) {}

  inline bool operator==(const Location &b) const
  {
    return (lineNo == b.lineNo && address == b.address && type == b.type);
  }
};

struct LocationHash {
  size_t operator()(const Location &l) const
  {
    auto a = std::hash<uint8_t>{}(l.lineNo);
    auto b = std::hash<ADDRINT>{}(l.address << 1);
    auto c = std::hash<int>{}(static_cast<int>(l.type));
    return a ^ (b >> 1) ^ (c << 1);
  }
};

class Resolver;

/*! \brief This class specifies information which is maintained
 * as part of the call stack for each thread
 */
struct StackFrame {
  /// Specifies the ID of the routine currently being executed
  model::Function::Id_t routineId;
  /// Specifies the call trace ID
  model::Call::Id_t callId;
  /// Specifies the last segment Id
  model::Segment::Id_t segmentId;
  /// Specifies the clock cycle at the start of the routine
  R_TIMERTYPE startTime;
  /// Specifies the ID of the instruction which caused the CALL
  model::Instruction::Id_t instructionId;
  /// StackPointer for the current call.
  ADDRINT sp;
  /// Return IP for the current call.
  ADDRINT returnIP;
  /// Specifies the current line number of the last call
  uint32_t lastCallLineNo;
  unsigned long skip_depth;
  bool isPartial;

  /// returns true if the address was recently accessed at the same source line
  bool isRecentlyAccessed(int lineNo, ADDRINT address, model::Access::Type type);

  StackFrame(model::Function::Id_t routine_id,
             model::Call::Id_t call_id,
             R_TIMERTYPE start_time,
             model::Instruction::Id_t instruction_id,
             ADDRINT sp,
             ADDRINT return_ip,
             bool isPartial = false)
    : routineId(routine_id),
      callId(call_id),
      segmentId(model::Segment::EMPTY),
      startTime(start_time),
      instructionId(instruction_id),
      sp(sp),
      returnIP(return_ip),
      lastCallLineNo(0),
      skip_depth(0),
      isPartial(isPartial)

  {}

 private:
  std::unordered_set<Location, LocationHash> accessCacheSet;
};

/*!
 * The CallStack class manages the call stack and the necessary instrumentation
 * / analysis.
 */
class CallStack
{
 public:
  /*!
   * @brief adjust the stack in case of a longjump according to the current
   * stack pointer
   */
  void adjustStackEnter(ADDRINT sp,         /**< [IN] the current stack pointer. */
                        Resolver *resolver /**[in] the resolver to clean the stack reference */);

  /*!
   * @brief adjust the stack in case of a longjump according to the current
   * stack pointer
   */
  void adjustStackLeave(ADDRINT sp,         /**< [in] the current stack pointer. */
                        ADDRINT returnIP,   /**< [in] the address of the return instruction. */
                        Resolver *resolver  /**< [in] the resolver to clean the stack reference */);

  /*!
   * @brief adds a stack frame to the stack.
   */
  void addToStack(std::unique_ptr<StackFrame> framePtr /**< [in] the new stack frame.*/);

  /*!
   * @brief pops the stack.
   */
  void pop(Resolver *resolver);

  /*!
   * @returns a pointer to the top of the stack.
   */
  StackFrame *top();

  /*!
   * @returns a pointer to the parent call of the top of the stack.
   */
  StackFrame *beforeTop();

  /*!
   * @returns true if the call stack is empty.
   */
  bool empty();

 private:
  std::vector<std::unique_ptr<StackFrame>> callStack_;
};

}  // namespace pcv

#endif  // PARCEIVE_CALLSTACK_H
