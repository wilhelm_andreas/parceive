/*! \file logger.h
 *  \brief This file contains the base logger class.
 *
 * Copyright 2015 Siemens Technology and Services
 */
#ifndef SRC_BACKEND_INCLUDE_LOGGER_H_
#define SRC_BACKEND_INCLUDE_LOGGER_H_

#include <string>

namespace pcv {

/*! \brief
 * The Logger class provides the << operator for logging. Every inherited
 * logger must override the log method.
 */
class Logger
{
 public:
  Logger()          = default;
  virtual ~Logger() = default;

  virtual void log(const std::string &str) = 0;

  template <typename T>
  Logger &operator<<(const T &data)
  {
    log(std::string(data));
    return *this;
  }
};

}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_LOGGER_H_
