/*! \file InstrumentThreadLibrary.h
 *  \brief This file contains the InstrumentThreadLibrary class which is the
 * base class for instrumenting functions in different thread libraries, like
 * Win32, PThread etc.
 */

/* Copyright 2014 Siemens Technology and Services*/

#ifndef SRC_BACKEND_INCLUDE_INSTRUMENTTHREADLIBRARY_H_
#define SRC_BACKEND_INCLUDE_INSTRUMENTTHREADLIBRARY_H_

#include <pin.H>
#include <stack>

#include "CallStack.h"
#include "DataModel.h"
#include "DebugOptions.h"
#include "Module.h"

namespace pcv {

class DataModel;
class Routine;
class Resolver;
class Parceive;

struct AllocationData {
  /*! The address passed to realloc */
  S_ADDRUINT reAllocAddress;
  /*! The size parameter passed to functions handling dynamic memory allocation */
  S_UINT size;
  /*! The address of the next instruction after allocation call */
  S_ADDRINT returnIP;
  /*! The line number of the latest call instruction. */
  int32_t lineNo;
};

/*! \brief ThreadTLS class maintains the Thread-specific information. This
 * data is stored in Pin's TLS
 *
 *   For each thread, it maintains
 *       pointer to the requested criticalSection
 *       pointer to the requested mutex
 *       pointer to the set of prepared statements, for database writing
 *       pointer to the array of vectors of QueryObjects
 *   Note: In the current implementation, we are not storing
 *   callStackPTR in the TLS, as for the PinTool, we have
 *   not tested multiple cache lines. All the data in the TLS,
 *   currently can be fitted into a single cache line, so that there
 *   is no contention among threads. In the next release, we will
 *   consider the multiple-cache line approach, and the total capacity
 *   of the TLS.
 *   Note2: If you add members to this structure, consider whether the cache
 *   line changes for 32-bit or 64-bit applications. Hence, make sure to
 *   update the pad size, UINT8 _pad.
 */
class ThreadData
{
 public:
  using segmentAccesses_t = std::unique_ptr<std::unordered_set<ADDRINT>>;

  ThreadData()
    : model(model::Thread::EMPTY,
            0,
            0,
            model::Instruction::EMPTY,
            model::Instruction::EMPTY,
            model::Thread::EMPTY,
            model::Call::MAIN_CALL),
      callStack(std::unique_ptr<CallStack>{new CallStack})
  {}

  inline CallStack *getCallStack() const { return callStack.get(); };

  /*! Gets the current size of the allocation stack */
  size_t getAllocationStackSize() const;
  /*! Gets whether the allocation stack is empty */
  bool isAllocationStackEmpty() const;
  /*! Gets the topmost data from the allocation stack.
   *  It is assumed that the stack is non empty when this
   *  method is called. */
  const AllocationData &getTopAllocationData() const;
  /*! Pushes a given instance onto the allocation stack */
  void pushAllocationData(const AllocationData &data);
  /*! Remvoes the topmost item from the allocation stack.
   *  Returns true when an item was pushed or else false. */
  bool popAllocationData();

  /// Thread data that is filled during run time, and written at finalization
  model::Thread model;

  /// The line number of the last instruction.
  int32_t lineNo;

 private:
  std::unique_ptr<CallStack> callStack;
  // separate from the callstack since data processing is filter-invariant
  std::stack<AllocationData> allocations;
};

class Thread : public Module
{
 public:
  Thread(Parceive *parceive);
  virtual ~Thread() override;

  virtual void initialize() override;
  virtual void finalize() override;

  void beforeInstrumentation() override;

  /**
   * @brief Returns the thread id of the current executing thread. Usually this
   * is the thread id set by PIN, except on emulated thread handling where no
   * thread libraries are involved.
   * @return The current thread.
   */
  static model::Thread::Id_t getThreadId(const THREADID &threadId);

  ThreadData *getThreadData(THREADID);

 protected:
  void setThreadData(THREADID, std::unique_ptr<ThreadData>);
  void removeThreadData(THREADID);

  virtual void initializeThread(THREADID);
  virtual void finalizeThread(THREADID);

  /*! \brief the callback for thread start */
  static void cb_threadStart(THREADID threadId, CONTEXT *ctxt, INT32 code, VOID *v);

  /*! \brief the callback for thread fini */
  static void cb_threadFini(THREADID threadId, const CONTEXT *ctxt, INT32 code, VOID *v);

  /*! Stores per-thread data */
  std::map<THREADID, std::unique_ptr<ThreadData>> threadDataMap;
  PIN_RWMUTEX mutex;

  DataModel *pModel;
};

}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_INSTRUMENTTHREADLIBRARY_H_
