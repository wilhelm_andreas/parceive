/*! \file PinToolAnalyzeWin32ThreadLibrary.h
 * \brief This file contains the PinToolAnalyzeWin32ThreadLibrary
 * class which contains the analysis functions to be called for the functions
 * instrumented in the Win32 Thread library
 *
 * Copyright 2014 Siemens Technology and Services
 */

#ifndef SRC_BACKEND_INCLUDE_PINTOOLDEBUGHELP_H_
#define SRC_BACKEND_INCLUDE_PINTOOLDEBUGHELP_H_

#ifdef _WIN32

#include <string>

#include "PinTool.h"

/*!\brief Derivation of PinTool for Debughelp (Windows).
 *
 * The PinToolDebugHelp class is used to handle debughelp (windows)
 * specific functionality.
 */
class PinToolDebugHelp : public PinTool
{
 public:
  explicit PinToolDebugHelp(const Factory& factory) : PinTool(factory) {}
  ~PinToolDebugHelp() {}

  void resolveRtnName(string* origRtnName);

 private:
  // prevent generation of methods
  PinToolDebugHelp(const PinToolDebugHelp&);
  PinToolDebugHelp& operator=(const PinToolDebugHelp&);
};

#endif

#endif  // SRC_BACKEND_INCLUDE_PINTOOLDEBUGHELP_H_
