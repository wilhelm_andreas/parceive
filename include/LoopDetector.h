/*
 * Copyright (c) 2016, Siemens AG. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef INCLUDE_LOOPDETECTOR_H_
#define INCLUDE_LOOPDETECTOR_H_

#include <boost/dynamic_bitset.hpp>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>

#include "pin.H"

#include "Parceive.h"
#include "model/Loop.h"

namespace pcv {

// pin interface
VOID LoopDetectorIMGLoad(IMG img, Parceive* pinTool);

// forward declaration
struct BasicBlockLoop;

// types
using leaders_t = std::set<ADDRINT>;
typedef std::map<ADDRINT, ADDRINT> jumps_t;
using loopString_t = std::string;
using loopNo_t     = uint32_t;
typedef std::map<ADDRINT, std::string> dissmap_t;

/*
 * The BasicBlock class has two purposes:
 * 1) A class for basic block instances (with public attributes).
 * 2) A container for the whole cfg and corresponding (static) loop
 *  identification methods.
 */
class BasicBlock
{
 public:
  using id_t   = size_t;
  using bbls_t = std::vector<BasicBlock*>;
  typedef std::map<ADDRINT, BasicBlockLoop*> loops_t;

  // attributes of a basic block
  id_t id;                      // id of the basic block (starting from 0)
  ADDRINT entry_ins;            // address of the first instruction
  ADDRINT exit_ins;             // address of the last instruction
  std::set<id_t> predecessors;  // set of id's of all predecessors
  std::set<id_t> dominators;    // set of id's of all dominators
  BasicBlock* successor;        // pointer to the successor
  BasicBlock* target;           // pointer to the target (if existing)
  BasicBlock* iDom;             // immediate dominator

  /**
   * Constructor
   */
  explicit BasicBlock(ADDRINT entry_ins,            /**< [IN] First instruction. */
                      const BasicBlock* predecessor /**< [IN] The optional predecessor bb. */
  );

  /**
   * Resets all basic blocks.
   */
  static void reset();

  /**
   * Get pointer to the basic block with given id.
   */
  static BasicBlock* get(id_t id /**< [IN] The basic block id. */
  )
  {
    return gBasicBlocks[id];
  }

  /**
   * Get the current number of inserted basic blocks.
   */
  static size_t size() { return gBasicBlocks.size(); }

  /**
   * Get the root basic block.
   */
  static BasicBlock* getRoot() { return root; }

  /**
   * Dump control flow graph to the given stream.
   */
  static void printCFG(std::ofstream* ofs /**< [OUT] The ofstream used for dumping. */
  );

  /**
   * Dump the dominator tree.
   */
  static void printDOM(std::ofstream* ofs /**< [OUT] The ofstream used for dumping. */
  );

  /**
   * Identify all loops of a pin routine by using the cfg and the dominators
   * (see the dragon book for the description of the algorithm).
   */
  static void identifyLoops(RTN rtn,                  /**< [IN] The pin routine. */
                            const leaders_t& leaders, /**< [IN] The leader bb's.*/
                            const jumps_t& jumps,     /**< [IN] The jumps. */
                            loops_t* loops            /**< [OUT] The found loops. */
  );

 private:
  // print control flow graph of the given node and all successors/targets
  static void printCFG(BasicBlock* node, std::ofstream* ofs, int level);

  // print dominator tree of the given node and all successors/targets
  static void printDOM(BasicBlock* node, std::ofstream* ofs, int level);

  // get loops dynamically
  static void buildDomTree(RTN rtn, const leaders_t& leaders, const jumps_t& jumps);

  // collect all loops
  static void collectLoop(id_t header, id_t current, BasicBlockLoop* loop);

  // identify loops by using the cfg and the dominators
  static void identifyLoops(loops_t* loops);

  /* buildStaticBBLs()
   *
   * builds a cfg consisting of newly allocated bbl's and returning the root
   * of the cfg.
   * This is done by considering each leader instruction as the beginning of a
   * bbl, each transition as a successor relation, and each jump as a target
   * relation. The predecessor attribute is filled accordingly to the successor
   * and target relation. The dominator attribute is not filled here (see
   * getDominators()).
   */
  static void buildStaticBBLs(RTN rtn, const leaders_t& leaders, const jumps_t& jumps);

  /* getDominators()
   *
   * computes the dominators of all basic blocks in the cfg
   */
  static void getDominators(BasicBlock* root);

  static BasicBlock::id_t gID;
  static bbls_t gBasicBlocks;
  static BasicBlock* root;
};

/*
 * struct Loop
 */
struct BasicBlockLoop {
  BasicBlock::id_t head;
  BasicBlock::id_t tail;
  std::set<BasicBlock::id_t> nodes;
  std::set<BasicBlock::id_t> exits;
};

class LoopDetector : public Module
{
 public:
  LoopDetector(Parceive* parceive);
  ~LoopDetector() override;

  void initialize() override;
  void finalize() override;

  void instrumentRoutine(IMG img, RTN rtn) override;

  void instrumentLoops(RTN rtn, BasicBlock::loops_t* loops, unsigned int eRtnId);

  static void callOnLoopEntry(LoopDetector* pThis,
                              THREADID THREADID,
                              model::Loop::Id_t loopId,
                              uint32_t rtnId,
                              uint32_t line);
  static void callOnLoopExit(LoopDetector* pThis,
                             THREADID THREADID,
                             model::Loop::Id_t loopId,
                             uint32_t rtnId);

 protected:
  DataModel* pModel;
  Thread* pThread;
};

}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_LOOPDETECTOR_H_
