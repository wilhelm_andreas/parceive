#pragma once

#ifndef DC_CPP_COMMON_H_
#define DC_CPP_COMMON_H_

#include <algorithm>
#include <cstdint>
#include <iterator>

#ifdef __GNUC__
#define UNUSED_ATTR __attribute__((unused))
#else
#define UNUSED_ATTR
#endif

namespace pcv {

#if defined(_MSC_VER)

#define S_UINT32 unsigned __int32
#define S_UINT64 unsigned __int64
#define S_INT32 __int32
#define S_INT64 __int64
#define S_DWORD32 WINDOWS::DWORD
#define S_DWORD64 WINDOWS::DWORD64

#ifdef _WIN64
#define S_ADDRUINT S_UINT64
#define S_ADDRINT S_INT64
#define S_MAXUINT _UI64_MAX
#define S_DWORD S_DWORD64
#define S_UINT S_UINT64
#define FRAME_POINTER "rsp"
#define S_PINTOOL_OFFSET_REG REG_RSP
#elif defined _WIN32
#define S_ADDRUINT S_UINT32
#define S_ADDRINT S_INT32
#define S_MAXUINT UINT_MAX
#define S_DWORD S_DWORD32
#define S_UINT S_UINT32
#define FRAME_POINTER "ebp"
#define S_PINTOOL_OFFSET_REG REG_EBP
#endif


#elif defined __linux__
using S_UINT32 = uint32_t;
using S_UINT64 = uint64_t;
using S_INT32 = int32_t;
using S_INT64 = int64_t;
#define S_PINTOOL_OFFSET_REG REG_GBP
#if defined __x86_64__
#define FRAME_POINTER "rbp"
#define S_ADDRUINT S_UINT64
#define S_ADDRINT S_INT64
#define S_UINT S_UINT64
#elif defined __i386__
#define FRAME_POINTER "ebp"
#define S_ADDRUINT S_UINT32
#define S_ADDRINT S_INT32
#define S_UINT S_UINT32
#else
#error Unsupported architecture
#endif
#endif

using Id_t = uint64_t;
using Symbol_t = unsigned long long;

#define PLOG(msg) printf(msg)                     // NOLINT(runtime/printf)
#define PLOGP(msg, ...) printf(msg, __VA_ARGS__)  // NOLINT(runtime/printf)

template <typename Container, typename Value>
static int indexOf(const Container& container, const Value& value)
{
  const auto beginIt = std::begin(container);
  const auto endIt   = std::end(container);
  const auto it      = std::find(beginIt, endIt, value);
  return (it == endIt) ? -1 : static_cast<int>(it - beginIt);
}

template <typename Container, typename Value>
static bool contains(const Container& container, const Value& key)
{
  const auto beginIt = std::begin(container);
  const auto endIt   = std::end(container);
  const auto it      = std::find(beginIt, endIt, key);
  return it != endIt;
}

template <typename T>
const T& max(const T& lhs, const T& rhs)
{
  return (lhs < rhs) ? rhs : lhs;
}

UNUSED_ATTR static int lastIndexOf(const std::string& str, const char c)
{
  for (int idx = static_cast<int>(str.length()) - 1; idx >= 0; --idx) {
    if (str[idx] == c) {
      return idx;
    }
  }
  return -1;
}

UNUSED_ATTR static bool endsWith(const std::string& value, const std::string& ending)
{
  if (ending.size() > value.size()) return false;
  return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

UNUSED_ATTR static bool startsWith(const std::string& value, const std::string& start)
{
  if (start.size() > value.size()) return false;
  return std::equal(start.begin(), start.end(), value.begin());
}

UNUSED_ATTR static std::string toLower(const std::string& value)
{
  std::string result;
  std::transform(value.begin(), value.end(), std::back_inserter(result), ::tolower);
  return result;
}

UNUSED_ATTR static std::string toUpper(const std::string& value)
{
  std::string result;
  std::transform(value.begin(), value.end(), std::back_inserter(result), ::toupper);
  return result;
}

bool matchesRegEx(const std::string& regex, const std::string& pattern);
bool isValidRegEx(const std::string& regEx);

}  // namespace pcv

#endif  // DC_CPP_COMMON_H_
