/*! \file PinToolRoutineProfiler.h
 *  \brief This file contains PinToolRoutineProfiler class which extends the
 * RoutineProfiler class. Contains the routine profiler developed using APIs
 * of Intel Pin framework
 */

/* Copyright 2014 Siemens Technology and Services*/

#ifndef SRC_BACKEND_INCLUDE_PINTOOLROUTINEPROFILER_H_
#define SRC_BACKEND_INCLUDE_PINTOOLROUTINEPROFILER_H_

#pragma once
#include <entities/ImageScopedId.h>
#include <memory>

#include "CallStack.h"
#include "DataModel.h"
#include "DebugOptions.h"
#include "Module.h"
#include "Timer.h"
#include "model/Call.h"

namespace pcv {

class Resolver;
class Thread;
class Filter;

/*! \brief PinToolRoutineProfiler class extends RoutineProfiler class and
 * provides functionality for the analysis routines to be added by the
 * PinTool
 */
class Routine : public Module
{
 public:
  bool isExceptionThrown = false;

  explicit Routine(Parceive *parceive);
  ~Routine() = default;

  void initialize() override;
  void finalize() override;

  void instrumentImage(IMG img) override;
  void instrumentTrace(IMG, TRACE, bool) override;

 private:
  /*!
   * @brief register a register to a specific class instance.
   */
  void registerInstance(S_ADDRUINT address,
                        const entity::ImageScopedId &cls,
                        pcv::model::Call::Id_t callerRtnId,
                        pcv::model::Call::Id_t callerId,
                        S_ADDRUINT caller_bp,
                        S_ADDRUINT sp) const;

  /*! \brief Function specifies the analysis to be done, before entering a
   * routine.
   * \return void
   */
  static void cbRoutineEnter(Routine *pRoutine, /**< [IN] Pointer to the Parceive object */
                             THREADID threadId, /**< [IN] ID of the thread executing the routine */
                             ADDRINT targetIp,  /**< [IN] Address of the call target instruction */
                             ADDRINT returnIp, /**< [IN] Address of the return target instruction */
                             ADDRINT sp,       /**< [IN] Value of the stack pointer */
                             const ADDRINT *arg0, /**< [IN] Pointer to the first argument */
                             ADDRINT bp /**< [IN] The base pointer */);

  /*! \brief Function specifies the analysis to be done, after leaving a
   * routine.
   * \return void
   */
  static void cbRoutineLeave(
    Routine *pRoutine,     /**< [IN] Pointer to the PinToolRoutineProfiler object */
    unsigned int threadId, /**< [IN] ID of the thread executing the routine */
    S_ADDRUINT routineId,  /**< [IN] The routine id */
    ADDRINT sp,            /**< [IN] The stack pointer */
    ADDRINT returnIp /**< [IN] The address of the return target */);

  /*!
   * @brief Analysis function for entering main.
   */
  static void cbMainEnter(Routine *pRoutine, /**< [IN] The pointer to the parceive instance.*/
                          uint32_t rtnId,    /**< [IN] The routine id of main.*/
                          ADDRINT sp,        /**< [IN] The stack pointer on entering main.*/
                          THREADID threadId);

  /*!
   * @brief Analysis function for leaving main.
   */
  static void cbMainLeave(Routine *pRoutine, /**< [IN] Pointer to the parceive instance.*/
                          THREADID threadid /**< [IN] Current thread id.*/);

  /*!
   * @brief Analysis function to capture the line number of the current call.
   */
  static void cbCaptureCallingLineNo(Routine *pRoutine, /**< [IN] Pointer to the routine instance.*/
                                     THREADID threadid, /**< [IN] Current thread id.*/
                                     uint32_t rtnId,    /**< [IN] The id of the caller routine.*/
                                     uint32_t lineNo /**< [IN] The captured line number.*/);

  /*!
   * @brief Analysis function for throwing an exception. This is necessary to correctly build the
   * call stack.
   */
  static void cbRaiseException(Routine *pRoutine /**< [IN] The pointer to the parceive instance.*/);

  /*!
   * @brief Analysis function for checking routines during runtime.
   */
  static ADDRINT cbTargetInteresting(
    ADDRINT target, /**< [IN] The address of the target instruction.*/
    Routine *pRoutine /**< [IN] The pointer to the parceive instance.*/);

  DataModel *pModel;
  Resolver *pResolver;
  pcv::Thread *pThread;
  Filter *pFilter;
};

}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_PINTOOLROUTINEPROFILER_H_
