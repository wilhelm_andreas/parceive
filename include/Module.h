/*
 * Module.h
 *
 *  Created on: Nov 17, 2016
 *      Author: adriaan
 */

#ifndef MODULE_H_
#define MODULE_H_

#include "pin.H"

namespace pcv {

class Parceive;

class Module
{
 public:
  explicit Module(Parceive *);
  virtual ~Module() = default;

  virtual void initialize() = 0;
  virtual void finalize()   = 0;
  virtual void beforeInstrumentation() {}
  virtual void beforeInstrumentImage(IMG) {}
  virtual void instrumentImage(IMG) {}
  virtual void instrumentTrace(IMG, TRACE, bool) {}
  virtual void instrumentRoutine(IMG, RTN) {}
  virtual void instrumentInstruction(IMG, RTN, INS) {}

 protected:
  Parceive *pParceive;
};

}  // namespace pcv

#endif /* MODULE_H_ */
