/*! \file Timer.h
 *  \brief This file contains functions for getting time stamps
 */

/* Copyright 2014 Siemens Technology and Services*/

#ifndef SRC_BACKEND_INCLUDE_TIMER_H_
#define SRC_BACKEND_INCLUDE_TIMER_H_

#include <chrono>
#include <cstdio>
#include <iomanip>
#include <sstream>
#include <string>

struct R_TIMERTYPE {
  uint64_t value;

  R_TIMERTYPE() : value(0) {}
  R_TIMERTYPE(uint64_t val) : value(val) {}
  operator uint64_t() const { return value; }
};

namespace pcv {

/*! \brief
 *  Function which gets the timer, i.e.,
 *  std::chrono::high_resolution_clock::time_point
 *  \return No. of clock cycles passed
 */
static inline R_TIMERTYPE getTimer()
{
  auto time = std::chrono::high_resolution_clock::now().time_since_epoch();
  auto ns   = std::chrono::duration_cast<std::chrono::nanoseconds>(time);
  R_TIMERTYPE result;
  result.value = static_cast<unsigned long long>(ns.count());
  return result;
}

static inline std::string makeTimeString(R_TIMERTYPE time)
{
  time_t t  = time.value / 1000000000;
  size_t ft = time.value % 1000000000;
  struct tm gt;
#ifdef WIN32
  gmtime_s(&gt, &t);
#else
  gmtime_r(&t, &gt);
#endif
  char time_buf[30];
  strftime(time_buf, sizeof(time_buf), "%Y-%m-%d %H:%M:%S", &gt);
  std::stringstream str;
  str << time_buf << "." << std::setw(9) << std::setfill('0') << ft;
  return str.str();
}

static inline std::string getTimeString()
{
  auto time = getTimer();
  return makeTimeString(time);
}

/*! \brief
 * Function which gets the cpu frequency
 * \return No. of clock cycles per second
 */
R_TIMERTYPE getCPUFrequency();

}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_TIMER_H_
