/*! \file PinToolInstrumentWin32ThreadLibrary.h
 *  \brief This file contains the PinToolInstrumentWin32ThreadLibrary
 * class which contains instrumenting functions for Win32 Thread library
 */

/* Copyright 2014 Siemens Technology and Services*/

#ifndef SRC_BACKEND_INCLUDE_PINTOOLINSTRUMENTWIN32THREADLIBRARY_H_
#define SRC_BACKEND_INCLUDE_PINTOOLINSTRUMENTWIN32THREADLIBRARY_H_

#ifdef _WIN32

namespace pcv {

#pragma once
#include "PinToolAnalyzeWin32ThreadLibrary.h"
#include "Query.h"
#include "instrumentthreadlibrary.h"

/*! \brief This class extends the InstrumentThreadLibrary class and provides
 * instrumentation routines for the Win32 Thread library
 */
class PinToolInstrumentWin32ThreadLibrary : public InstrumentThreadLibrary
{
 private:
  /*! This is a pointer to an object of type PinToolAnalyzeWin32ThreadLibrary,
   * which provides access to the analysis routines for the functions
   * instrumented in the Win32 Thread library.
   */
  PinToolAnalyzeWin32ThreadLibrary *pinToolAnalyzeWin32ThreadLibraryObject;

 public:
  PinToolInstrumentWin32ThreadLibrary(void);
  ~PinToolInstrumentWin32ThreadLibrary(void);
  /*! This method sets the Query object used by the PinTool. The Query object
   * is used to generate queries for the data model
   * \return void
   */
  void setQuery(Query *query
                /**< [IN] Pointer to the Query object */);
  /*! This method sets the pointer to the list containing ThreadDataNode objects
   * \return void
   */
  void setThreadDataNodeList(ThreadDataNode **threadDataNodeList
                             /**< [IN] Pointer to the list containing
                                     ThreadDataNode objects */);
  /*! This method sets the pointer to the ThreadAnalyze object. This provides
   * access to the PinTool's TLS
   * \return void
   */
  void setThreadAnalyzeObject(ThreadAnalyze *threadAnalyzeObject
                              /**< [IN] Pointer to the ThreadAnalyze object */);
  /*! This method sets the pointer to the FilterInstrumentation object. This
   * provides access to the filters which have been defined for the PinTool
   * \return void
   */
  void setFilterObject(FilterInstrumentation *filterObject
                       /**< [IN] Pointer to the Filter(Blacklist) object */);

  /*! This method returns the Instrumentation object for the PThread library.
   * The pointer to this function is passed to the RegisterThreadLibrary() in
   * the ThreadLibraryFactory class.
   * \return The Instrumentation object for the Win32 Thread library
   */
  static InstrumentThreadLibrary *_stdcall Create()
  {
    return new PinToolInstrumentWin32ThreadLibrary();
  }

  /*! This is the friend method which instruments the images for
   * instrumenting routines for the Win32 Thread library.
   * \return void
   */
  friend void instrumentWin32ThreadLibrary(IMG img,
                                           /**< [IN] The Pin's IMG object */
                                           VOID *v
                                           /**< [IN] Pointer to the
                                           PinToolInstrumentaWin32ThreadLibrary
                                           object */);

  /*! This is the analysis routine which is called before main is executed. This
   * method calls PinToolAnalyzeWin32ThreadLibrary's tlibMainEnter method. This
   * prevents capturing of data before main is executed.
   * \return void
   */
  friend void instlibMainEnter(VOID *v
                               /**< [IN] Pointer to the
                               PinToolInstrumentaWin32ThreadLibrary
                               object */);

  /*! This is the analysis routine which is called after main is executed. This
   * method calls PinToolAnalyzeWin32ThreadLibrary's tlibMainLeave method. This
   * prevents capturing of data after main is executed.
   * \return void
   */
  friend void instlibMainLeave(
    VOID *v /**< [IN] Pointer to *PinToolInstrumentaWin32ThreadLibrary */);
};

}  // namespace pcv

#endif  // _WIN32

#endif  // SRC_BACKEND_INCLUDE_PINTOOLINSTRUMENTWIN32THREADLIBRARY_H_
