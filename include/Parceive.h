/*! \file PinTool.h
 *  \brief This file contains class for creating the Parceive PinTool.
 */

/* Copyright 2014 Siemens Technology and Services*/

#ifndef SRC_BACKEND_INCLUDE_PINTOOL_H_
#define SRC_BACKEND_INCLUDE_PINTOOL_H_

#include <pin.H>

#include <list>
#include <map>
#include <memory>
#include <string>

#include "Module.h"

namespace pcv {

class Filter;
class SymInfo;
class Resolver;
class DataModel;
class LoopDetector;
class Variable;
class Thread;
class Routine;
class Writer;

/*!
 * @brief An object of this class creates the PinTool. It has functions
 * for adding instrumentation, and specifying parameters for the PinTool.
 */
class Parceive
{
 public:
  explicit Parceive(int _argc, char **_argv);
  virtual ~Parceive(void);

  /*!
   * @brief initializes the modules, symbol handling and sets instrumentaion callbacks
   */
  void initialize();

  /*!
   * @brief finalizes the modules
   */
  void finalize();

  /*!
   * @returns true if any image of the user binary is optimized
   */
  bool isOptimized();

  /*!
   * @returns true if the pintool is executed in optimization check mode ("-optimized on")
   */
  bool isOptimizationCheckMode();

  /*!
   * @brief instrumentation of pin images
   */
  void instrumentImage(IMG img /**< [IN] the image */);

  /*!
   * @brief instrumentation of pin traces
   */
  void instrumentTrace(TRACE trace /**< [IN] the trace */);

  /*!
   * @brief instrumentation of pin routines
   */
  void instrumentRoutine(IMG img, /**< [IN] the image */
                         RTN rtn /**< [IN] the routine */);

  /*!
   * @brief instrumentation of pin instructions
   */
  void instrumentInstruction(IMG img, /**< [IN] the image */
                             RTN rtn, /**< [IN] the routine */
                             INS ins /**< [IN] the instruction */);

  /*!
   * @brief register an instrumentation callback module as observer
   */
  void registerModule(Module *module /**< [IN] the module to regiser */);

  /*!
   * @returns the commandline knob string for a given knob identifier
   */
  const std::string &getKnob(const std::string &knob /**< [IN] the knob identifier*/);

  /*!
   * @brief starts the program execution by pin
   */
  void startProgram();

  /*! \brief
   * 	This functions prints the Usage ( help )
   */
  void usage();

  /*! \brief
   * 	This is a friend function for instrumenting at image level
   */
  static void cb_instrumentImage(IMG img /**< [IN] Image to be instrumented*/,
                                 VOID *v /**< [IN] Used to pass data*/);

  /*! \brief
   * 	This is a friend function for instrumenting at trace level
   */
  static void cb_instrumentTrace(TRACE trace /**< [IN] Trace to be instrumented*/,
                                 VOID *v /**< [IN] Used to pass data*/);

  /*!
   * @brief callback function on program start
   */
  static void cb_programStart(
    void *v /**< [INOUT] the void pointer pointing to the parceive instance*/);

  /*!
   * @brief callback function on program end
   */
  static void cb_programEnd(
    INT32 Code, /**< [IN] the return code*/
    VOID *v /**< [INOUT] the void pointer pointing to the parceive instance*/);

  // getters
  inline pcv::Thread *getThread() const { return pThread.get(); }
  inline SymInfo *getSymInfo() const { return pSymInfo.get(); }
  inline DataModel *getModel() const { return pModel.get(); }
  inline Resolver *getResolver() const { return pResolver.get(); }
  inline Writer *getWriter() const { return pWriter.get(); }
  inline Filter *getFilter() const { return pFilter.get(); }
  inline bool getAnalysisEnabled() const { return analysisEnabled; }
  bool getLookupSourceLocations() const { return lookupSourceLocations; }
  bool traceUnknownSymbols() const { return traceUnknowns; }

  // setters
  void setAnalysisEnabled(bool enabled) { analysisEnabled = enabled; }

 private:
  std::map<std::string, KNOB<std::string> *> knobMap;

  int argc;
  char **argv;

  std::list<Module *> moduleList;

  std::string executable;
  std::unique_ptr<Routine> pRoutine;
  std::unique_ptr<Variable> pVariable;
  std::unique_ptr<pcv::Thread> pThread;
  std::unique_ptr<LoopDetector> pLoop;
  std::unique_ptr<SymInfo> pSymInfo;
  std::unique_ptr<Writer> pWriter;
  std::unique_ptr<Resolver> pResolver;
  std::unique_ptr<DataModel> pModel;
  std::unique_ptr<Filter> pFilter;

  bool lookupSourceLocations;
  bool analysisEnabled;
  bool traceUnknowns;

  void initializeKnobs();
  void readSymbolsOf(IMG img) const;

  static uint64_t getLinkToLoadTimeOffset(IMG img);
};

}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_PINTOOL_H_
