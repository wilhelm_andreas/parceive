/*! \file FilterRuleCollection.h
 *  \brief This file contains a class used for adding and querying filter rules.
 *
 * Copyright 2017 Siemens Technology and Services
 */
#ifndef SRC_BACKEND_INCLUDE_FILTERRULECOLLECTION_H_
#define SRC_BACKEND_INCLUDE_FILTERRULECOLLECTION_H_

#pragma once

#include <string>
#include <utility>
#include <vector>

#include "Filter.h"

namespace pcv {

class FilterRuleCollection
{
 public:
  void addJsonRule(const Filter::Action &action, const string &rule);
  void addXMLRule(const Filter::Action &action, const string &rule);

  Filter::Action getDynamicFilterAction(const string& pattern) const;
  Filter::Action getLineFilterAction(const string& filePath, const int line) const;
  bool isExcluded(const string& name) const;
  bool isPartial(const string& name) const;

 private:
  typedef pair<Filter::Action, string> rule_t;
  typedef tuple<Filter::Action, string, int> line_rule_t;
  typedef vector<rule_t> rule_list_t;
  typedef vector<line_rule_t> line_rule_list_t;

  void addLineRule(const Filter::Action& action, const string& rule);
  void sortLineRules();

  Filter::Action getStaticFilterAction(const string& name) const;

  rule_list_t staticRules;
  rule_list_t dynamicRules;
  line_rule_list_t lineRules;
  Filter::Action defaultStaticAction = Filter::INCLUDE;

  std::unordered_set<std::string> _includes, _excludes, _partial;
};

}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_FILTERRULECOLLECTION_H_
