/*! \file JsonFilterSystem.h
 *  \brief This file contains an implementation of FilterSystem based on XML data.
 *
 * Copyright 2017 Siemens Technology and Services
 */

#ifndef SRC_BACKEND_INCLUDE_XMLFILTERSYSTEM_H_
#define SRC_BACKEND_INCLUDE_XMLFILTERSYSTEM_H_

#pragma once
#include <string>

#include "FilterSystem.h"

namespace pcv {

class XmlFilterSystem final : public FilterSystem
{
 public:
  XmlFilterSystem(const string &filename);

 private:
  /*! \brief
   * Parses the input filter XML file and adds to the respective struct
   * include_excude variable
   * \return true if parsed successfully else false
   */
  bool readXml(const istream &is /**< [IN] Routine ID*/);
};

}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_XMLFILTERSYSTEM_H_