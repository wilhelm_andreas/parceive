/*! \file FilterInstrumentation.h
 *  \brief This file contains a class that helps to filter using Boost::Regex
 *  expressions
 */

/* Copyright 2014 Siemens Technology and Services*/

#ifndef SRC_BACKEND_INCLUDE_FILTERINSTRUMENTATION_H_
#define SRC_BACKEND_INCLUDE_FILTERINSTRUMENTATION_H_

#pragma once

#include <pin.H>

#include <cstdint>
#include <memory>
#include <string>
#include <unordered_map>
#include <unordered_set>

namespace pcv {

/*!
 * IpInfo describes the id and the name of a routine.
 */
struct RtnInfo {
  uint32_t rtnId;       // the routine id
  std::string rtnName;  // the routine name

  RtnInfo(uint32_t rtnId, const std::string& rtName) : rtnId(rtnId), rtnName(rtName) {}
};

class FilterSystem;

//!  This class helps filtering inputs by validatiing wrt regex inputs given via
//!  XML files in a specified format.
class Filter
{
 public:
  enum Action { PARTIAL, INCLUDE, EXCLUDE, ENABLE, DISABLE, START, STOP, NONE };
  Filter(const std::string& filter);
  ~Filter();

  void precalculateFilterStateFor(const RTN& rtn, const std::string& filename);

  /*! \brief
   * Utilizes the filter to validate the given PIN routine
   * according to the region and the corresponding file path.
   * Returns true if the routine is valid, otherwise false.
   */
  bool isIncluded(const RTN& rtn) const;

  /*! \brief
   * Utilizes the filter to get the inclusion state for the given PIN image.
   * Caches results for inputs internally so subsequent queries are less costly.
   * Returns true if the image is valid, otherwise false.
   */
  bool isIncluded(const IMG& img);

  bool isIncluded(const std::string& filename);

  /*! \brief
   * Gets the filter action associated with the given routine's id.
   * This is either ENABLE, DISABLE or NONE.
   */
  Action getDynamicFilterAction(UINT32 rtnId) const;

  /*! \brief
   * Gets the filter action associated with the given filepath and line.
   * This is either START, STOP or NONE.
   */
  Action getLineFilterAction(const string& filePath, const int line) const;

  /*
   * @brief Sets the given routine as valid or invalid.
   */
  void forceInclusion(const RTN& rtn /**< [IN] The routine to (in)validate.*/);

  void forceInclusion(const IMG& img, const bool valid);

  /*!
   * @returns true if the instruction address is the first of an interesting routine, i.e., belongs
   * to a routine that was seen during image instrumentation.
   */
  bool isRoutineAddressExpected(
    ADDRINT target /**< [IN] The address of the first instruction of a routine.*/) const;

  /*!
   * @brief adds the address of the first instruction of a routine to the interesting ip map.
   */
  void addExpectedRoutineAddress(
    ADDRINT ip /**< [IN] The address of the first instruction of a routine.*/);

  void addPartial(ADDRINT ip);
  bool isPartial(ADDRINT target) const;

  /*!
   * @brief set the ipInfo for the first instruction address of a routine.
   */
  void setRtnInfo(ADDRINT ip, /**< [IN] The instruction address. */
                  std::unique_ptr<RtnInfo> info /**< [IN] The ipInfo pointer. */);

  /*!
   * @brief returns the routine info for the address of the first instruction of a routine.
   */
  const RtnInfo* getRtnInfo(ADDRINT ip /**< [IN] the instruction address. */);

 private:
  static bool isInternal(const std::string& rtnName);

  FilterSystem* filterSys;
  std::unordered_map<UINT32, bool> rtnFilter_map;
  std::unordered_map<UINT32, bool> imgFilter_map;

  std::unordered_map<UINT32, Action> dynamic_filter_map;
  std::unordered_set<ADDRINT> expectedIps_, partialIPs_;

  std::unordered_map<ADDRINT, std::unique_ptr<RtnInfo>> rtnInfos_;
};

}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_FILTERINSTRUMENTATION_H_
