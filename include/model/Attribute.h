/*! \file Attribute.h
 *  \brief This file contains the class for the Attribute entity.
 *
 * Copyright 2015 Siemens Technology and Services
 */
#ifndef SRC_BACKEND_INCLUDE_ATTRIBUTE_H_
#define SRC_BACKEND_INCLUDE_ATTRIBUTE_H_

#include "Entity.h"
namespace pcv {
namespace model {

/*!
 * \brief The Attribute class.
 */
class Attribute : public Entity<Attribute>
{
 public:
  Attribute() : Entity() {}
};

}  // namespace model
}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_ATTRIBUTE_H_
