/*! \file function.h
 *  \brief This file contains the class for the Function entity.
 *
 * Copyright 2015 Siemens Technology and Services
 */
#pragma once

#include "Entity.h"

#include <string>
#include <utility>

namespace pcv {
namespace model {

/*!
 * \brief The Function class.
 */
class Function : public Entity<Function>
{
 public:
  /*!/brief The type of the function */
  enum class Type : int {
    ENTRY_ROUTINE    = (1 << 0),
    EXIT_ROUTINE     = (1 << 1),
    FUNCTION         = (1 << 2),
    METHOD           = (1 << 3),
    ALLOC            = (1 << 4),
    FREE             = (1 << 5),
    FORK             = (1 << 6),
    JOIN             = (1 << 7),
    ACQUIRE          = (1 << 8),
    RELEASE          = (1 << 9),
    EXTERNAL         = (1 << 10),
    BARRIER          = (1 << 11),
    WAIT             = (1 << 12),
    SIGNAL_SINGLE    = (1 << 13),
    SIGNAL_BROADCAST = (1 << 14)
  };

  Function() : Entity<Function>() {}
  explicit Function(Id_t id,
                    std::string name,
                    std::string prototype,
                    Id_t file,
                    int line,
                    Type type,
                    uint32_t symbol,
                    Id_t image)
    : id(id),
      name(std::move(name)),
      prototype(std::move(prototype)),
      file(file),
      line(line),
      type(type),
      symbol(symbol),
      image(image)
  {}

  // members
  Id_t id;
  std::string name;
  std::string prototype;
  Id_t file;
  int line;
  int column;
  Type type;
  uint32_t symbol;
  Id_t image;
};

// allow the '|' operator for the Function Type
inline Function::Type operator|(Function::Type x, Function::Type y)
{
  return static_cast<Function::Type>(static_cast<int>(x) | static_cast<int>(y));
}

}  // namespace model
}  // namespace pcv
