/*! \file loop.h
 *  \brief This file contains the class for the Loop entity.
 *
 * Copyright 2015 Siemens Technology and Services
 */
#ifndef SRC_BACKEND_INCLUDE_LOOP_H_
#define SRC_BACKEND_INCLUDE_LOOP_H_

#include "Entity.h"

namespace pcv {
namespace model {

/*!
 * \brief The Loop class.
 */
class Loop : public Entity<Loop>
{
 public:
  Loop() : Entity<Loop>() {}
  explicit Loop(Id_t id, int line) : Entity<Loop>(), id(id), line(line) {}

  // members
  Id_t id;
  int line;
};

}  // namespace model
}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_LOOP_H_
