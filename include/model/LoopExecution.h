/*! \file loopexecution.h
 *  \brief This file contains the class for the Loop Execution entity.
 *
 * Copyright 2015 Siemens Technology and Services
 */
#ifndef SRC_BACKEND_INCLUDE_LOOPEXECUTION_H_
#define SRC_BACKEND_INCLUDE_LOOPEXECUTION_H_
#include "Entity.h"

namespace pcv {
namespace model {

/*!
 * \brief The LoopExecution class.
 */
class LoopExecution : public Entity<LoopExecution>
{
 public:
  static const int INITVAL = 0;  // initial value

  LoopExecution() : Entity<LoopExecution>() {}
  explicit LoopExecution(Id_t id, Id_t loop, Id_t parentIteration, uint64_t start, uint64_t end)
    : Entity<LoopExecution>(),
      id(id),
      loop(loop),
      parentIteration(parentIteration),
      start(start),
      end(end)
  {}

  // members
  Id_t id;
  Id_t loop;
  Id_t parentIteration;
  uint64_t start;
  uint64_t end;
};

}  // namespace model
}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_LOOPEXECUTION_H_
