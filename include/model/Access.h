/*! \file access.h
 *  \brief This file contains the class for the Access entity.
 *
 * Copyright 2014 Siemens Technology and Services
 */
#ifndef SRC_BACKEND_INCLUDE_ACCESS_H_
#define SRC_BACKEND_INCLUDE_ACCESS_H_

#include "Entity.h"

namespace pcv {
namespace model {

/*! \brief The Access class. */
class Access : public Entity<Access>
{
 public:
  /* /brief! The type of the access */
  enum class Type : int { READ = 1, WRITE = 2, READWRITE = (int)READ | (int)WRITE, ELSE = 0 };

  /*/brief! The state of the access */
  enum class State { INIT = 0, EXCLUSIVE = 1, READ_WRITE = 2, READ_WRITE_SHARED = 3 };

  Access() : Entity<Access>() {}
  explicit Access(Id_t id,
                  Id_t instruction,
                  int position,
                  Id_t reference,
                  Type type,
                  State state,
                  uint64_t offset)
    : Entity<Access>(),
      id(id),
      instruction(instruction),
      position(position),
      reference(reference),
      type(type),
      state(state),
      offset(offset)
  {}

  // member variables
  Id_t id;
  Id_t instruction;
  int position;
  Id_t reference;
  Type type;
  State state;
  uint64_t offset;
};

}  // namespace model
}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_ACCESS_H_
