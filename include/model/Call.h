/*! \file call.h
 *  \brief This file contains the class for the Call entity.
 *
 * Copyright 2014 Siemens Technology and Services
 */
#ifndef SRC_BACKEND_INCLUDE_CALL_H_
#define SRC_BACKEND_INCLUDE_CALL_H_

#include "../../include/Timer.h"
#include "Entity.h"

namespace pcv {
namespace model {

/*!
 * \brief The Call class.
 */
class Call : public Entity<Call>
{
 public:
  static const Id_t NO_CALL   = 0;
  static const Id_t MAIN_CALL = 1;

  Call() : Entity<Call>() {}
  explicit Call(
    Id_t id, Id_t thread, Id_t function, Id_t instruction, R_TIMERTYPE start, R_TIMERTYPE end)
    : Entity<Call>(),
      id(id),
      thread(thread),
      function(function),
      instruction(instruction),
      start(start),
      end(end)
  {}

  // members
  Id_t id;
  Id_t thread;
  Id_t function;
  Id_t instruction;
  R_TIMERTYPE start;
  R_TIMERTYPE end;
};

}  // namespace model
}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_CALL_H_
