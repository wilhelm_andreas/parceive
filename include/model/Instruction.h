/*! \file instruction.h
 *  \brief This file contains the class for the Instruction entity.
 *
 * Copyright 2015 Siemens Technology and Services
 */
#ifndef SRC_BACKEND_INCLUDE_INSTRUCTION_H_
#define SRC_BACKEND_INCLUDE_INSTRUCTION_H_

#include "Entity.h"

namespace pcv {
namespace model {

/*!
 * \brief The Instruction class.
 */
class Instruction : public Entity<Instruction>
{
 public:
  static const int NOCOLUMN = -1;

  /*!/brief The type of the instruction */
  enum class Type {
    CALL    = 0,
    ACCESS  = 1,
    ALLOC   = 2,
    FREE    = 3,
    FORK    = 4,
    JOIN    = 5,
    ACQUIRE = 6,
    RELEASE = 7
  };

  Instruction() : Entity<Instruction>() {}
  explicit Instruction(Id_t id, Id_t segment, Type type, int line, int column)
    : Entity<Instruction>(), id(id), segment(segment), type(type), line(line), column(column)
  {}

  // members
  Id_t id;
  Id_t segment;
  Type type;
  int line;
  int column;
};

}  // namespace model
}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_INSTRUCTION_H_
