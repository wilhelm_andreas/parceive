/*! \file Image.h
 *  \brief This file contains the class for the Image entity.
 *
 * Copyright 2015 Siemens Technology and Services
 */
#ifndef SRC_BACKEND_INCLUDE_IMAGE_H_
#define SRC_BACKEND_INCLUDE_IMAGE_H_

#include "Entity.h"

#include <string>
#include <utility>

namespace pcv {
namespace model {

/*!
 * \brief The Image class.
 */
class Image : public Entity<Image>
{
 public:
  Image() : Entity<Image>() {}
  explicit Image(Id_t id, std::string name) : id(id), name(std::move(name)) {}

  // members
  Id_t id;
  std::string name;
};

}  // namespace model
}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_IMAGE_H_
