//
// Created by wilhelma on 11/23/17.
//

#pragma once

#include <memory>
#include <unordered_map>
#include <vector>

#include "entities/Class.h"
#include "entities/File.h"
#include "entities/Image.h"
#include "entities/Namespace.h"
#include "entities/Routine.h"
#include "entities/Variable.h"

namespace pcv {

/*
 * @brief Context that provides an interface to the software architecture reconstruction component.
 * Will be filled by the symbol information handler.
 */
class Context
{
 public:
  std::vector<std::unique_ptr<entity::Image>> images;
  std::vector<std::unique_ptr<entity::Namespace>> namespaces;
  std::vector<std::unique_ptr<entity::Routine>> routines;
  std::vector<std::unique_ptr<entity::Class>> classes;
  std::vector<std::unique_ptr<entity::Variable>> variables;
  std::vector<std::unique_ptr<entity::File>> files;

  const entity::Class* getClass(const entity::ImageScopedId& id) const;
  entity::ImageScopedId getClassId(Id_t imageId, const std::string& fullRtnName) const;
  const entity::Routine* getRoutine(Id_t imageId, const std::string& fullRtnName) const;
  std::unique_ptr<std::vector<const entity::Variable*>> getGlobalVariables(Id_t imageId) const;
  std::unique_ptr<std::vector<const entity::Variable*>> getStaticVariables(Id_t imageId) const;
  const entity::Variable* getMemberVariable(const entity::ImageScopedId& classId,
                                            entity::Class::offset_t offset) const;

  void prepareForNewImage(Id_t imageId);
  void addImage(entity::Image* img);
  void addNamespace(entity::Namespace* ns);
  void addRoutine(entity::Routine* rtn);
  void addClass(entity::Class* cls);
  void addVariable(entity::Variable* var);

 private:
  typedef std::unordered_map<std::string, const entity::Routine*> RoutineLookupByName;
  typedef std::unordered_map<Id_t, const entity::Variable*> VariableLookupById;
  typedef std::unordered_map<Id_t, const entity::Class*> ClassLookupById;

  std::vector<RoutineLookupByName> routinesPerImage;
  std::vector<VariableLookupById> globalVariablesPerImage;
  std::vector<VariableLookupById> staticVariablesPerImage;
  std::vector<ClassLookupById> classesPerImage;
};

}  // namespace pcv
