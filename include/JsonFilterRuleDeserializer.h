/*! \file JsonFilterRuleDeserializer.h
 *  \brief This file contains the class for the Function entity.
 *
 * Copyright 2017 Siemens Technology and Services
 */
#ifndef SRC_BACKEND_INCLUDE_JSONFILTERRULEDESERIALIZER_H_
#define SRC_BACKEND_INCLUDE_JSONFILTERRULEDESERIALIZER_H_

#pragma once

#include <string>
#include <vector>

#include "Filter.h"

namespace pcv {

enum class JsonFilterScope : int {
  NONE      = 0,
  CLASS     = 1,
  ROUTINE   = 2,
  VARIABLE  = 4,
  NAMESPACE = 8,
  IMAGE     = 16
};

struct JsonFilterData {
  JsonFilterScope scope;
  std::string pattern;
  std::string nameSpace;
};

struct JsonFilterRule {
  Filter::Action action;
  std::vector<JsonFilterData> entries;
};

typedef std::vector<JsonFilterRule> JsonFilterRules;

JsonFilterRules deserializeJsonFilterRules(const std::string& filterRuleFilepath);

}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_JSONFILTERRULEDESERIALIZER_H_
