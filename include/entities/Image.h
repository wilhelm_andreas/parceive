//
// Created by wilhelma on 12/22/16.
//

#pragma once

#include <string>
#include <vector>

#include "SoftwareEntity.h"

namespace pcv {
namespace entity {

struct Image : public SoftwareEntity {
  std::vector<const SoftwareEntity *> entities;
  std::vector<const SoftwareEntity *> files;

  explicit Image(Id_t id, std::string name)
    : SoftwareEntity(id, std::move(name), nullptr, nullptr, nullptr, nullptr, 0)
  {
    img       = this;
    full_name = name;
  }
  Image() = default;

  EntityType getEntityType() const override { return EntityType::Image; };
};

}  // namespace entity
}  // namespace pcv
