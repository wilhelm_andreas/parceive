//
// Created by wilhelma on 12/22/16.
//

#pragma once

#include <string>
#include <vector>

#include "SoftwareEntity.h"

namespace pcv {
namespace entity {

struct Namespace : public SoftwareEntity {
  Namespace *parent{};
  std::vector<const Namespace *> children;
  std::vector<const SoftwareEntity *> entities;

  Namespace(
    Id_t id, name_t name, Image *img, Namespace *nmsp, File *file, line_t line, Namespace *parent)
    : SoftwareEntity(id, name, img, nmsp, nullptr, file, line), parent(parent)
  {}

  explicit Namespace(const std::string &name, Namespace *parent, Image *img)
    : SoftwareEntity(0, name, img, nullptr, nullptr, nullptr, 0), parent(parent)
  {
    static int nextId = 1;
    id                = nextId++;
  }
  Namespace() = default;

  EntityType getEntityType() const override { return EntityType::Namespace; }

  std::string getFullName()
  {
    if (full_name.empty()) {
      full_name = getFullName(this);
    }
    return full_name;
  }

 private:
  static std::string getFullName(const Namespace *ns)
  {
    std::string full = ns->name;
    if (ns->parent != nullptr && !ns->parent->name.empty()) {
      full = getFullName(static_cast<const Namespace *>(ns->parent)) + "::" + full;
    }
    return full;
  }
};

}  // namespace entity
}  // namespace pcv
