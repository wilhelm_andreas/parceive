//
// Created by wilhelma on 12/22/16.
//

#pragma once

#include <string>
#include <vector>

#include "Image.h"
#include "SoftwareEntity.h"

namespace pcv {
namespace entity {

struct File : public SoftwareEntity {
  std::vector<const SoftwareEntity *> entities;

  explicit File(std::string name, Image *img)
    : SoftwareEntity(0, std::move(name), img, nullptr, nullptr, nullptr, 0)
  {
    static Id_t idCount{0};

    id = idCount++;
    if (img) img->files.push_back(this);
  }

  File() = default;

  EntityType getEntityType() const override { return EntityType::File; };
};

}  // namespace entity
}  // namespace pcv
