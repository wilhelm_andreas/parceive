#pragma once
#include "Common.h"

namespace pcv {
namespace entity {

using Id_t              = ::pcv::Id_t;
static const Id_t NO_ID = UINT64_MAX;

struct ImageScopedId {
  Id_t imageId;
  Id_t id;

  ImageScopedId(const Id_t imageId, const Id_t classId) : imageId(imageId), id(classId) {}
  ImageScopedId() : imageId(NO_ID), id(NO_ID) {}

  bool operator<(const ImageScopedId& rhs) const { return imageId < rhs.imageId || id < rhs.id; }

  bool operator==(const ImageScopedId& rhs) const { return imageId == rhs.imageId && id == rhs.id; }

  bool operator!=(const ImageScopedId& rhs) const { return !operator==(rhs); }

  bool isValid() const { return id != NO_ID && imageId != NO_ID; }

  static ImageScopedId getNone();
};

inline ImageScopedId ImageScopedId::getNone()
{
  static ImageScopedId none{NO_ID, NO_ID};
  return none;
}

}  // namespace entity
}  // namespace pcv

// hash template specialization for ImageScopedId so we can use it
// with containers that depend on std::hash by default
namespace std {
template <>
struct hash<pcv::entity::ImageScopedId> {
  std::size_t operator()(const pcv::entity::ImageScopedId& rhs) const
  {
    return std::hash<pcv::Id_t>()(rhs.imageId) ^ (std::hash<pcv::Id_t>()(rhs.id) << 1);
  }
};
}  // namespace std
