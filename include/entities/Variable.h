//
// Created by wilhelma on 12/22/16.
//

#pragma once

#include "Class.h"
#include "Routine.h"
#include "SoftwareEntity.h"

#include <string>

namespace pcv {
namespace entity {

struct Variable : public SoftwareEntity {
  enum class Type : uint8_t {
    STACK     = 1,
    HEAP      = 2,
    STATICVAR = 4,
    GLOBAL    = 8,
    UNKNOWN   = 16,
  };

  offset_t offset{};
  Type type{};
  Class* declaringClass{};
  Routine* declaringRoutine{};

  explicit Variable(const Id_t id,
                    const std::string& name,
                    const std::string& full_name,
                    Image* img,
                    Namespace* nmsp,
                    Class* cls,
                    File* file,
                    const int line,
                    const size_t size,
                    const offset_t offset,
                    const Type type,
                    Class* classType = nullptr)
    : SoftwareEntity(id, name, img, nmsp, cls, file, line, full_name),
      offset(offset),
      type(type),
      declaringClass(classType)
  {
    this->size = size;
  }
  Variable() = default;

  EntityType getEntityType() const override { return EntityType::Variable; }

  std::string getFullName()
  {
    if (full_name.empty()) {
      if (declaringClass) {
        full_name = declaringClass->getFullName();
      } else if (declaringRoutine) {
        full_name = declaringRoutine->getFullName();
      }
      if (!full_name.empty()) {
        full_name += "::";
      }
      full_name += name;
    }
    return full_name;
  }
};

inline Variable::Type operator|(Variable::Type lhs, Variable::Type rhs)
{
  return Variable::Type(uint8_t(lhs) | uint8_t(rhs));
}
inline Variable::Type& operator|=(Variable::Type& lhs, Variable::Type rhs)
{
  lhs = Variable::Type(uint8_t(lhs) | uint8_t(rhs));
  return lhs;
}
inline Variable::Type operator&(Variable::Type lhs, Variable::Type rhs)
{
  return Variable::Type(uint8_t(lhs) & uint8_t(rhs));
}
inline Variable::Type& operator&=(Variable::Type& lhs, Variable::Type rhs)
{
  lhs = Variable::Type(uint8_t(lhs) & uint8_t(rhs));
  return lhs;
}

}  // namespace entity
}  // namespace pcv
