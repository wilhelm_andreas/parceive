//
// Created by wilhelma on 12/22/16.
//

#pragma once

#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include "Class.h"
#include "Namespace.h"
#include "SoftwareEntity.h"

namespace pcv {
namespace entity {

struct Variable;

struct Routine : public SoftwareEntity {
  bool isConstructor{};
  std::vector<Variable *> locals;
  Symbol_t symbol;

  explicit Routine(const Id_t id,
                   const std::string name,
                   Image *img,
                   Namespace *nmsp,
                   Class *cls,
                   File *file,
                   const int line,
                   Symbol_t symbol,
                   const bool isConstructor = false)
    : SoftwareEntity(id, name, img, nmsp, cls, file, line),
      isConstructor(isConstructor),
      symbol(symbol)
  {}
  Routine() = default;
  ~Routine() = default;

  EntityType getEntityType() const override { return EntityType::Routine; }

  std::string getFullName()
  {
    if (full_name.empty()) {
      full_name = nmsp == nullptr ? "" : nmsp->getFullName();
      if (!full_name.empty()) { full_name += "::"; }
      if (cls) {
        const auto nestingClassScopes = cls->getNestingClassScopes();
        if (!nestingClassScopes.empty()) { full_name += nestingClassScopes + "::"; }
        full_name += cls->name + "::";
      }
      full_name += name;
    }
    return full_name;
  }
};

}  // namespace entity
}  // namespace pcv
