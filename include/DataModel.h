/*! \file ParceiveQuery.h
 *  \brief This file contains ParceiveQuery class which extends the Query class,
 *  and provides implementation for writing into the backend provided
 *  by the Data Model
 */

/* Copyright 2014 Siemens Technology and Services*/

#ifndef SRC_BACKEND_INCLUDE_PARCEIVEQUERY_H_
#define SRC_BACKEND_INCLUDE_PARCEIVEQUERY_H_

#include <atomic>
#include <map>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

#include "pin.H"

#include "model/Access.h"
#include "model/Attribute.h"
#include "model/Call.h"
#include "model/File.h"
#include "model/Function.h"
#include "model/Image.h"
#include "model/Instruction.h"
#include "model/Loop.h"
#include "model/LoopExecution.h"
#include "model/LoopIteration.h"
#include "model/Member.h"
#include "model/Reference.h"
#include "model/Segment.h"
#include "model/Thread.h"

#include <unordered_set>
#include "Module.h"

namespace pcv {

class Parceive;
class Writer;
class Thread;

/*! \brief ParceiveQuery class extends the Query class. It provides the
 * write() based on the Data Model.
 */

/*
For each of the process() functions.It calls the appropriate write() to
write the logs. Make sure that each of the process() functions creates a
vector of ENTRY objects which is passed to the appropriate write() to be
written to the logs
*/
class DataModel : public Module
{
 public:
  explicit DataModel(Parceive *parceive);
  ~DataModel() override;

  void initialize() override;
  void finalize() override;

  bool processCall(model::Call::Id_t callID,
                   model::Instruction::Id_t insID,
                   model::Thread::Id_t threadID,
                   model::Function::Id_t functionID,
                   unsigned long long startTime,
                   unsigned long long endTime);
  bool processCallEntry(model::Call::Id_t callerID,
                        model::Call::Id_t calleeID,
                        model::Thread::Id_t threadID,
                        model::LoopIteration::Id_t iterationId = model::LoopIteration::NULLVAL);
  bool processCallReEntry(model::Call::Id_t callID, model::Thread::Id_t threadID);
  bool processLoopEntry(model::Thread::Id_t threadID,
                        model::Loop::Id_t loopID,
                        unsigned int line,
                        model::Call::Id_t call);
  bool processLoopExit(model::Thread::Id_t threadID,
                       model::Loop::Id_t loopID,
                       model::Call::Id_t call);
  bool processInstruction(model::Thread::Id_t threadID,
                          model::Call::Id_t callID,
                          model::Instruction::Type instrType,
                          int lineNo,
                          model::Instruction::Id_t *instructionID);
  bool processReference(model::Reference::Id_t referenceID,
                        model::Thread::Id_t threadID,
                        unsigned int size,
                        model::Reference::Type memoryType,
                        const char *name,
                        model::Instruction::Id_t allocIns,
                        unsigned int symbolId,
                        model::Image::Id_t imageId);
  bool processVarAccess(model::Thread::Id_t threadID,
                        model::Instruction::Id_t instructionID,
                        unsigned int pos,
                        model::Reference::Id_t referenceID,
                        model::Access::Type accessType,
                        model::Access::State memState,
                        uint64_t offset = 0);
  bool processThread(model::Thread::Id_t threadId,
                     unsigned long long start,
                     unsigned long long end,
                     model::Instruction::Id_t createInstruction,
                     model::Instruction::Id_t joinInstruction,
                     model::Thread::Id_t parentThreadId,
                     model::Call::Id_t call);
  bool processImage(unsigned int imageId, const std::string &imageName);
  bool processRoutine(unsigned int imageId,
                      const std::string &routineName,
                      const std::string &prototype,
                      const std::string &file,
                      model::Function::Type functionType,
                      unsigned int lineNo,
                      unsigned int routineID,
                      unsigned int symbolId);
  bool processFile(const std::string &file, unsigned int imageID, unsigned int *fileID);
  bool processFile(model::Thread::Id_t threadID, char **file, unsigned int *fileID);

 private:
  Writer *pWriter;
  /*! Specifies the count of the number of files */
  std::atomic<unsigned int> _filesCount;
  /*! Specifies a <filename,file> map. This prevents, entering multiple entries
   * into file table, for the same file name */
  std::unordered_map<std::string, unsigned int> _fileNameIdMap;
  std::unordered_set<model::Reference::Id_t> _processedReferenceIds;
};

}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_PARCEIVEQUERY_H_
