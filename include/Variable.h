/*! \file PinToolVariableAnalyzer.h
 *  \brief This file contains the class which extends the Variable Analyzer
 * class, for PinTool
 */

/* Copyright 2014 Siemens Technology and Services*/

#ifndef SRC_BACKEND_INCLUDE_PINTOOLVARIABLEANALYZER_H_
#define SRC_BACKEND_INCLUDE_PINTOOLVARIABLEANALYZER_H_

#pragma once

#include <Filter.h>
#include <string>
#include <unordered_map>
#include <unordered_set>

#include "DebugOptions.h"
#include "Module.h"
#include "model/Reference.h"
#include "pin.H"

namespace pcv {

class Routine;
class Thread;
class SymInfo;
class DataModel;
class Resolver;

using std::string;

//!    The Variable module provides the functionality for analyzing reads and
//!    writes to memory locations, when dynamic memory is allocated and freed

class Variable : public Module
{
 public:
  Variable(Parceive *parceive);
  ~Variable() override;

  void initialize() override;
  void finalize() override;

  void beforeInstrumentation() override;
  void instrumentInstruction(IMG img, RTN rtn, INS ins) override;

  void addAllocReturnAddress(S_ADDRUINT address);

 private:
  const bool _traceOffsets;
  std::unordered_set<S_ADDRUINT> allocReturns_;

  void resolveVariableInstrumentationTime(unsigned int currentRtnCount,
                                          INS ins,
                                          int operand,
                                          UINT32 mem_op,
                                          unsigned int imageId,
                                          void **location,
                                          model::Reference::Type *type);

  static void cb_instrumentImage(IMG img, VOID *v);
  static void cb_instrumentTrace(TRACE trace, VOID *v);

  /*! \brief
   *   Friend function which calls the analysis routine when memory is accessed.
   */
  static void callCaptureMemoryAccess(Variable *pVariable,
                                      /**< [IN] Pintool Variable Analyzer Object*/
                                      THREADID threadId,    /**< [IN] Thread ID*/
                                      ADDRINT address,      /**< [IN] Effective access address*/
                                      S_ADDRUINT imageId,   /**< [IN] Image ID*/
                                      S_ADDRUINT routineNo, /**< [IN] Routine ID*/
                                      S_ADDRUINT pos,       /**< [IN] Memory operand location*/
                                      S_ADDRUINT lineNo,    /**< [IN] Line No. causing the access*/
                                      void *location, /**< [IN] Location address of the variable*/
                                      UINT32 refType, /**< [IN] Type of the variable*/
                                      UINT32 accessType, /**< [IN] Type of the access */
                                      ADDRINT ebp,
                                      ADDRINT ip);

  /*! \brief
   *	  Records  allocation data for processing when the allocation is done.
   */
  static void callDynamicMemoryAllocationBefore(
    Variable *pinToolVariableAnalyzerObject, /**< [IN] Pintool Variable Analyzer */
    THREADID threadID,                       /**< [IN] Thread ID*/
    unsigned int requestedSize,              /**< [IN] requested size*/
    unsigned int numberOfBlocks,             /**< [IN] number of blocks */
    S_ADDRUINT reAllocatedAddress,           /**< [IN] realloc address */
    S_ADDRINT returnIP /**< [in] return address of allocation */);

  /*! \brief
   * Processes allocation data recorded beforehand.
   */
  static void callDynamicMemoryAllocationAfter(
    Variable *pinToolVariableAnalyzerObject, /**< [IN] Pintool Variable Analyzer */
    THREADID threadID,                       /**< [IN] Thread ID*/
    S_ADDRUINT allocatedAddress /**< [IN] allocated address*/);

  /*! \brief
   * Friend function which calls the analysis routine when a memory which
   * is dynamically allocated, is freed. To avoid an extra jump, the analysis
   * routine, dynamicMemoryFreeBefore's code, is placed in the friend function
   * itself
   */
  static void callDynamicMemoryFreeBefore(
    Variable *pinToolVariableAnalyzerObject, /**< [IN] pintool variable analyzer */
    S_ADDRUINT allocatedAddress /**< [IN] Dynamically allocated address*/);

  // Pointer to the Query object for logging data
  DataModel *pModel;
  // Pointer to ThreadAnalyze object
  Thread *pThread;

  Resolver *pResolver;

  /*! This map stores the mapping between return instruction address and the
  corresponding line number, fileID and isValid parameter which is used to
  decide whether we will register dynamic memory allocations for this return
  instruction address */
  unordered_map<S_ADDRUINT, struct returnIPinfo *> returnIP_file_line_map;
};

}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_PINTOOLVARIABLEANALYZER_H_
