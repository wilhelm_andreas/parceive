/*! \file FilterSystem.h
 *  \brief This file contains a base class used for filtering certain aspects of processed data.
 *
 * Copyright 2017 Siemens Technology and Services
 */

#ifndef SRC_BACKEND_INCLUDE_FILTERSYSTEM_H_
#define SRC_BACKEND_INCLUDE_FILTERSYSTEM_H_

#pragma once

#include <string>

#include "Filter.h"
#include "FilterRuleCollection.h"

namespace pcv {

class FilterSystem
{
 public:
  virtual ~FilterSystem() = default;
  virtual bool isImageIncluded(const std::string& imgName) const;
  virtual bool isRoutineExcluded(const std::string& routineName) const;
  virtual bool isRoutinePartial(const std::string& routineName) const;
  virtual bool isFileExcluded(const std::string& fileName) const;
  /*! \brief
   * Gets a dynamic filter action for the given routine name.
   * \return returns either ENABLE, DISABLE or NONE
   */
  virtual Filter::Action getDynamicFilterAction(const std::string& routineName) const;
  /*! \brief
   * Gets a line filter action for the given routine name.
   * \return   // returns either START, STOP or NONE
   */
  virtual Filter::Action getLineFilterAction(const std::string& filePath, const int line) const;

 protected:
  FilterRuleCollection regions, images, files;
};

}  // namespace pcv

#endif  // SRC_BACKEND_INCLUDE_FILTERSYSTEM_H_
