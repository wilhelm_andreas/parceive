/*
 * Copyright (c) 2016, Siemens AG. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef VARIABLERESOLVER_H_
#define VARIABLERESOLVER_H_

#include <pin.H>
#include <map>
#include <memory>
#include <mutex>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <utility>

#include "../include/entities/ImageScopedId.h"
#include "DataModel.h"
#include "Module.h"
#include "ShadowReference.h"
#include "SymInfo.h"
#include "Timer.h"

namespace pcv {

class Parceive;

/**
 * Manages references of type stack and heap.
 */
class Resolver : public Module
{
 public:
  explicit Resolver(Parceive* parceive, SymInfo* symInfo);
  ~Resolver() override;

  /* disable copy/move construction and assignment operators */
  Resolver(const Resolver&) = delete;
  Resolver(Resolver&&)      = delete;
  Resolver& operator=(const Resolver&) = delete;
  Resolver& operator=(Resolver&&) = delete;

  void initialize() override;
  void finalize() override;

  void instrumentRoutine(IMG img, RTN rtn) override;

  /*!
   * @brief Uses the debug interface to fill global symbol information.
   * @note Will be called immedieately after debug information has been collected.
   */
  void fillGlobalSymbolInfo(Id_t imgId);

  /*!
   * @brief Uses the debug interface to fill static symbol information.
   * @note Will be called immedieately after debug information has been collected.
   */
  void fillStaticSymbolInfo(Id_t imgId);

  /*!
   * @brief Adds symbol information for the stack variable at a routine/offset.
   */
  void addStackSymbolInfo(UINT32 rtnId,   /**< [IN] The routine id  */
                          ADDRINT offset, /**< [IN] The offset of the variable */
                          unique_ptr<SymbolInfo> info /**< [IN] The symbol information */);

  /*!
   * @brief Adds symbol information for the allocated variable at an address.
   */
  void addAllocatedSymbolInfo(ADDRINT address, /**< [IN] The memory address */
                              unique_ptr<SymbolInfo> info /**< [IN] The symbol information */);

  /**
   * Adds symbol information for the global variable at an address.
   */
  void addGlobalSymbolInfo(ADDRINT address,            /**< [IN] The memory address */
                           unique_ptr<SymbolInfo> info /**< [IN] The symbol information */
  );

  /**
   * Adds a reference to the shadow stack map.
   *
   * \returns Stack reference is newly added.
   */
  bool addStackReference(ADDRINT address,          /**< [IN] The memory address. */
                         SymbolInfo* info,         /**< [IN] The previously created symbol info. */
                         model::Call::Id_t callId, /**< [IN] The call id for the stack reference. */
                         Reference_t** reference   /**< [OUT] The assigned reference. */
  );

  /**
   * Adds a reference to the shadow map for global memory.
   *
   * \returns Reference is newly added.
   */
  bool addGlobalReference(S_ADDRUINT address,     /**< [IN] The memory address. */
                          SymbolInfo* info,       /**< [IN] The new symbol info. */
                          Reference_t** reference /**< [OUT] The created reference. */
  );

  /**
   * Adds a reference to the shadow map for allocated memory.
   *
   * \returns Reference is newly added.
   */
  bool addAllocatedReference(S_ADDRUINT address,          /**< [IN] The memory address. */
                             unique_ptr<SymbolInfo> info, /**< [IN] The new symbol info. */
                             S_ADDRUINT allocationIP,     /**< [IN] The allocation instruction. */
                             Reference_t** reference      /**< [OUT] The created reference. */
  );

  /**
   * Returns the symbol information for a stack variable.
   *
   * \returns A pointer to the symbol information.
   */
  SymbolInfo* getStackSymbolInfo(UINT32 rtnId,    /**< [IN] The routine id. */
                                 S_ADDRINT offset /**< [IN] The stack offset. */
  );

  /**
   * Returns the symbol information for a stack variable.
   *
   * \returns A pointer to the symbol information.
   */
  SymbolInfo* getGlobalSymbolInfo(
    S_ADDRUINT address,      /**< [IN] The address of the reference. */
    S_ADDRUINT* startAddress /**< [OUT] The start address of the reference */
  );

  /**
   * Returns the reference (stack or allocated) for a given address.
   *
   * \returns True if a reference exists at the given address.
   */
  bool getReference(ADDRINT address,  /**< [IN] The address of the reference. */
                    Reference_t** ref /**< [OUT] The reference. */
  );

  /**
   * @returns a pointer to the reference that abstracts all unresolved variables.
   */
  Reference_t* getUnknownReference() const;

  void assignReferenceToClass(S_ADDRUINT address,
                              S_ADDRUINT referenceId,
                              const entity::ImageScopedId& classId);
  void removeStackReferences(model::Call::Id_t callId);
  void refineReference(S_ADDRUINT address,
                       const Reference_t& reference,
                       model::Reference::Id_t* referenceId,
                       S_ADDRUINT* variableSize,
                       std::string* varName,
                       uint32_t* symbol,
                       model::Image::Id_t* imageId);
  void removeAllocatedReference(S_ADDRUINT address);

  /*!
   * @brief Match the routine id from instrumentation to the symbol information.
   * @returns the symbol id of the routine.
   */
  virtual pcv::entity::Id_t matchRtn(const unsigned imageId, /**< [IN] Image ID*/
                                     const unsigned rtnId,   /**< [IN] Routine ID*/
                                     const std::string& rtnName /**< [IN] Routine Name*/);

  /*!
   * @brief Refine the routine name accordingly to the symbol information rules.
   * @return The refined routine name.
   */
  virtual string refineRtnName(const unsigned imageId, /**< [IN] Image ID*/
                               const std::string& rtnName /**< [IN] Routine Name */);

  /*!
   * @brief Returns the class id of the constructor with the given name.
   * @return The class id.
   */
  virtual entity::ImageScopedId getUniqueClassId(
    uint32_t imgId, /**< [IN] Image ID.*/
    const std::string& rtnName /**< [IN] The constructor name.*/) const;

  // todo: add descriptions
  model::Reference::Id_t getRefId(ADDRINT address);

  typedef struct classStruct_t {
    S_ADDRUINT address;
    entity::ImageScopedId classId;
    explicit classStruct_t(S_ADDRUINT address, const entity::ImageScopedId classId)
      : address(address), classId(std::move(classId))
    {}
  } classStruct_t;

  void addAccessInCall(ADDRINT address);
  bool accessInCallExists(ADDRINT address);
  void resetAccessesInCalls();

 private:
  SymInfo* pSymInfo;
  DataModel* pModel;
  PIN_RWMUTEX _stackLock, _allocLock, _globalLock, _refClassMapLock;
  const bool _singleStackReferences;

  using offsetSymbols_t = std::map<S_ADDRINT, std::unique_ptr<SymbolInfo>>;
  std::unordered_map<model::Function::Id_t, offsetSymbols_t> _stackSymbols;
  std::map<ADDRINT, unique_ptr<SymbolInfo>> _globalSymbols;
  std::unordered_map<ADDRINT, unique_ptr<SymbolInfo>> _allocSymbols;
  std::unordered_map<model::Call::Id_t, std::set<ADDRINT>> _addressMap;

  std::map<ADDRINT, unique_ptr<StackReference_t>> _stackReferences;
  std::map<ADDRINT, unique_ptr<GlobalReference_t>> _globalReferences;
  std::map<ADDRINT, unique_ptr<AllocatedReference_t>> _allocReferences;

  std::map<ADDRINT, model::Reference::Id_t> _addrRefMap;
  using classStructVec_t = std::vector<classStruct_t>;
  std::unordered_map<model::Reference::Id_t, classStructVec_t> _refClassVecMap;

  bool getStackReference(ADDRINT address, StackReference_t** ref);
  bool getGlobalReference(ADDRINT address, GlobalReference_t** ref);
  bool getAllocatedReference(ADDRINT address, AllocatedReference_t** ref);

  std::unique_ptr<SymbolInfo> _unknownSymbol;
  std::unique_ptr<Reference_t> _unknownReference;

  unordered_set<ADDRINT> accessesInCall_;
};

}  // namespace pcv

#endif  // VARIABLERESOLVER_H
