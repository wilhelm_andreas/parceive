R"====(CREATE TABLE IF NOT EXISTS SoftwareEntity (
  Id INTEGER NOT NULL,
  ImageId INTEGER NOT NULL,
  EntityType INTEGER NOT NULL,
  NamespaceId INTEGER,
  ClassId INTEGER,
  Name VARCHAR,
  Size INTEGER,
  FilePath VARCHAR,
  Line INTEGER,
  PRIMARY KEY(Id, ImageId, EntityType)
);

CREATE TABLE IF NOT EXISTS ClassRelationships (
  FirstClassId INTEGER NOT NULL,
  SecondClassId INTEGER NOT NULL,
  ImageId INTEGER NOT NULL,
  RelationshipType INTEGER,
  PRIMARY KEY(FirstClassId, SecondClassId, ImageId)
);

CREATE TABLE IF NOT EXISTS Namespace (
  EntityId INTEGER NOT NULL,
  ImageId INTEGER NOT NULL,
  ParentId INTEGER,
  FOREIGN KEY(EntityId) REFERENCES SoftwareEntity(Id),
  FOREIGN KEY(ImageId) REFERENCES SoftwareEntity(Id),
  PRIMARY KEY(EntityId, ImageId)
);

CREATE TABLE IF NOT EXISTS Routine (
  EntityId INTEGER NOT NULL,
  ImageId INTEGER NOT NULL,
  IsConstructor INTEGER,
  FOREIGN KEY(EntityId) REFERENCES SoftwareEntity(Id),
  FOREIGN KEY(ImageId) REFERENCES SoftwareEntity(Id),
  PRIMARY KEY(EntityId, ImageId)
);

CREATE TABLE IF NOT EXISTS Variable (
  EntityId INTEGER NOT NULL,
  ImageId INTEGER NOT NULL,
  DeclaringClassId INTEGER,
  DeclaringRoutineId INTEGER,
  Offset INTEGER,
  Type INTEGER,
  FOREIGN KEY(EntityId) REFERENCES SoftwareEntity(Id),
  PRIMARY KEY(EntityId, ImageId)
);
)===="