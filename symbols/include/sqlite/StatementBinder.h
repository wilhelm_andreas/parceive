#pragma once

#include "Statement.h"

namespace pcv {
namespace symbols {
namespace sqlite {

class StatementBinder
{
 public:
  StatementBinder(const Statement &stmt) : bindingIdx(1), stmt(stmt) {}

  template <typename T>
  StatementBinder &operator<<(const T &obj)
  {
    stmt.bind(bindingIdx++, obj);
    return *this;
  }

 private:
  int bindingIdx;
  const Statement &stmt;
};

template <>
inline StatementBinder &StatementBinder::operator<<<NullClass>(const NullClass &)
{
  stmt.bindNULL(bindingIdx++);
  return *this;
}

template <typename T>
StatementBinder operator<<(Statement &stmt, const T &obj)
{
  StatementBinder binder(stmt);
  binder << obj;
  return binder;
}

}  // namespace sqlite
}  // namespace symbols
}  // namespace pcv
