#pragma once

namespace pcv {
namespace symbols {
namespace sqlite {

enum class ClassRelationshipType { None, Base, Inherited, Nested, Composite };
}
}  // namespace symbols
}  // namespace pcv