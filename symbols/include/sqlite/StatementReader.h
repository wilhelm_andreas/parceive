#pragma once

#include "Statement.h"

namespace pcv {
namespace symbols {
namespace sqlite {

class StatementReader
{
 public:
  StatementReader(const Statement &stmt) : column(0), stmt(stmt) {}

  template <typename T>
  StatementReader &operator>>(T &obj)
  {
    stmt.getColumn(obj, column++);
    return *this;
  }

 private:
  int column;
  const Statement &stmt;
};

template <typename T>
StatementReader operator>>(const Statement &stmt, T &obj)
{
  StatementReader reader(stmt);
  reader >> obj;
  return reader;
}

}  // namespace sqlite
}  // namespace symbols
}  // namespace pcv
