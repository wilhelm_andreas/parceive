#pragma once

#include "sqlite/Statement.h"

struct sqlite3;

namespace pcv {
namespace symbols {
namespace sqlite {

class Connection final
{
 public:
  Connection();
  ~Connection();

  bool open(const char* const dbFilePath, bool forceCreate = false);
  bool isOpen() const;
  void close();

  bool execute(const char* const sql) const;

  bool createStatement(Statement& stmt, const char* const sql) const;
  bool createStatement(Statement& stmt, const std::string sql) const;

  Statement createStatement(const char* const sql) const;
  Statement createStatement(const std::string sql) const;

  void createBeginTransationStatement(Statement& stmt) const;
  void createCommitTransactionStatement(Statement& stmt) const;

 private:
  sqlite3_stmt* createSqlStatement(const char* const sql) const;

  void ensureIsClosed()
  {
    if (isOpen()) {
      close();
    }
  }

  sqlite3* db;
};

}  // namespace sqlite
}  // namespace symbols
}  // namespace pcv
