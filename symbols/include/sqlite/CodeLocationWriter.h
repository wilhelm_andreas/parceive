#pragma once

#include <array>
#include <string>

#include "Common.h"
#include "DbgCollector.h"
#include "sqlite/Connection.h"
#include "sqlite/Statement.h"

namespace pcv {
namespace symbols {
namespace sqlite {

class Connection;

class CodeLocationWriter final
{
 public:
  void insertCodeLocations(const std::string& dbFilePath,
                           const pcv::Id_t imageId,
                           const dbghelp::AddressToCodeLocationMap& locationMap,
                           bool clearPreexistingData = false);

 private:
  bool writeData(const pcv::Id_t imageId, const dbghelp::AddressToCodeLocationMap& locationMap);

  bool createDatabase() const;
  bool prepareStatements();
  void finalizeAllStatements();
  bool areAllStatementsValid();

  std::array<Statement*, 3> getAllStatements()
  {
    return {&beginTransaction, &commitTransaction, &insertCodeLocation};
  }

  // order: connection variable is stated first so it
  // will be destructed after the statemens
  Connection conn;
  Statement beginTransaction;
  Statement commitTransaction;
  Statement insertCodeLocation;
};

}  // namespace sqlite
}  // namespace symbols
}  // namespace pcv
