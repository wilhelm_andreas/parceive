#pragma once
#include "Loguru.h"

// if defined any usage of the error log will try to break into the debugger
//#define BREAK_AT_ERRORS

namespace pcv {
namespace symbols {
enum class LogMode {
  DETAILED,
  SIMPLE,
};

void initLogging(int argc, char** argv);
void setLogMode(const LogMode& mode);
void setVerbosity(const int level);
}  // namespace symbols
}  // namespace pcv
void debugBreak();

#undef ERROR

#ifdef USE_LOGGING

#define INFO_SCOPE(fmt, ...) VLOG_SCOPE_F(0, fmt, __VA_ARGS__)
#define VERBOSE_SCOPE(level, fmt, ...) VLOG_SCOPE_F(level, fmt, __VA_ARGS__)
#define INFO(fmt, ...) LOG_F(INFO, fmt, __VA_ARGS__)
#define WARN(fmt, ...) LOG_F(WARN, fmt, __VA_ARGS__)
#define VERBOSE(level, fmt, ...) LOG_F(level, fmt, __VA_ARGS__)
#define CHECK(test, ...) CHECK_F(test, __VA_ARGS__)
#ifdef BREAK_AT_ERRORS
#define ERROR(fmt, ...)           \
  LOG_F(ERROR, fmt, __VA_ARGS__); \
  debugBreak();
#define ERROR_IF(cond, ...)    \
  if (cond) {                  \
    LOG_F(ERROR, __VA_ARGS__); \
    debugBreak();              \
  }
#else
#define ERROR(fmt, ...) LOG_F(ERROR, fmt, __VA_ARGS__)
#define ERROR_IF(cond, ...) LOG_IF_F(ERROR, cond, __VA_ARGS__)
#endif

#else

#define INFO_SCOPE(fmt, ...)
#define VERBOSE_SCOPE(level, fmt, ...)
#define INFO(fmt, ...)
#define WARN(fmt, ...)
#define ERROR(fmt, ...)
#define VERBOSE(level, fmt, ...)
#define CHECK(test, ...)
#define ERROR_IF(cond, ...) \
  if (cond) {               \
  }

#endif
