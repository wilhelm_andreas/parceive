#include "ArgumentParser.h"
#include "Logging.h"
#include "Symbols.h"

using namespace pcv::symbols;

int main(const int argc, char** argv)
{
  int retVal = 1;
  setLogMode(LogMode::SIMPLE);

  Arguments args;
  const ArgumentParser parser(argc, argv);

  if (!parser.parseArguments(argc, argv, args)) {
    parser.printUsage();
  } else {
    setLogMode(LogMode::DETAILED);
    initLogging(argc, argv);
    retVal = runProgram(args);
  }

  return retVal;
}