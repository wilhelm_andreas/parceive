#include <stdlib.h>
#include <algorithm>

#include "Logging.h"

#include "sqlite/Connection.h"
#include "sqlite/ContextWriter.h"
#include "sqlite/StatementBinder.h"

#include <sqlite3.h>
#include "Context.h"
#include "entities/Class.h"
#include "entities/Image.h"
#include "entities/Namespace.h"
#include "entities/Routine.h"
#include "entities/Variable.h"

using namespace pcv;
using namespace pcv::entity;
using namespace pcv::symbols::sqlite;
using namespace std;

std::string getFullPathName(const std::string& input)
{
  char fullPath[_MAX_PATH];
  if (_fullpath(fullPath, input.c_str(), _MAX_PATH) != NULL) {
    return fullPath;
  }
  return input;
}

void sqliteErrorLog(void* pArg, int iErrCode, const char* zMsg)
{
  ERROR("(%d) %s\n", iErrCode, zMsg);
}

ContextWriter::ContextWriter()
{
#ifdef DEBUG
  sqlite3_config(SQLITE_CONFIG_LOG, sqliteErrorLog, nullptr);
#endif
}

void ContextWriter::insertContext(const string& dbFilePath,
                                  const Context& ctxt,
                                  const bool clearPreexistingData)
{
  VERBOSE_SCOPE(1, "Insert context");

  if (conn.open(dbFilePath.c_str(), clearPreexistingData)) {
    if (createDatabase()) {
      if (prepareStatements()) {
        writeData(ctxt);

        finalizeAllStatements();
        conn.close();
      } else {
        ERROR("Unable to insert context: not all statments are valid.");
      }
    }
  } else {
    ERROR("Unable to insert context into %s.", dbFilePath);
  }
}

void ContextWriter::writeData(const Context& ctxt)
{
  if (!beginTransaction.execute()) {
    ERROR("Unable to begin transaction.");
  }

  for (const auto& img : ctxt.images) {
    insert(*img);
  }
  for (const auto& ns : ctxt.namespaces) {
    insert(*ns);
  }
  for (const auto& routine : ctxt.routines) {
    insert(*routine);
  }
  for (const auto& cls : ctxt.classes) {
    insert(*cls);
  }
  for (const auto& var : ctxt.variables) {
    insert(*var);
  }

  if (!commitTransaction.execute()) {
    ERROR("Unable to commit transaction.");
  }
}

pcv::Id_t getId(const SoftwareEntity* entity) { return entity == nullptr ? -1 : entity->id; }

void ContextWriter::insert(const SoftwareEntity& entity, const EntityType& type)
{
  const auto fileName = (entity.file == nullptr) ? SoftwareEntity::name_t() : entity.file->name;

  insertSoftwareEntity << entity.id << getId(entity.img) << (int)type << getId(entity.nmsp)
                       << getId(entity.cls) << entity.name << entity.size << fileName
                       << entity.line;

  if (!insertSoftwareEntity.execute()) {
    ERROR("Failed to insert software entity with id %d and type %d", entity.id, type);
  }
}

void ContextWriter::insert(const Image& image)
{
  insert(static_cast<const SoftwareEntity&>(image), EntityType::Image);
}

void ContextWriter::insert(const Namespace& ns)
{
  insert(static_cast<const SoftwareEntity&>(ns), EntityType::Namespace);

  insertNamespace << ns.id << getId(ns.img) << getId(ns.parent);
  if (!insertNamespace.execute()) {
    ERROR("Failed to insert namespace with id %d", ns.id);
  }
}

void ContextWriter::insert(const Routine& routine)
{
  insert(static_cast<const SoftwareEntity&>(routine), EntityType::Routine);

  insertRoutine << routine.id << getId(routine.img) << (int)routine.isConstructor;
  if (!insertRoutine.execute()) {
    ERROR("Failed to insert routine with id %d", routine.id);
  }
}

void ContextWriter::insert(const Class& cls)
{
  insert(static_cast<const SoftwareEntity&>(cls), EntityType::Class);
  for (const auto& base : cls.baseClasses) {
    insertClassRelationship(cls, base, ClassRelationshipType::Base);
  }
  for (const auto& inherited : cls.inheritClasses) {
    insertClassRelationship(cls, inherited, ClassRelationshipType::Inherited);
  }
  for (const auto& nested : cls.nestedClasses) {
    insertClassRelationship(cls, nested, ClassRelationshipType::Nested);
  }
  for (const auto& composite : cls.composites) {
    insertClassRelationship(cls, composite, ClassRelationshipType::Composite);
  }
  for (const auto& member : cls.members) {
    this->insert(*member);
  }
}

void ContextWriter::insert(const Variable& var)
{
  insert(static_cast<const SoftwareEntity&>(var), EntityType::Variable);

  insertVariable << var.id << getId(var.img) << getId(var.declaringClass)
                 << getId(var.declaringRoutine) << var.offset << (int)var.type;
  if (!insertVariable.execute()) {
    ERROR("Failed to insert variable with id %d", var.id);
  }
}

void ContextWriter::insertClassRelationship(const Class& owner,
                                            const Class* owned,
                                            const ClassRelationshipType& type)
{
  insertClassRelationships << owner.id << getId(owned) << getId(owner.img) << int(type);
  if (!insertClassRelationships.execute()) {
    ERROR(
      "Failed to insert class relationship betwee classes with ids %d and %d", owner.id, owned->id);
  }
}

bool ContextWriter::createDatabase() const
{
  return conn.execute(
#include "sqlite/create_context_db.sql.h"
  );
}

bool ContextWriter::prepareStatements()
{
  conn.createBeginTransationStatement(beginTransaction);
  conn.createCommitTransactionStatement(commitTransaction);

  conn.createStatement(insertSoftwareEntity,
                       "INSERT INTO SoftwareEntity(Id, ImageId, EntityType, NamespaceId, ClassId, "
                       "Name, Size, FilePath, Line)"
                       "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?);");
  conn.createStatement(
    insertClassRelationships,
    "INSERT INTO ClassRelationships(FirstClassId, SecondClassId, ImageId, RelationshipType)"
    "VALUES(?, ?, ?, ?);");
  conn.createStatement(insertNamespace,
                       "INSERT INTO Namespace(EntityId, ImageId, ParentId)"
                       "VALUES(?, ?, ?);");
  conn.createStatement(insertRoutine,
                       "INSERT INTO Routine(EntityId, ImageId, IsConstructor)"
                       "VALUES(?, ?, ?);");
  conn.createStatement(
    insertVariable,
    "INSERT INTO Variable(EntityId, ImageId, DeclaringClassId, DeclaringRoutineId, Offset, Type)"
    "VALUES(?, ?, ?, ?, ?, ?);");

  return areAllStatementsValid();
}

void ContextWriter::finalizeAllStatements()
{
  for (auto& stmt : getAllStatements()) {
    stmt->finalize();
  }
}

bool ContextWriter::areAllStatementsValid()
{
  bool res = true;
  for (const auto& stmt : getAllStatements()) {
    res &= stmt->isValid();
  }
  return res;
}

array<Statement*, 7> ContextWriter::getAllStatements()
{
  return {&beginTransaction,
          &commitTransaction,
          &insertSoftwareEntity,
          &insertClassRelationships,
          &insertNamespace,
          &insertRoutine,
          &insertVariable};
}
