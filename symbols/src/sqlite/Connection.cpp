#include <sqlite3.h>

#include "Logging.h"
#include "sqlite/Connection.h"

using namespace pcv::symbols::sqlite;

Connection::Connection() : db(nullptr) {}

Connection::~Connection() { ensureIsClosed(); }

bool Connection::open(const char* const dbFilePath, const bool clearPreexistingData)
{
  ensureIsClosed();

  if (clearPreexistingData) {
    if (EACCES == remove(dbFilePath)) {
      ERROR("The given db path is either opened or read only.");
      return false;
    }
  }

  const int flags = SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_FULLMUTEX;
  int code        = sqlite3_open_v2(dbFilePath, &db, flags, nullptr);
  bool isOk       = SQLITE_OK == code;
  ERROR_IF(!isOk, "Failed to open %s: %s", dbFilePath, sqlite3_errstr(code));

  return isOk;
}

bool Connection::isOpen() const { return db != nullptr; }

void Connection::close()
{
  int code = sqlite3_close(db);

  if (SQLITE_OK == code) {
    db = nullptr;
  } else {
    ERROR("Failed to close db: %s", sqlite3_errstr(code));
  }
}

bool Connection::execute(const char* const sql) const
{
  char* err = nullptr;
  int code  = sqlite3_exec(db, sql, nullptr, nullptr, &err);
  if (SQLITE_OK != code) {
    ERROR("Failed to execute '%s' : %s", sql, err ? err : sqlite3_errstr(code));
    sqlite3_free(err);
    return false;
  }
  return true;
}

bool Connection::createStatement(Statement& stmt, const char* const sql) const
{
  const auto sql_stmt = createSqlStatement(sql);

#ifdef DEBUG
  stmt.setStatement(sql_stmt, sql);
#else
  stmt.setStatement(sql_stmt);
#endif
  return sql_stmt != nullptr;
}

bool Connection::createStatement(Statement& stmt, const std::string sql) const
{
  return createStatement(stmt, sql.c_str());
}

Statement Connection::createStatement(const char* const sql) const
{
  const auto sql_stmt = createSqlStatement(sql);

#ifdef DEBUG
  return Statement(sql_stmt, sql);
#else
  return Statement(sql_stmt);
#endif
}

Statement Connection::createStatement(const std::string sql) const
{
  return createStatement(sql.c_str());
}

void Connection::createBeginTransationStatement(Statement& stmt) const
{
  createStatement(stmt, "BEGIN EXCLUSIVE TRANSACTION");
}

void Connection::createCommitTransactionStatement(Statement& stmt) const
{
  createStatement(stmt, "COMMIT TRANSACTION");
}

sqlite3_stmt* Connection::createSqlStatement(const char* const sql) const
{
  sqlite3_stmt* sql_stmt = nullptr;
  CHECK(isOpen(), "Cannot create statement: DB is not open.");

  int code = sqlite3_prepare_v2(db, sql, static_cast<int>(strlen(sql)), &sql_stmt, nullptr);

  ERROR_IF(SQLITE_OK != code, "Unable to create statement from sql '%s'.", sql);
  return sql_stmt;
}
