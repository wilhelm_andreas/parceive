#include <algorithm>
#include <cinttypes>
#include "Logging.h"

#include "sqlite/CodeLocationWriter.h"
#include "sqlite/Connection.h"
#include "sqlite/StatementBinder.h"

using namespace pcv;
using namespace pcv::dbghelp;
using namespace pcv::symbols::sqlite;
using namespace std;

void CodeLocationWriter::insertCodeLocations(const std::string& dbFilePath,
                                             const pcv::Id_t imageId,
                                             const AddressToCodeLocationMap& locationMap,
                                             const bool clearPreexistingData)
{
  VERBOSE_SCOPE(1, "Inserting code locations");

  if (conn.open(dbFilePath.c_str(), clearPreexistingData)) {
    if (createDatabase()) {
      if (prepareStatements()) {
        if (!writeData(imageId, locationMap)) {
          ERROR("Unable to write source code context.");
        }

        finalizeAllStatements();
        conn.close();
      } else {
        ERROR("Unable to insert context: not all statments are valid.");
      }
    }
  } else {
    ERROR("Unable to insert context into %s.", dbFilePath);
  }
}

bool CodeLocationWriter::writeData(const pcv::Id_t imageId,
                                   const AddressToCodeLocationMap& locationMap)
{
  if (!beginTransaction.execute()) {
    ERROR("Unable to begin transaction.");
  }

  bool insertionsOk = true;

  for (const auto& kvp : locationMap) {
    const auto address = kvp.first;
    const auto& ctxt   = kvp.second;
    insertCodeLocation << imageId << address << ctxt.fileName << ctxt.line;
    if (!insertCodeLocation.execute()) {
      ERROR("Unable to insert row with image id %d and address " PRIu64 ".", imageId, address);
      insertionsOk = false;
    }
  }

  if (insertionsOk) {
    if (!commitTransaction.execute()) {
      ERROR("Unable to commit transaction.");
    }
  }
  return insertionsOk;
}

bool CodeLocationWriter::createDatabase() const
{
  return conn.execute(
#include "sqlite/create_code_location_db.sql.h"
  );
}

bool CodeLocationWriter::prepareStatements()
{
  conn.createBeginTransationStatement(beginTransaction);
  conn.createCommitTransactionStatement(commitTransaction);
  conn.createStatement(insertCodeLocation,
                       "INSERT INTO CodeLocation(ImageId, Address, FileName, Line)"
                       "VALUES(?, ?, ?, ?);");

  return areAllStatementsValid();
}

void CodeLocationWriter::finalizeAllStatements()
{
  for (const auto& stmt : getAllStatements()) {
    stmt->finalize();
  }
}

bool CodeLocationWriter::areAllStatementsValid()
{
  bool res = true;
  for (const auto& stmt : getAllStatements()) {
    res &= stmt->isValid();
  }
  return res;
}
